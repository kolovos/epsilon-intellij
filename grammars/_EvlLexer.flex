package org.eclipse.epislon.labs.intellij.grammar.parser.evl;

import com.intellij.lexer.FlexLexer;
import com.intellij.psi.tree.IElementType;

import static com.intellij.psi.TokenType.BAD_CHARACTER;
import static com.intellij.psi.TokenType.WHITE_SPACE;
import static org.eclipse.epislon.labs.intellij.grammar.psi.eol.EolTypes.*;
import static org.eclipse.epislon.labs.intellij.grammar.psi.erl.ErlTypes.*;
import static org.eclipse.epislon.labs.intellij.grammar.psi.evl.EvlTypes.*;

%%

%{
  public _EvlLexer() {
    this((java.io.Reader)null);
  }
%}

%public
%class _EvlLexer
%implements FlexLexer
%function advance
%type IElementType
%unicode

EOL=\R
WHITE_SPACE=\s+

FLOAT=([0-9]*\.)?[0-9]+([eE][-+]?[0-9]+)?[fFdD]?
ID=[:jletter:] [:jletterdigit:]*
STRING=('([^'\\]|\\.)*'|\"([^\"\\]|\\\"|\\'|\\)*\")
ANNOTATION=@[^\n\r]*
LINE_COMMENT="//"[^\n\r]*
BLOCK_COMMENT="/"\*[^*]*\*+([^/*][^*]*\*+)*"/"

%%
<YYINITIAL> {
  {WHITE_SPACE}      { return WHITE_SPACE; }

    ","                  { return EOL_COMMA; }
    ";"                  { return EOL_SEMICOLON; }
    "{"                  { return EOL_LEFT_BRACE; }
    "}"                  { return EOL_RIGHT_BRACE; }
    "("                  { return EOL_LEFT_PAREN; }
    ")"                  { return EOL_RIGHT_PAREN; }
    "["                  { return EOL_LEFT_SQ; }
    "]"                  { return EOL_RIGHT_SQ; }
    "$"                  { return EOL_EXEC_ANNOT; }
    "$pre"               { return EOL_PRE_COND; }
    "$post"              { return EOL_POST_COND; }
    "abort"              { return EOL_ABORT_KEY; }
    "Bag"                { return EOL_BAG_KEY; }
    "breakAll"           { return EOL_BREAK_ALL_KEY; }
    "break"              { return EOL_BREAK_KEY; }
    "case"               { return EOL_CASE_KEY; }
    "Collection"         { return EOL_COL_KEY; }
    "continue"           { return EOL_CONTINUE_KEY; }
    "default"            { return EOL_DEFAULT_KEY; }
    "delete"             { return EOL_DELETE_KEY; }
    "else"               { return EOL_ELSE_KEY; }
    "ext"                { return EOL_EXT_KEY; }
    "false"              { return EOL_FALSE_KEY; }
    "for"                { return EOL_FOR_KEY; }
    "function"           { return EOL_FUNCTION_KEY; }
    "if"                 { return EOL_IF_KEY; }
    "import"             { return EOL_IMPORT_KEY; }
    "in"                 { return EOL_IN_KEY; }
    "List"               { return EOL_LIST_KEY; }
    "Map"                { return EOL_MAP_KEY; }
    "Native"             { return EOL_NATIVE_KEY; }
    "new"                { return EOL_NEW_KEY; }
    "operation"          { return EOL_OPERATION_KEY; }
    "OrderedSet"         { return EOL_ORDSET_KEY; }
    "return"             { return EOL_RETURN_KEY; }
    "Sequence"           { return EOL_SEQ_KEY; }
    "Set"                { return EOL_SET_KEY; }
    "switch"             { return EOL_SWITCH_KEY; }
    "throw"              { return EOL_THROW_KEY; }
    "transaction"        { return EOL_TRANS_KEY; }
    "true"               { return EOL_TRUE_KEY; }
    "var"                { return EOL_VAR_KEY; }
    "while"              { return EOL_WHILE_KEY; }
    "self"               { return EOL_SELF_BIN; }
    "loopCount"          { return EOL_LOOP_CNT_BIN; }
    "hasMore"            { return EOL_HAS_MORE_BIN; }
    "and"                { return EOL_AND_OP; }
    "->"                 { return EOL_ARROW_OP; }
    "="                  { return EOL_ASSIGN_OP; }
    ":"                  { return EOL_COLON_OP; }
    "/="                 { return EOL_DIV_ASSIG_OP; }
    "/"                  { return EOL_DIV_OP; }
    "#"                  { return EOL_ENUM_OP; }
    "=="                 { return EOL_EQ_OP; }
    "::="                { return EOL_EQUIV_ASSIGN_OP; }
    ">="                 { return EOL_GE_OP; }
    ">"                  { return EOL_GT_OP; }
    "implies"            { return EOL_IMPLIES_OP; }
    "|"                  { return EOL_IN_OP; }
    "++"                 { return EOL_INC_OP; }
    "<="                 { return EOL_LE_OP; }
    "<"                  { return EOL_LT_OP; }
    "-="                 { return EOL_MINUS_ASSIGN_OP; }
    "!"                  { return EOL_MODEL_OP; }
    "<>"                 { return EOL_NE_OP; }
    "-"                  { return EOL_NEG_OP; }
    "not"                { return EOL_NOT_OP; }
    "or"                 { return EOL_OR_OP; }
    "+="                 { return EOL_PLUS_ASSIGN_OP; }
    "+"                  { return EOL_PLUS_OP; }
    "."                  { return EOL_POINT_OP; }
    ".."                 { return EOL_POINT_POINT_OP; }
    "::"                 { return EOL_QUAL_OP; }
    ":="                 { return EOL_SPECIAL_ASSIGN_OP; }
    "=>"                 { return EOL_THEN_OP; }
    "*="                 { return EOL_TIMES_ASSIGN_OP; }
    "*"                  { return EOL_TIMES_OP; }
    "xor"                { return EOL_XOR_OP; }

  "pre"              { return ERL_PRE_KEY; }
  "post"             { return ERL_POST_KEY; }
  "guard"            { return ERL_GUARD_KEY; }
  "extends"          { return ERL_EXTENDS_KEY; }

  "constraint"       { return EVL_CONSTRAINT_KEY; }
  "critique"         { return EVL_CRITIQUE_KEY; }
  "context"          { return EVL_CONTEXT_KEY; }
  "fix"              { return EVL_FIX_KEY; }
  "check"            { return EVL_CHECK_KEY; }
  "do"               { return EVL_DO_KEY; }
  "title"            { return EVL_TITLE_KEY; }
  "message"          { return EVL_MESSAGE_KEY; }


   {FLOAT}              { return EOL_FLOAT; }
   {ID}                 { return EOL_ID; }
   {STRING}             { return EOL_STRING; }
   {ANNOTATION}         { return EOL_ANNOTATION; }
   {LINE_COMMENT}       { return EOL_LINE_COMMENT; }
   {BLOCK_COMMENT}      { return EOL_BLOCK_COMMENT; }

}

[^] { return BAD_CHARACTER; }

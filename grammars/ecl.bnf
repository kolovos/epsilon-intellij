/*******************************************************************************
 * Copyright (c) 2008 The University of York.
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *     Horacio Hoyos Rodriguez - initial API and implementation
 ******************************************************************************/
{
  classHeader="license.txt"
  parserClass="org.eclipse.epislon.labs.intellij.grammar.parser.ecl.EclParser"
  parserImports=["static org.eclipse.epislon.labs.intellij.grammar.parser.EpsilonParserUtil.*"]
  psiImplUtilClass="org.eclipse.epislon.labs.intellij.grammar.parser.ecl.EclParserUtil"

  extends="com.intellij.extapi.psi.ASTWrapperPsiElement"

  psiClassPrefix="Ecl"
  psiImplClassSuffix="Impl"
  psiPackage="org.eclipse.epislon.labs.intellij.grammar.psi.ecl"
  psiImplPackage="org.eclipse.epislon.labs.intellij.grammar.psi.ecl.impl"

  elementTypeHolderClass="org.eclipse.epislon.labs.intellij.grammar.psi.ecl.EclTypes"
  elementTypePrefix="ECL_"
  elementTypeClass="org.eclipse.epislon.labs.intellij.grammar.psi.ecl.EclElementType"
  tokenTypeClass="org.eclipse.epislon.labs.intellij.grammar.psi.ecl.EclTokenType"

  generate=[token-accessors="yes"]

  tokens = [
    // Keywords
    RULE_KEY='rule'
    MATCH_KEY='match'
    WITH_KEY='with'
    COMPARE_KEY='compare'
    DO_KEY='do'
  ]
}

root ::= module <<eof>>
external ID ::= eolId
external EOL_LB ::= eolLeftBrace
external EOL_RB ::= eolRightBrace
external ERL_PRE ::= erlPre
external ERL_POST ::= erlPost

external importStatement ::= eolImportStatement
external modelDeclaration ::= eolModelDeclaration
external annotationBlock ::= eolAnnotationBlock
external operationDeclaration ::= eolOperationDeclaration
external eolFormalParameter ::= eolFormalParameter
external eolStatement ::= eolStatement

external erlGuard ::= erlGuard
external erlExtend ::= erlExtend

// Make local versions of external rules
statement ::= eolStatement
pre ::= ERL_PRE ID? EOL_LB statement* EOL_RB
post ::= ERL_POST ID? EOL_LB statement* EOL_RB
formalParameter ::= eolFormalParameter
external expressionOrStatementBlock ::= eolExprOrStatementBlock
external statementBlock ::= eolStatementBlock
guard ::= erlGuard{
  methods=[getExpr getStatementBlock]
}
extends ::= erlExtend

// Local rules
module ::= importStatement* modelDeclaration* content {
    implements="org.eclipse.epislon.labs.intellij.grammar.psi.EpsilonModule"
    extends="org.eclipse.epislon.labs.intellij.grammar.psi.EpsilonModuleImpl"
}

content ::= (matchRule | pre | post | annotationBlock |  operationDeclaration)* {
    methods = [
    getAnnotationBlockList
    getOperationDeclarationList]
}

matchRule ::= 'rule' ID 'match' formalParameter 'with' formalParameter
	extends? EOL_LB guard? compareBlock? doBlock? EOL_RB {
    methods=[getLeftBrace]
}

compareBlock ::= 'compare' expressionOrStatementBlock {
    methods=[getExpr getStatementBlock]
}

doBlock ::= 'do' statementBlock {
    methods=[getStatementBlock]
}

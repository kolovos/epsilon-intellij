package org.eclipse.epislon.labs.intellij.grammar.parser.egl;

import com.intellij.lexer.FlexLexer;
import com.intellij.psi.tree.IElementType;

import static com.intellij.psi.TokenType.BAD_CHARACTER;
import static com.intellij.psi.TokenType.WHITE_SPACE;
import static org.eclipse.epislon.labs.intellij.grammar.psi.egl.EglTypes.*;

%%
%{
  public _EglLexer() {
    this((java.io.Reader)null);
  }
  StringBuffer verbatim = new StringBuffer();

//  private Symbol symbol(int type) {
//    return new Symbol(type, yyline, yycolumn);
//  }
%}

%public
%class _EglLexer
%implements FlexLexer
%function advance
%type IElementType
%unicode

EOL=\R
WHITE_SPACE=\s+

FLOAT=([0-9]*\.)?[0-9]+([eE][-+]?[0-9]+)?[fFdD]?
ID=[:jletter:] [:jletterdigit:]*
STRING=('([^'\\]|\\.)*'|\"([^\"\\]|\\\"|\\'|\\)*\")
ANNOTATION=@[^\n\r]*
LINE_COMMENT="//"[^\n\r%]*
BLOCK_COMMENT="/"\*[^*]*\*+([^/*][^*]*\*+)*"/"

%state CODE, PRECODE, POSTCODE

%%
<YYINITIAL> {
   "["                  { yybegin(PRECODE);
                         //verbatim.append( yytext() );
                        }
     [^\[]*             {
                        verbatim.append(yytext());
                        return EGL_VERBATIM_TEXT;
                        }
}
<PRECODE> {
   "%"                 { yybegin(CODE);
                         verbatim.append( yytext() );
                         //System.out.println(verbatim.toString());
                         return EGL_CODE_LIM;
                       }
   [^%]                { yybegin(YYINITIAL);
                         verbatim.append( yytext() );
                       }
}
<POSTCODE> {
   "]"                 { yybegin(YYINITIAL);
                         return EGL_CODE_LIM;
                        }
   [^\]]               { yybegin(CODE);
                         //return WHITE_SPACE;
                        }
}
<CODE> {
   {WHITE_SPACE}        { return WHITE_SPACE; }
   "%"                  {
                        verbatim.setLength(0);
                           yybegin(POSTCODE);
                        }
   ","                  { return EGL_COMMA; }
   ";"                  { return EGL_SEMICOLON; }
   "{"                  { return EGL_LEFT_BRACE; }
   "}"                  { return EGL_RIGHT_BRACE; }
   "("                  { return EGL_LEFT_PAREN; }
   ")"                  { return EGL_RIGHT_PAREN; }
   "["                  { return EGL_LEFT_SQ; }
   "]"                  { return EGL_RIGHT_SQ; }
   "$"                  { return EGL_EXEC_ANNOT; }
   "$pre"               { return EGL_PRE_COND; }
   "$post"              { return EGL_POST_COND; }
   "abort"              { return EGL_ABORT_KEY; }
   "Bag"                { return EGL_BAG_KEY; }
   "breakAll"           { return EGL_BREAK_ALL_KEY; }
   "break"              { return EGL_BREAK_KEY; }
   "case"               { return EGL_CASE_KEY; }
   "Collection"         { return EGL_COL_KEY; }
   "continue"           { return EGL_CONTINUE_KEY; }
   "default"            { return EGL_DEFAULT_KEY; }
   "delete"             { return EGL_DELETE_KEY; }
   "else"               { return EGL_ELSE_KEY; }
   "ext"                { return EGL_EXT_KEY; }
   "false"              { return EGL_FALSE_KEY; }
   "for"                { return EGL_FOR_KEY; }
   "function"           { return EGL_FUNCTION_KEY; }
   "if"                 { return EGL_IF_KEY; }
   "import"             { return EGL_IMPORT_KEY; }
   "in"                 { return EGL_IN_KEY; }
   "List"               { return EGL_LIST_KEY; }
   "Map"                { return EGL_MAP_KEY; }
   "Native"             { return EGL_NATIVE_KEY; }
   "new"                { return EGL_NEW_KEY; }
   "operation"          { return EGL_OPERATION_KEY; }
   "OrderedSet"         { return EGL_ORDSET_KEY; }
   "return"             { return EGL_RETURN_KEY; }
   "Sequence"           { return EGL_SEQ_KEY; }
   "Set"                { return EGL_SET_KEY; }
   "switch"             { return EGL_SWITCH_KEY; }
   "throw"              { return EGL_THROW_KEY; }
   "transaction"        { return EGL_TRANS_KEY; }
   "true"               { return EGL_TRUE_KEY; }
   "var"                { return EGL_VAR_KEY; }
   "while"              { return EGL_WHILE_KEY; }
   "self"               { return EGL_SELF_BIN; }
   "loopCount"          { return EGL_LOOP_CNT_BIN; }
   "hasMore"            { return EGL_HAS_MORE_BIN; }
   "and"                { return EGL_AND_OP; }
   "->"                 { return EGL_ARROW_OP; }
   "="                  { return EGL_ASSIGN_OP; }
   ":"                  { return EGL_COLON_OP; }
   "/="                 { return EGL_DIV_ASSIG_OP; }
   "/"                  { return EGL_DIV_OP; }
   "#"                  { return EGL_ENUM_OP; }
   "=="                 { return EGL_EQ_OP; }
   "::="                { return EGL_EQUIV_ASSIGN_OP; }
   ">="                 { return EGL_GE_OP; }
   ">"                  { return EGL_GT_OP; }
   "implies"            { return EGL_IMPLIES_OP; }
   "|"                  { return EGL_IN_OP; }
   "++"                 { return EGL_INC_OP; }
   "<="                 { return EGL_LE_OP; }
   "<"                  { return EGL_LT_OP; }
   "-="                 { return EGL_MINUS_ASSIGN_OP; }
   "!"                  { return EGL_MODEL_OP; }
   "<>"                 { return EGL_NE_OP; }
   "-"                  { return EGL_NEG_OP; }
   "not"                { return EGL_NOT_OP; }
   "or"                 { return EGL_OR_OP; }
   "+="                 { return EGL_PLUS_ASSIGN_OP; }
   "+"                  { return EGL_PLUS_OP; }
   "."                  { return EGL_POINT_OP; }
   ".."                 { return EGL_POINT_POINT_OP; }
   "::"                 { return EGL_QUAL_OP; }
   ":="                 { return EGL_SPECIAL_ASSIGN_OP; }
   "=>"                 { return EGL_THEN_OP; }
   "*="                 { return EGL_TIMES_ASSIGN_OP; }
   "*"                  { return EGL_TIMES_OP; }
   "xor"                { return EGL_XOR_OP; }

   "pre"                { return EGL_PRE_KEY; }
   "post"               { return EGL_POST_KEY; }
   "guard"              { return EGL_GUARD_KEY; }
   "extends"            { return EGL_EXTENDS_KEY; }

   {FLOAT}              { return EGL_FLOAT; }
   {ID}                 { return EGL_ID; }
   {STRING}             { return EGL_STRING; }
   {ANNOTATION}         { return EGL_ANNOTATION; }
   {LINE_COMMENT}       { return EGL_LINE_COMMENT; }
   {BLOCK_COMMENT}      { return EGL_BLOCK_COMMENT; }
}

[^] { return BAD_CHARACTER; }

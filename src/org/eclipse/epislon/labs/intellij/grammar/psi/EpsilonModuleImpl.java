/*******************************************************************************
 * Copyright (c) 2008 The University of York.
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *     Horacio Hoyos Rodriguez - initial API and implementation
 ******************************************************************************/
package org.eclipse.epislon.labs.intellij.grammar.psi;

import com.intellij.extapi.psi.ASTWrapperPsiElement;
import com.intellij.lang.ASTNode;
import com.intellij.psi.PsiElementVisitor;
import com.intellij.psi.util.PsiTreeUtil;
import org.eclipse.epislon.labs.intellij.grammar.psi.eol.*;
import org.jetbrains.annotations.NotNull;

import java.util.List;

public class EpsilonModuleImpl extends ASTWrapperPsiElement implements EpsilonModule {

  public EpsilonModuleImpl(@NotNull ASTNode node) {
    super(node);
  }

  @Override
  @NotNull
  public List<EolImportStatement> getImportStatementList() {
    return PsiTreeUtil.getChildrenOfTypeAsList(this, EolImportStatement.class);
  }

  @Override
  @NotNull
  public List<EolModelDeclaration> getModelDeclarationList() {
    return PsiTreeUtil.getChildrenOfTypeAsList(this, EolModelDeclaration.class);
  }

}

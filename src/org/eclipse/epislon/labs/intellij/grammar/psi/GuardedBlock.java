package org.eclipse.epislon.labs.intellij.grammar.psi;

import com.intellij.psi.PsiElement;
import org.eclipse.epislon.labs.intellij.grammar.psi.eol.EolStatementBlock;
import org.eclipse.epislon.labs.intellij.grammar.psi.erl.ErlGuard;
import org.jetbrains.annotations.NotNull;

public interface GuardedBlock extends PsiElement {

}

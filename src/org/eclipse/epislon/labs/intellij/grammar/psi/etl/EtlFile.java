// THIS FILE IS GENERATED. DO NOT MODIFY IT
/*******************************************************************************
 * Copyright (c) 2008 The University of York.
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *     Horacio Hoyos Rodriguez - initial API and implementation
 ******************************************************************************/
package org.eclipse.epislon.labs.intellij.grammar.psi.etl;

import com.intellij.extapi.psi.PsiFileBase;
import com.intellij.openapi.fileTypes.FileType;
import com.intellij.psi.FileViewProvider;
import org.eclipse.epislon.labs.intellij.grammar.language.eol.EolFileType;
import org.eclipse.epislon.labs.intellij.grammar.language.eol.EolLanguage;
import org.eclipse.epislon.labs.intellij.grammar.language.etl.EtlFileType;
import org.eclipse.epislon.labs.intellij.grammar.language.etl.EtlLanguage;
import org.jetbrains.annotations.NotNull;

import javax.swing.*;

public class EtlFile extends PsiFileBase {
	public EtlFile(@NotNull FileViewProvider viewProvider) {
		super(viewProvider, EtlLanguage.INSTANCE);
	}

	@NotNull
	@Override
	public FileType getFileType() {
		return EtlFileType.INSTANCE;
	}

	@Override
	public String toString() {
		return "ETL File";
	}

	@Override
	public Icon getIcon(int flags) {
		return super.getIcon(flags);
	}
}

// THIS FILE IS GENERATED. DO NOT MODIFY IT
/*******************************************************************************
 * Copyright (c) 2008 The University of York.
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *     Horacio Hoyos Rodriguez - initial API and implementation
 ******************************************************************************/
package org.eclipse.epislon.labs.intellij.grammar.psi.ecl;

import com.intellij.extapi.psi.PsiFileBase;
import com.intellij.openapi.fileTypes.FileType;
import com.intellij.psi.FileViewProvider;
import org.eclipse.epislon.labs.intellij.grammar.language.eol.EolFileType;
import org.eclipse.epislon.labs.intellij.grammar.language.eol.EolLanguage;
import org.eclipse.epislon.labs.intellij.grammar.language.ecl.EclFileType;
import org.eclipse.epislon.labs.intellij.grammar.language.ecl.EclLanguage;
import org.jetbrains.annotations.NotNull;

import javax.swing.*;

public class EclFile extends PsiFileBase {
	public EclFile(@NotNull FileViewProvider viewProvider) {
		super(viewProvider, EclLanguage.INSTANCE);
	}

	@NotNull
	@Override
	public FileType getFileType() {
		return EclFileType.INSTANCE;
	}

	@Override
	public String toString() {
		return "ECL File";
	}

	@Override
	public Icon getIcon(int flags) {
		return super.getIcon(flags);
	}
}

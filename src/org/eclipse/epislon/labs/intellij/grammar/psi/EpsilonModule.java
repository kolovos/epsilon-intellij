/*******************************************************************************
 * Copyright (c) 2008 The University of York.
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *     Horacio Hoyos Rodriguez - initial API and implementation
 ******************************************************************************/
package org.eclipse.epislon.labs.intellij.grammar.psi;

import com.intellij.psi.PsiElement;
import org.eclipse.epislon.labs.intellij.grammar.psi.eol.EolImportStatement;
import org.eclipse.epislon.labs.intellij.grammar.psi.eol.EolModelDeclaration;
import org.jetbrains.annotations.NotNull;

import java.util.List;

public interface EpsilonModule extends PsiElement {

  @NotNull
  List<EolImportStatement> getImportStatementList();

  @NotNull
  List<EolModelDeclaration> getModelDeclarationList();

}

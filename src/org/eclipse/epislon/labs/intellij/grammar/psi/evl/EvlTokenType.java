// THIS FILE IS GENERATED. DO NOT MODIFY IT
/*******************************************************************************
 * Copyright (c) 2008 The University of York.
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *     Horacio Hoyos Rodriguez - initial API and implementation
 ******************************************************************************/
package org.eclipse.epislon.labs.intellij.grammar.psi.evl;

import com.intellij.psi.tree.IElementType;
import org.eclipse.epislon.labs.intellij.grammar.language.evl.EvlLanguage;
import org.jetbrains.annotations.NonNls;
import org.jetbrains.annotations.NotNull;

public class EvlTokenType extends IElementType {

	public EvlTokenType(@NotNull @NonNls String debugName) {
		super(debugName, EvlLanguage.INSTANCE);
	}

	@Override
	public String toString() {
		return super.toString();
	}
}

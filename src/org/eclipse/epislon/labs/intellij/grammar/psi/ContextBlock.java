package org.eclipse.epislon.labs.intellij.grammar.psi;

import com.intellij.psi.PsiElement;
import org.eclipse.epislon.labs.intellij.grammar.psi.eol.EolStatementBlock;
import org.jetbrains.annotations.NotNull;

public interface ContextBlock extends PsiElement {

	@NotNull
	EolStatementBlock getStatementBlock();
}

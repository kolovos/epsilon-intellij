// THIS FILE IS GENERATED. DO NOT MODIFY IT
/*******************************************************************************
 * Copyright (c) 2008 The University of York.
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *     Horacio Hoyos Rodriguez - initial API and implementation
 ******************************************************************************/
package org.eclipse.epislon.labs.intellij.grammar.psi.egl;

import com.intellij.extapi.psi.PsiFileBase;
import com.intellij.openapi.fileTypes.FileType;
import com.intellij.psi.FileViewProvider;
import org.eclipse.epislon.labs.intellij.grammar.language.eol.EolFileType;
import org.eclipse.epislon.labs.intellij.grammar.language.eol.EolLanguage;
import org.eclipse.epislon.labs.intellij.grammar.language.egl.EglFileType;
import org.eclipse.epislon.labs.intellij.grammar.language.egl.EglLanguage;
import org.jetbrains.annotations.NotNull;

import javax.swing.*;

public class EglFile extends PsiFileBase {
	public EglFile(@NotNull FileViewProvider viewProvider) {
		super(viewProvider, EglLanguage.INSTANCE);
	}

	@NotNull
	@Override
	public FileType getFileType() {
		return EglFileType.INSTANCE;
	}

	@Override
	public String toString() {
		return "EGL File";
	}

	@Override
	public Icon getIcon(int flags) {
		return super.getIcon(flags);
	}
}

/*******************************************************************************
 * Copyright (c) 2008 The University of York.
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *     Horacio Hoyos Rodriguez - initial API and implementation
 ******************************************************************************/
package org.eclipse.epislon.labs.intellij.grammar.parser.etl;

import com.intellij.lang.parser.GeneratedParserUtilBase;
import com.intellij.psi.PsiElement;
import org.eclipse.epislon.labs.intellij.grammar.psi.eol.EolAnnotationBlock;
import org.eclipse.epislon.labs.intellij.grammar.psi.eol.EolExpr;
import org.eclipse.epislon.labs.intellij.grammar.psi.eol.EolOperationDeclaration;
import org.eclipse.epislon.labs.intellij.grammar.psi.eol.EolStatementBlock;
import org.eclipse.epislon.labs.intellij.grammar.psi.etl.EtlContent;
import org.eclipse.epislon.labs.intellij.grammar.psi.etl.EtlGuard;
import org.eclipse.epislon.labs.intellij.grammar.psi.etl.EtlTransformationRule;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

import static org.eclipse.epislon.labs.intellij.grammar.parser.EpsilonParserUtil.*;

public class EtlParserUtil extends GeneratedParserUtilBase {

	@Nullable
	public static EolExpr getExpr(@NotNull EtlGuard guard) {
		return getExprInternal(guard);
	}


	@Nullable
	public static EolStatementBlock getStatementBlock(@NotNull EtlGuard guard) {
		return getStatementBlockInternal(guard);
	}


	@NotNull
	public static List<EolAnnotationBlock>  getAnnotationBlockList(@NotNull EtlContent content) {
		return getAnnotationBlockListInternal(content);
	}
	@NotNull
	public static List<EolOperationDeclaration>  getOperationDeclarationList(@NotNull EtlContent content) {
		return getOperationDeclarationListInternal(content);
	}
	@NotNull
	public static PsiElement getLeftBrace(@NotNull EtlTransformationRule contextBlock) {
		return getLeftBraceInternal(contextBlock);
	}

}

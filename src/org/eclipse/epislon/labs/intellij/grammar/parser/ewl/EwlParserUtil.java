/*******************************************************************************
 * Copyright (c) 2008 The University of York.
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *     Horacio Hoyos Rodriguez - initial API and implementation
 ******************************************************************************/
package org.eclipse.epislon.labs.intellij.grammar.parser.ewl;

import com.intellij.lang.parser.GeneratedParserUtilBase;
import com.intellij.psi.PsiElement;
import org.eclipse.epislon.labs.intellij.grammar.psi.eol.EolAnnotationBlock;
import org.eclipse.epislon.labs.intellij.grammar.psi.eol.EolExpr;
import org.eclipse.epislon.labs.intellij.grammar.psi.eol.EolOperationDeclaration;
import org.eclipse.epislon.labs.intellij.grammar.psi.eol.EolStatementBlock;
import org.eclipse.epislon.labs.intellij.grammar.psi.erl.ErlPost;
import org.eclipse.epislon.labs.intellij.grammar.psi.erl.ErlPre;
import org.eclipse.epislon.labs.intellij.grammar.psi.ewl.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

import static org.eclipse.epislon.labs.intellij.grammar.parser.EpsilonParserUtil.*;

public class EwlParserUtil extends GeneratedParserUtilBase {

	@Nullable
	public static EolExpr getExpr(@NotNull EwlGuard guard) {
		return getExprInternal(guard);
	}

	@Nullable
	public static EolExpr getExpr(@NotNull EwlTitleBlock titleBlock) {
		return getExprInternal(titleBlock);
	}

	@Nullable
	public static EolExpr getExpr(@NotNull EwlDoBlock doBlock) {
		return getExprInternal(doBlock);
	}

	@NotNull
	public static EolStatementBlock getStatementBlock(@NotNull EwlPre pre) {
		return getStatementBlockInternal(pre);
	}

	@NotNull
	public static EolStatementBlock getStatementBlock(@NotNull EwlPost post) {
		return getStatementBlockInternal(post);
	}

	@Nullable
	public static EolStatementBlock getStatementBlock(@NotNull EwlGuard guard) {
		return getStatementBlockInternal(guard);
	}

	@Nullable
	public static EolStatementBlock getStatementBlock(@NotNull EwlTitleBlock titleBlock) {
		return getStatementBlockInternal(titleBlock);
	}

	@Nullable
	public static EolStatementBlock getStatementBlock(@NotNull EwlDoBlock doBlock) {
		return getStatementBlockInternal(doBlock);
	}

	@NotNull
	public static List<EolAnnotationBlock>  getAnnotationBlockList(@NotNull EwlContent content) {
		return getAnnotationBlockListInternal(content);
	}

	@NotNull
	public static List<EolOperationDeclaration>  getOperationDeclarationList(@NotNull EwlContent content) {
		return getOperationDeclarationListInternal(content);
	}

	@NotNull
	public static PsiElement getLeftBrace(@NotNull EwlWizard contextBlock) {
		return getLeftBraceInternal(contextBlock);
	}

}

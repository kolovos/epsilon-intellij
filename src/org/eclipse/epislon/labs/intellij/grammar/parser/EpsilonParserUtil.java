/*******************************************************************************
 * Copyright (c) 2008 The University of York.
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *     Horacio Hoyos Rodriguez - initial API and implementation
 ******************************************************************************/
package org.eclipse.epislon.labs.intellij.grammar.parser;

import com.intellij.lang.ASTNode;
import com.intellij.lang.PsiBuilder;
import com.intellij.lang.parser.GeneratedParserUtilBase;
import com.intellij.openapi.diagnostic.Logger;
import com.intellij.psi.PsiElement;
import com.intellij.psi.tree.IElementType;
import com.intellij.psi.util.PsiTreeUtil;
import org.eclipse.epislon.labs.intellij.grammar.psi.eol.EolAnnotationBlock;
import org.eclipse.epislon.labs.intellij.grammar.psi.eol.EolExpr;
import org.eclipse.epislon.labs.intellij.grammar.psi.eol.EolOperationDeclaration;
import org.eclipse.epislon.labs.intellij.grammar.psi.eol.EolStatementBlock;
import org.eclipse.epislon.labs.intellij.grammar.psi.erl.ErlGuard;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

import static org.eclipse.epislon.labs.intellij.grammar.parser.eol.EolParser.*;
import static org.eclipse.epislon.labs.intellij.grammar.parser.erl.ErlParser.extends_$;
import static org.eclipse.epislon.labs.intellij.grammar.parser.erl.ErlParser.guard;
import static org.eclipse.epislon.labs.intellij.grammar.psi.eol.EolTypes.*;
import static org.eclipse.epislon.labs.intellij.grammar.psi.erl.ErlTypes.*;

public class EpsilonParserUtil extends GeneratedParserUtilBase {

	private static final Logger LOG = Logger.getInstance("#org.eclipse.epislon.labs.intellij.grammar.parser.EpsilonParserUtil");

	// TOKENS
	public static boolean eolId(PsiBuilder b, int l) {
		return consumeToken(b, EOL_ID);
	}
	public static boolean eolLeftBrace(PsiBuilder b, int l) {
		return consumeToken(b, EOL_LEFT_BRACE);
	}
	public static boolean eolRightBrace(PsiBuilder b, int l) {
		return consumeToken(b, EOL_RIGHT_BRACE);
	}
	public static boolean eolLeftSquare(PsiBuilder b, int l) {
		return consumeToken(b, EOL_LEFT_SQ);
	}
	public static boolean eolRightSquare(PsiBuilder b, int l) {
		return consumeToken(b, EOL_RIGHT_SQ);
	}
	public static boolean erlPre(PsiBuilder b, int l) {
		return consumeToken(b, ERL_PRE_KEY);
	}
	public static boolean erlPost(PsiBuilder b, int l) {
		return consumeToken(b, ERL_POST_KEY);
	}

	// EOL
	public static boolean eolAnnotationBlock(PsiBuilder b, int l) {
		return annotationBlock(b, l + 1);
	}
	public static boolean eolExprOrStatementBlock(PsiBuilder b, int l) {
		return exprOrStatementBlock(b, l + 1);
	}
	public static boolean eolIdList(PsiBuilder b, int l) {
		return idList(b, l + 1);
	}
	public static boolean eolImportStatement(PsiBuilder b, int l) {
		return importStatement(b, l + 1);
	}
	public static boolean eolModelDeclaration(PsiBuilder b, int l) {
		return modelDeclaration(b, l + 1);
	}
	public static boolean eolOperationDeclaration(PsiBuilder b, int l) {
		return operationDeclaration(b, l + 1);
	}
	public static boolean eolStatementBlock(PsiBuilder b, int l) {
		return statementBlock(b, l + 1);
	}
	public static boolean eolStatement(PsiBuilder b, int l) {
		return statement(b, l + 1);
	}
	public static boolean eolTypeName(PsiBuilder b, int l) {
		return typeName(b, l + 1);
	}
	public static boolean eolFormalParameter(PsiBuilder b, int l) {
		return formalParameter(b, l + 1);
	}
	public static boolean eolFormalParameterList(PsiBuilder b, int l) {
		return formalParameterList(b, l + 1);
	}
	public static boolean eolExpr(PsiBuilder b, int l) {
		return expr(b, l + 1);
	}

	// ERL
	public static boolean erlGuard(PsiBuilder b, int l) {
		return guard(b, l + 1);
	}
	public static boolean erlExtend(PsiBuilder b, int l) {
		return extends_$(b, l + 1);
	}


	// Injection methods as access types are reused across languages.

	@Nullable
	public static EolStatementBlock getStatementBlockInternal(@NotNull PsiElement element) {
		return PsiTreeUtil.findChildOfAnyType(element, false, EolStatementBlock.class);
	}

	@NotNull
	public static List<EolAnnotationBlock> getAnnotationBlockListInternal(@NotNull PsiElement element) {
		return PsiTreeUtil.getChildrenOfTypeAsList(element, EolAnnotationBlock.class);
	}

	@NotNull
	public static List<EolOperationDeclaration> getOperationDeclarationListInternal(@NotNull PsiElement element) {
		return PsiTreeUtil.getChildrenOfTypeAsList(element, EolOperationDeclaration.class);
	}

	@Nullable
	public static EolAnnotationBlock getAnnotationBlockInternal(@NotNull PsiElement element) {
		return PsiTreeUtil.findChildOfAnyType(element, false, EolAnnotationBlock.class);
	}

	@Nullable
	public static EolExpr getExprInternal(@NotNull PsiElement element) {
		return PsiTreeUtil.findChildOfAnyType(element, false, EolExpr.class);
	}

	@Nullable
	public static ErlGuard getGuardInternal(@NotNull PsiElement element) {
		return PsiTreeUtil.findChildOfAnyType(element, false, ErlGuard.class);
	}

	@Nullable
	public static PsiElement getGuardKeyInternal(@NotNull PsiElement element) {
		return findChildByType(ERL_GUARD_KEY, element);
	}

	@NotNull
	public static PsiElement getLeftBraceInternal(@NotNull PsiElement element) {
		return findNotNullChildByType(EOL_LEFT_BRACE, element);
	}

	@NotNull
	public static PsiElement getRightBraceInternal(@NotNull PsiElement element) {
		return findNotNullChildByType(EOL_RIGHT_BRACE, element);
	}

	@Nullable
	private static <T extends PsiElement> T findChildByType(IElementType type, PsiElement element) {
		ASTNode node = element.getNode().findChildByType(type);
		return node == null ? null : (T)node.getPsi();
	}

	@NotNull
	private static <T extends PsiElement> T findNotNullChildByType(IElementType type, PsiElement element) {
		return notNullChild(findChildByType(type, element), element);
	}

	@NotNull
	private static <T> T notNullChild(T child, PsiElement element) {
		if (child == null) {
			LOG.error(element.getText() + "\n parent=" + element.getParent().getText());
		}
		return child;
	}

}

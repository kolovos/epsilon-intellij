/*******************************************************************************
 * Copyright (c) 2008 The University of York.
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *     Horacio Hoyos Rodriguez - initial API and implementation
 ******************************************************************************/
package org.eclipse.epislon.labs.intellij.grammar.parser.evl;

import com.intellij.lang.parser.GeneratedParserUtilBase;
import com.intellij.psi.PsiElement;
import org.eclipse.epislon.labs.intellij.grammar.psi.eol.EolAnnotationBlock;
import org.eclipse.epislon.labs.intellij.grammar.psi.eol.EolExpr;
import org.eclipse.epislon.labs.intellij.grammar.psi.eol.EolOperationDeclaration;
import org.eclipse.epislon.labs.intellij.grammar.psi.eol.EolStatementBlock;
import org.eclipse.epislon.labs.intellij.grammar.psi.evl.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

import static org.eclipse.epislon.labs.intellij.grammar.parser.EpsilonParserUtil.*;

public class EvlParserUtil extends GeneratedParserUtilBase {

	/** Method injection */

	@Nullable
	public static EolExpr getExpr(@NotNull EvlGuard guard) {
		return getExprInternal(guard);
	}

	@Nullable
	public static EolExpr getExpr(@NotNull EvlCheck check) {
		return getExprInternal(check);
	}

	@Nullable
	public static EolExpr getExpr(@NotNull EvlMessage message) {
		return getExprInternal(message);
	}

	@Nullable
	public static EolExpr getExpr(@NotNull EvlTitle title) {
		return getExprInternal(title);
	}

	@Nullable
	public static EolExpr getExpr(@NotNull EvlFixBody fixBody) {
		return getExprInternal(fixBody);
	}

	@NotNull
	public static EolStatementBlock getStatementBlock(@NotNull EvlPre pre) {
		return getStatementBlockInternal(pre);
	}

	@NotNull
	public static EolStatementBlock getStatementBlock(@NotNull EvlPost post) {
		return getStatementBlockInternal(post);
	}

	@Nullable
	public static EolStatementBlock getStatementBlock(@NotNull EvlGuard guard) {
		return getStatementBlockInternal(guard);
	}

	@Nullable
	public static EolStatementBlock getStatementBlock(@NotNull EvlCheck check) {
		return getStatementBlockInternal(check);
	}

	@Nullable
	public static EolStatementBlock getStatementBlock(@NotNull EvlMessage message) {
		return getStatementBlockInternal(message);
	}

	@Nullable
	public static EolStatementBlock getStatementBlock(@NotNull EvlFixBody fixBody) {
		return getStatementBlockInternal(fixBody);
	}

	@Nullable
	public static EolStatementBlock getStatementBlock(@NotNull EvlTitle title) {
		return getStatementBlockInternal(title);
	}

	@NotNull
	public static List<EolAnnotationBlock>  getAnnotationBlockList(@NotNull EvlContent content) {
		return getAnnotationBlockListInternal(content);
	}

	@NotNull
	public static List<EolOperationDeclaration>  getOperationDeclarationList(@NotNull EvlContent content) {
		return getOperationDeclarationListInternal(content);
	}

	@Nullable
	public static EolAnnotationBlock  getAnnotationBlock(@NotNull EvlContextContent content) {
		return getAnnotationBlockInternal(content);
	}

	@NotNull
	public static PsiElement getLeftBrace(@NotNull EvlContextBlock contextBlock) {
		return getLeftBraceInternal(contextBlock);
	}

	@NotNull
	public static PsiElement getLeftBrace(@NotNull EvlInvariantBlock contextBlock) {
		return getLeftBraceInternal(contextBlock);
	}

	@NotNull
	public static PsiElement getLeftBrace(@NotNull EvlFixBlock contextBlock) {
		return getLeftBraceInternal(contextBlock);
	}

	@Nullable
	public static PsiElement getGuardKey(@NotNull EvlContextBlock contextBlock) {
		return getGuardKeyInternal(contextBlock);
	}

	@Nullable
	public static PsiElement getGuardKey(@NotNull EvlInvariantBlock contextBlock) {
		return getGuardKeyInternal(contextBlock);
	}

	@Nullable
	public static PsiElement getGuardKey(@NotNull EvlFixBlock contextBlock) {
		return getGuardKeyInternal(contextBlock);
	}

	@NotNull
	public static PsiElement getRightBrace(@NotNull EvlContextBlock contextBlock) {
		return getRightBraceInternal(contextBlock);
	}

	@NotNull
	public static PsiElement getRightBrace(@NotNull EvlInvariantBlock contextBlock) {
		return getRightBraceInternal(contextBlock);
	}

	@NotNull
	public static PsiElement getRightBrace(@NotNull EvlFixBlock contextBlock) {
		return getRightBraceInternal(contextBlock);
	}
}

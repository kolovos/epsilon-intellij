/*******************************************************************************
 * Copyright (c) 2008 The University of York.
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *     Horacio Hoyos Rodriguez - initial API and implementation
 ******************************************************************************/
package org.eclipse.epislon.labs.intellij.grammar.parser.erl;

import com.intellij.lang.parser.GeneratedParserUtilBase;
import org.eclipse.epislon.labs.intellij.grammar.psi.eol.EolAnnotationBlock;
import org.eclipse.epislon.labs.intellij.grammar.psi.eol.EolExpr;
import org.eclipse.epislon.labs.intellij.grammar.psi.eol.EolOperationDeclaration;
import org.eclipse.epislon.labs.intellij.grammar.psi.eol.EolStatementBlock;
import org.eclipse.epislon.labs.intellij.grammar.psi.erl.ErlContent;
import org.eclipse.epislon.labs.intellij.grammar.psi.erl.ErlGuard;
import org.eclipse.epislon.labs.intellij.grammar.psi.erl.ErlPost;
import org.eclipse.epislon.labs.intellij.grammar.psi.erl.ErlPre;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

import static org.eclipse.epislon.labs.intellij.grammar.parser.EpsilonParserUtil.*;

public class ErlParserUtil extends GeneratedParserUtilBase {

	/** Method injection */

	@Nullable
	public static EolExpr getExpr(@NotNull ErlGuard guard) {
		return getExprInternal(guard);
	}

	@NotNull
	public static EolStatementBlock getStatementBlock(@NotNull ErlPre pre) {
		return getStatementBlockInternal(pre);
	}

	@NotNull
	public static EolStatementBlock getStatementBlock(@NotNull ErlPost post) {
		return getStatementBlockInternal(post);
	}

	@Nullable
	public static EolStatementBlock getStatementBlock(@NotNull ErlGuard guard) {
		return getStatementBlockInternal(guard);
	}

	@NotNull
	public static List<EolAnnotationBlock>  getAnnotationBlockList(@NotNull ErlContent content) {
		return getAnnotationBlockListInternal(content);
	}

	@NotNull
	public static List<EolOperationDeclaration>  getOperationDeclarationList(@NotNull ErlContent content) {
		return getOperationDeclarationListInternal(content);
	}


}

/*******************************************************************************
 * Copyright (c) 2008 The University of York.
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *     Horacio Hoyos Rodriguez - initial API and implementation
 ******************************************************************************/
package org.eclipse.epislon.labs.intellij.grammar.parser.ecl;

import com.intellij.lang.parser.GeneratedParserUtilBase;
import com.intellij.psi.PsiElement;
import org.eclipse.epislon.labs.intellij.grammar.psi.ecl.*;
import org.eclipse.epislon.labs.intellij.grammar.psi.eol.EolAnnotationBlock;
import org.eclipse.epislon.labs.intellij.grammar.psi.eol.EolExpr;
import org.eclipse.epislon.labs.intellij.grammar.psi.eol.EolOperationDeclaration;
import org.eclipse.epislon.labs.intellij.grammar.psi.eol.EolStatementBlock;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

import static org.eclipse.epislon.labs.intellij.grammar.parser.EpsilonParserUtil.*;

public class EclParserUtil extends GeneratedParserUtilBase {

	@Nullable
	public static EolExpr getExpr(@NotNull EclGuard guard) {
		return getExprInternal(guard);
	}

	@Nullable
	public static EolExpr getExpr(@NotNull EclCompareBlock compareBlock) {
		return getExprInternal(compareBlock);
	}

	@NotNull
	public static EolStatementBlock getStatementBlock(@NotNull EclPre pre) {
		return getStatementBlockInternal(pre);
	}

	@NotNull
	public static EolStatementBlock getStatementBlock(@NotNull EclPost post) {
		return getStatementBlockInternal(post);
	}

	@Nullable
	public static EolStatementBlock getStatementBlock(@NotNull EclGuard guard) {
		return getStatementBlockInternal(guard);
	}

	@Nullable
	public static EolStatementBlock getStatementBlock(@NotNull EclCompareBlock compareBlock) {
		return getStatementBlockInternal(compareBlock);
	}

	@Nullable
	public static EolStatementBlock getStatementBlock(@NotNull EclDoBlock doBlock) {
		return getStatementBlockInternal(doBlock);
	}

	@NotNull
	public static List<EolAnnotationBlock>  getAnnotationBlockList(@NotNull EclContent content) {
		return getAnnotationBlockListInternal(content);
	}

	@NotNull
	public static List<EolOperationDeclaration>  getOperationDeclarationList(@NotNull EclContent content) {
		return getOperationDeclarationListInternal(content);
	}

	@NotNull
	public static PsiElement getLeftBrace(@NotNull EclMatchRule contextBlock) {
		return getLeftBraceInternal(contextBlock);
	}


}

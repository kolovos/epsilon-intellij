// THIS FILE IS GENERATED. DO NOT MODIFY IT
/*******************************************************************************
 * Copyright (c) 2008 The University of York.
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *     Horacio Hoyos Rodriguez - initial API and implementation
 ******************************************************************************/
package org.eclipse.epislon.labs.intellij.grammar.parser.egl;

import com.intellij.lexer.FlexAdapter;

import java.io.Reader;

public class EglLexerAdapter extends FlexAdapter {
	public EglLexerAdapter() {
		super(new _EglLexer((Reader) null));
	}
}
// THIS FILE IS GENERATED. DO NOT MODIFY IT
/*******************************************************************************
 * Copyright (c) 2008 The University of York.
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *     Horacio Hoyos Rodriguez - initial API and implementation
 ******************************************************************************/
package org.eclipse.epislon.labs.intellij.grammar.editor.egl;

import com.intellij.openapi.fileTypes.SyntaxHighlighter;
import com.intellij.openapi.options.colors.AttributesDescriptor;
import icons.EpsilonIcons;
import org.eclipse.epislon.labs.intellij.grammar.editor.eol.EolColorSettingsPage;
import org.eclipse.epislon.labs.intellij.grammar.editor.eol.EolSyntaxHighlighter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.swing.*;

public class EglColorSettingsPage extends EolColorSettingsPage {

	private static final AttributesDescriptor[] DESCRIPTORS = new AttributesDescriptor[]{
			new AttributesDescriptor("Operator", EolSyntaxHighlighter.EOL_OPERATORS),
			new AttributesDescriptor("Text Operator", EolSyntaxHighlighter.EOL_TEXT_OPERATORS),
			new AttributesDescriptor("Keyword", EolSyntaxHighlighter.EOL_KEYWORD),
			new AttributesDescriptor("Annotation", EolSyntaxHighlighter.EOL_ANNOTATIONS),
			new AttributesDescriptor("String", EolSyntaxHighlighter.EOL_STRING),
			new AttributesDescriptor("Number", EolSyntaxHighlighter.EOL_NUMBERS),
			new AttributesDescriptor("Built-in", EolSyntaxHighlighter.EOL_BUILTIN),
			new AttributesDescriptor("Verbatim", EglSyntaxHighlighter.VERBATIM_TEXT),
			new AttributesDescriptor("Code Limits", EglSyntaxHighlighter.TEMPLATE_DELIM),
	};

	@Nullable
	@Override
	public Icon getIcon() {
		return EpsilonIcons.EGL_FILE;
	}

	@NotNull
	@Override
	public AttributesDescriptor[] getAttributeDescriptors() {
		return DESCRIPTORS;
	}

	@NotNull
	@Override
	public SyntaxHighlighter getHighlighter() {
		return new EglSyntaxHighlighter();
	}

	@NotNull
	@Override
	public String getDemoText() {
		return "This is a demo EGL file\n" +
		       "<h1>Book [%=index%]: [%=book.a_title%]</h1>\n" +
		       "\n" +
		       "<h2>Authors</h2>\n" +
		       "<ul>\n" +
		       "[%for (author in book.c_author) { \n" +
				"  var class = \"text-right\"%]\n" +
				"  var width = 100; %]\n" +
		       "  <li class=\"[%=class%]\" style=\"width=[%=width%]%\">[%=author.text%]\n" +
		       "[%}%]\n" +
		       "</ul>";

	}

	@NotNull
	@Override
	public String getDisplayName() {
		return "EGL";
	}
}

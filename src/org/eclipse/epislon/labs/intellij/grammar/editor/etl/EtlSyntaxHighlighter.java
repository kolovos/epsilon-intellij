// THIS FILE IS GENERATED. DO NOT MODIFY IT
/*******************************************************************************
 * Copyright (c) 2008 The University of York.
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *     Horacio Hoyos Rodriguez - initial API and implementation
 ******************************************************************************/
package org.eclipse.epislon.labs.intellij.grammar.editor.etl;

import com.intellij.lexer.Lexer;
import com.intellij.openapi.editor.colors.TextAttributesKey;
import com.intellij.psi.tree.IElementType;
import org.eclipse.epislon.labs.intellij.grammar.editor.erl.ErlSyntaxHighlighter;
import org.eclipse.epislon.labs.intellij.grammar.parser.etl.EtlLexerAdapter;
import org.eclipse.epislon.labs.intellij.grammar.psi.etl.EtlTypes;
import org.jetbrains.annotations.NotNull;

public class EtlSyntaxHighlighter extends ErlSyntaxHighlighter {

	@NotNull
	@Override
	public Lexer getHighlightingLexer() {
		return new EtlLexerAdapter();
	}

	@NotNull
	@Override
	public TextAttributesKey[] getTokenHighlights(IElementType tokenType) {
		if (tokenType.equals(EtlTypes.ETL_RULE_KEY)
            || tokenType.equals(EtlTypes.ETL_TRANSFORM_KEY)
            || tokenType.equals(EtlTypes.ETL_TO_KEY)
		) {
			return EOL_KEYWORD_KEYS;
		}
		else {
/* protected region superTokens on begin */
    return super.getTokenHighlights(tokenType);
/* protected region superTokens end */
		}
		
	}
}
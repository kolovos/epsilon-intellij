// THIS FILE IS GENERATED. DO NOT MODIFY IT
/*******************************************************************************
 * Copyright (c) 2008 The University of York.
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *     Horacio Hoyos Rodriguez - initial API and implementation
 ******************************************************************************/
package org.eclipse.epislon.labs.intellij.grammar.editor.egl;

import com.intellij.lexer.Lexer;
import com.intellij.openapi.editor.DefaultLanguageHighlighterColors;
import com.intellij.openapi.editor.colors.TextAttributesKey;
import com.intellij.psi.tree.IElementType;
import org.eclipse.epislon.labs.intellij.grammar.editor.eol.EolSyntaxHighlighter;
import org.eclipse.epislon.labs.intellij.grammar.parser.egl.EglLexerAdapter;
import org.eclipse.epislon.labs.intellij.grammar.psi.egl.EglTypes;
import org.jetbrains.annotations.NotNull;

import static com.intellij.openapi.editor.colors.TextAttributesKey.createTextAttributesKey;

public class EglSyntaxHighlighter extends EolSyntaxHighlighter {

	public static final TextAttributesKey TEMPLATE_DELIM =
			createTextAttributesKey("TEMPLATE_DELIM", DefaultLanguageHighlighterColors.CONSTANT);

	public static final TextAttributesKey VERBATIM_TEXT =
			createTextAttributesKey("VERBATIM_TEXT", DefaultLanguageHighlighterColors.NUMBER);

	public static final TextAttributesKey[] TEMPLATE_DELIM_KEYS = new TextAttributesKey[]{TEMPLATE_DELIM};
	public static final TextAttributesKey[] VERBATIM_TEXT_KEYS = new TextAttributesKey[]{VERBATIM_TEXT};

	@NotNull
	@Override
	public Lexer getHighlightingLexer() {
		return new EglLexerAdapter();
	}

	@NotNull
	@Override
	public TextAttributesKey[] getTokenHighlights(IElementType tokenType) {
/* protected region superTokens on begin */
		if (tokenType.equals(EglTypes.EGL_EXTENDS_KEY)
				|| tokenType.equals(EglTypes.EGL_GUARD_KEY)
				|| tokenType.equals(EglTypes.EGL_POST_KEY)
				|| tokenType.equals(EglTypes.EGL_PRE_KEY)
		) {
			return EOL_KEYWORD_KEYS;
		}
		if (tokenType.equals(EglTypes.EGL_ARROW_OP)
				|| tokenType.equals(EglTypes.EGL_ASSIGN_OP)
				|| tokenType.equals(EglTypes.EGL_COLON_OP)
				|| tokenType.equals(EglTypes.EGL_DIV_ASSIG_OP)
				|| tokenType.equals(EglTypes.EGL_DIV_OP)
				|| tokenType.equals(EglTypes.EGL_ENUM_OP)
				|| tokenType.equals(EglTypes.EGL_EQ_OP)
				|| tokenType.equals(EglTypes.EGL_EQUIV_ASSIGN_OP)
				|| tokenType.equals(EglTypes.EGL_GE_OP)
				|| tokenType.equals(EglTypes.EGL_GT_OP)
				|| tokenType.equals(EglTypes.EGL_IMPLIES_OP)
				|| tokenType.equals(EglTypes.EGL_IN_OP)
				|| tokenType.equals(EglTypes.EGL_INC_OP)
				|| tokenType.equals(EglTypes.EGL_LE_OP)
				|| tokenType.equals(EglTypes.EGL_LT_OP)
				|| tokenType.equals(EglTypes.EGL_MINUS_ASSIGN_OP)
				|| tokenType.equals(EglTypes.EGL_MODEL_OP)
				|| tokenType.equals(EglTypes.EGL_NE_OP)
				|| tokenType.equals(EglTypes.EGL_NEG_OP)
				|| tokenType.equals(EglTypes.EGL_PLUS_ASSIGN_OP)
				|| tokenType.equals(EglTypes.EGL_PLUS_OP)
				|| tokenType.equals(EglTypes.EGL_POINT_OP)
				|| tokenType.equals(EglTypes.EGL_POINT_POINT_OP)
				|| tokenType.equals(EglTypes.EGL_QUAL_OP)
				|| tokenType.equals(EglTypes.EGL_SPECIAL_ASSIGN_OP)
				|| tokenType.equals(EglTypes.EGL_THEN_OP)
				|| tokenType.equals(EglTypes.EGL_TIMES_ASSIGN_OP)
				|| tokenType.equals(EglTypes.EGL_TIMES_OP)
		) {
			return EOL_OPERATOR_KEYS;
		}
		else if(tokenType.equals(EglTypes.EGL_AND_OP)
				|| tokenType.equals(EglTypes.EGL_NOT_OP)
				|| tokenType.equals(EglTypes.EGL_OR_OP)
				|| tokenType.equals(EglTypes.EGL_XOR_OP)
		) {
			return EOL_TEXT_OPERATOR_KEYS;
		}
		else if (tokenType.equals(EglTypes.EGL_ABORT_KEY)
				|| tokenType.equals(EglTypes.EGL_BAG_KEY)
				|| tokenType.equals(EglTypes.EGL_BREAK_ALL_KEY)
				|| tokenType.equals(EglTypes.EGL_BREAK_KEY)
				|| tokenType.equals(EglTypes.EGL_CASE_KEY)
				|| tokenType.equals(EglTypes.EGL_COL_KEY)
				|| tokenType.equals(EglTypes.EGL_CONTINUE_KEY)
				|| tokenType.equals(EglTypes.EGL_DEFAULT_KEY)
				|| tokenType.equals(EglTypes.EGL_DELETE_KEY)
				|| tokenType.equals(EglTypes.EGL_ELSE_KEY)
				|| tokenType.equals(EglTypes.EGL_EXT_KEY)
				|| tokenType.equals(EglTypes.EGL_FALSE_KEY)
				|| tokenType.equals(EglTypes.EGL_FOR_KEY)
				|| tokenType.equals(EglTypes.EGL_FUNCTION_KEY)
				|| tokenType.equals(EglTypes.EGL_IF_KEY)
				|| tokenType.equals(EglTypes.EGL_IMPORT_KEY)
				|| tokenType.equals(EglTypes.EGL_IN_KEY)
				|| tokenType.equals(EglTypes.EGL_LIST_KEY)
				|| tokenType.equals(EglTypes.EGL_MAP_KEY)
				|| tokenType.equals(EglTypes.EGL_NATIVE_KEY)
				|| tokenType.equals(EglTypes.EGL_NEW_KEY)
				|| tokenType.equals(EglTypes.EGL_OPERATION_KEY)
				|| tokenType.equals(EglTypes.EGL_ORDSET_KEY)
				|| tokenType.equals(EglTypes.EGL_RETURN_KEY)
				|| tokenType.equals(EglTypes.EGL_SEQ_KEY)
				|| tokenType.equals(EglTypes.EGL_SET_KEY)
				|| tokenType.equals(EglTypes.EGL_SWITCH_KEY)
				|| tokenType.equals(EglTypes.EGL_THROW_KEY)
				|| tokenType.equals(EglTypes.EGL_TRANS_KEY)
				|| tokenType.equals(EglTypes.EGL_TRUE_KEY)
				|| tokenType.equals(EglTypes.EGL_VAR_KEY)
				|| tokenType.equals(EglTypes.EGL_WHILE_KEY)
		) {
			return EOL_KEYWORD_KEYS;
		}
		else if (tokenType.equals(EglTypes.EGL_ANNOTATION)
				|| tokenType.equals(EglTypes.EGL_EXEC_ANNOT)
				|| tokenType.equals(EglTypes.EGL_EXECUTABLE_ANNOTATION)) {
			return EOL_ANNOTATIONS_KEYS;
		}
		else if (tokenType.equals(EglTypes.EGL_FLOAT)) {
			return EOL_NUMBER_KEYS;
		}
		else if (tokenType.equals(EglTypes.EGL_SELF_BIN)
				|| tokenType.equals(EglTypes.EGL_LOOP_CNT_BIN)
				|| tokenType.equals(EglTypes.EGL_HAS_MORE_BIN)) {
			return EOL_BUILTIN_KEYS;
		}
		else if (tokenType.equals(EglTypes.EGL_STRING)) {
			return EOL_STRING_KEYS;
		}
		else if (tokenType.equals(EglTypes.EGL_BLOCK_COMMENT) || tokenType.equals(EglTypes.EGL_LINE_COMMENT)) {
			return EOL_COMMENT_KEYS;
		}
		else if (tokenType.equals(EglTypes.EGL_CODE_LIM)) {
			return TEMPLATE_DELIM_KEYS;
		}
		else if (tokenType.equals(EglTypes.EGL_VERBATIM_TEXT)) {
			return VERBATIM_TEXT_KEYS;
		}
		else {
			return EMPTY_KEYS;
		}
/* protected region superTokens end */
	}
}
// THIS FILE IS GENERATED. DO NOT MODIFY IT
/*******************************************************************************
 * Copyright (c) 2008 The University of York.
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *     Horacio Hoyos Rodriguez - initial API and implementation
 ******************************************************************************/
package org.eclipse.epislon.labs.intellij.grammar.editor.ecl;

import com.intellij.openapi.fileTypes.SyntaxHighlighter;
import icons.EpsilonIcons;
import org.eclipse.epislon.labs.intellij.grammar.editor.eol.EolColorSettingsPage;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.swing.*;

public class EclColorSettingsPage extends EolColorSettingsPage {

	@Nullable
	@Override
	public Icon getIcon() {
		return EpsilonIcons.ECL_FILE;
	}

	@NotNull
	@Override
	public SyntaxHighlighter getHighlighter() {
		return new EclSyntaxHighlighter();
	}

	@NotNull
	@Override
	public String getDemoText() {
		return "// This is a demo ECL file\n" +
		       "\n" +
		       "pre {\n" +
		       "\tvar weight = 3.24\n" +
		       "}\n" +
		       "\n" +
		       "/** Match nodes of the simple Tree metamodel */\n" +
		       "rule Tree2Tree match l : T1!Tree\n" +
		       "\twith r : T2!Tree {\n" +
		       "\tcompare : l.label = r.label and\n" +
		       "\t\tl.parent.matches(r.parent) and\n" +
		       "\t\tl.children.matches(r.children)\n" +
		       "/t}\n";

	}

	@NotNull
	@Override
	public String getDisplayName() {
		return "ECL";
	}
}

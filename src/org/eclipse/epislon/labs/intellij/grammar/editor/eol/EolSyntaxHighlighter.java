/*******************************************************************************
 * Copyright (c) 2008 The University of York.
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *     Horacio Hoyos Rodriguez - initial API and implementation
 ******************************************************************************/
package org.eclipse.epislon.labs.intellij.grammar.editor.eol;

import com.intellij.lexer.Lexer;
import com.intellij.openapi.editor.DefaultLanguageHighlighterColors;
import com.intellij.openapi.editor.HighlighterColors;
import com.intellij.openapi.editor.colors.TextAttributesKey;
import com.intellij.openapi.fileTypes.SyntaxHighlighterBase;
import com.intellij.psi.TokenType;
import com.intellij.psi.tree.IElementType;
import org.eclipse.epislon.labs.intellij.grammar.parser.eol.EolLexerAdapter;
import org.eclipse.epislon.labs.intellij.grammar.psi.eol.EolTypes;
import org.jetbrains.annotations.NotNull;

import static com.intellij.openapi.editor.colors.TextAttributesKey.createTextAttributesKey;

public class EolSyntaxHighlighter extends SyntaxHighlighterBase {
	public static final TextAttributesKey EOL_OPERATORS =
			createTextAttributesKey("EOL_OPERATORS", DefaultLanguageHighlighterColors.OPERATION_SIGN);
	public static final TextAttributesKey EOL_TEXT_OPERATORS =
			createTextAttributesKey("EOL_TEXT_OPERATORS", DefaultLanguageHighlighterColors.LOCAL_VARIABLE);
	public static final TextAttributesKey EOL_KEYWORD =
			createTextAttributesKey("EOL_KEYWORD", DefaultLanguageHighlighterColors.INSTANCE_METHOD);
	public static final TextAttributesKey EOL_ANNOTATIONS =
			createTextAttributesKey("EOL_ANNOTATIONS", DefaultLanguageHighlighterColors.METADATA);
	public static final TextAttributesKey EOL_NUMBERS =
			createTextAttributesKey("EOL_NUMBERS", DefaultLanguageHighlighterColors.NUMBER);
	public static final TextAttributesKey EOL_BUILTIN =
			createTextAttributesKey("EOL_BUILTIN", DefaultLanguageHighlighterColors.FUNCTION_CALL);
	public static final TextAttributesKey EOL_STRING =
			createTextAttributesKey("STRING", DefaultLanguageHighlighterColors.STRING);
	public static final TextAttributesKey EOL_COMMENT =
			createTextAttributesKey("SIMPLE_COMMENT", DefaultLanguageHighlighterColors.LINE_COMMENT);
	public static final TextAttributesKey BAD_CHARACTER =
			createTextAttributesKey("SIMPLE_BAD_CHARACTER", HighlighterColors.BAD_CHARACTER);


	public static final TextAttributesKey[] EOL_OPERATOR_KEYS = new TextAttributesKey[]{EOL_OPERATORS};
	public static final TextAttributesKey[] EOL_TEXT_OPERATOR_KEYS = new TextAttributesKey[]{EOL_TEXT_OPERATORS};
	public static final TextAttributesKey[] EOL_KEYWORD_KEYS = new TextAttributesKey[]{EOL_KEYWORD};
	public static final TextAttributesKey[] EOL_ANNOTATIONS_KEYS = new TextAttributesKey[]{EOL_ANNOTATIONS};
	public static final TextAttributesKey[] EOL_NUMBER_KEYS = new TextAttributesKey[]{EOL_NUMBERS};
	public static final TextAttributesKey[] EOL_BUILTIN_KEYS = new TextAttributesKey[]{EOL_BUILTIN};
	public static final TextAttributesKey[] EOL_STRING_KEYS = new TextAttributesKey[]{EOL_STRING};
	public static final TextAttributesKey[] EOL_COMMENT_KEYS = new TextAttributesKey[]{EOL_COMMENT};
	public static final TextAttributesKey[] EMPTY_KEYS = new TextAttributesKey[0];
	private static final TextAttributesKey[] BAD_CHAR_KEYS = new TextAttributesKey[]{BAD_CHARACTER};

	@NotNull
	@Override
	public Lexer getHighlightingLexer() {
		return new EolLexerAdapter();
	}

	@NotNull
	@Override
	public TextAttributesKey[] getTokenHighlights(IElementType tokenType) {
		if (tokenType.equals(EolTypes.EOL_ARROW_OP)
			|| tokenType.equals(EolTypes.EOL_ASSIGN_OP)
			|| tokenType.equals(EolTypes.EOL_COLON_OP)
			|| tokenType.equals(EolTypes.EOL_DIV_ASSIG_OP)
			|| tokenType.equals(EolTypes.EOL_DIV_OP)
			|| tokenType.equals(EolTypes.EOL_ENUM_OP)
			|| tokenType.equals(EolTypes.EOL_EQ_OP)
			|| tokenType.equals(EolTypes.EOL_EQUIV_ASSIGN_OP)
			|| tokenType.equals(EolTypes.EOL_GE_OP)
			|| tokenType.equals(EolTypes.EOL_GT_OP)
			|| tokenType.equals(EolTypes.EOL_IMPLIES_OP)
			|| tokenType.equals(EolTypes.EOL_IN_OP)
			|| tokenType.equals(EolTypes.EOL_INC_OP)
			|| tokenType.equals(EolTypes.EOL_LE_OP)
			|| tokenType.equals(EolTypes.EOL_LT_OP)
			|| tokenType.equals(EolTypes.EOL_MINUS_ASSIGN_OP)
			|| tokenType.equals(EolTypes.EOL_MODEL_OP)
			|| tokenType.equals(EolTypes.EOL_NE_OP)
			|| tokenType.equals(EolTypes.EOL_NEG_OP)
			|| tokenType.equals(EolTypes.EOL_PLUS_ASSIGN_OP)
			|| tokenType.equals(EolTypes.EOL_PLUS_OP)
			|| tokenType.equals(EolTypes.EOL_POINT_OP)
			|| tokenType.equals(EolTypes.EOL_POINT_POINT_OP)
			|| tokenType.equals(EolTypes.EOL_QUAL_OP)
			|| tokenType.equals(EolTypes.EOL_SPECIAL_ASSIGN_OP)
			|| tokenType.equals(EolTypes.EOL_THEN_OP)
			|| tokenType.equals(EolTypes.EOL_TIMES_ASSIGN_OP)
			|| tokenType.equals(EolTypes.EOL_TIMES_OP)
		) {
			return EOL_OPERATOR_KEYS;
		}
		else if(tokenType.equals(EolTypes.EOL_AND_OP)
				|| tokenType.equals(EolTypes.EOL_NOT_OP)
				|| tokenType.equals(EolTypes.EOL_OR_OP)
				|| tokenType.equals(EolTypes.EOL_XOR_OP)
		) {
			return EOL_TEXT_OPERATOR_KEYS;
		}
		else if (tokenType.equals(EolTypes.EOL_ABORT_KEY)
				|| tokenType.equals(EolTypes.EOL_BAG_KEY)
				|| tokenType.equals(EolTypes.EOL_BREAK_ALL_KEY)
				|| tokenType.equals(EolTypes.EOL_BREAK_KEY)
				|| tokenType.equals(EolTypes.EOL_CASE_KEY)
				|| tokenType.equals(EolTypes.EOL_COL_KEY)
				|| tokenType.equals(EolTypes.EOL_CONTINUE_KEY)
				|| tokenType.equals(EolTypes.EOL_DEFAULT_KEY)
				|| tokenType.equals(EolTypes.EOL_DELETE_KEY)
				|| tokenType.equals(EolTypes.EOL_ELSE_KEY)
				|| tokenType.equals(EolTypes.EOL_EXT_KEY)
				|| tokenType.equals(EolTypes.EOL_FALSE_KEY)
				|| tokenType.equals(EolTypes.EOL_FOR_KEY)
				|| tokenType.equals(EolTypes.EOL_FUNCTION_KEY)
				|| tokenType.equals(EolTypes.EOL_IF_KEY)
				|| tokenType.equals(EolTypes.EOL_IMPORT_KEY)
				|| tokenType.equals(EolTypes.EOL_IN_KEY)
				|| tokenType.equals(EolTypes.EOL_LIST_KEY)
				|| tokenType.equals(EolTypes.EOL_MAP_KEY)
				|| tokenType.equals(EolTypes.EOL_NATIVE_KEY)
				|| tokenType.equals(EolTypes.EOL_NEW_KEY)
				|| tokenType.equals(EolTypes.EOL_OPERATION_KEY)
				|| tokenType.equals(EolTypes.EOL_ORDSET_KEY)
				|| tokenType.equals(EolTypes.EOL_RETURN_KEY)
				|| tokenType.equals(EolTypes.EOL_SEQ_KEY)
				|| tokenType.equals(EolTypes.EOL_SET_KEY)
				|| tokenType.equals(EolTypes.EOL_SWITCH_KEY)
				|| tokenType.equals(EolTypes.EOL_THROW_KEY)
				|| tokenType.equals(EolTypes.EOL_TRANS_KEY)
				|| tokenType.equals(EolTypes.EOL_TRUE_KEY)
				|| tokenType.equals(EolTypes.EOL_VAR_KEY)
				|| tokenType.equals(EolTypes.EOL_WHILE_KEY)
				) {
			return EOL_KEYWORD_KEYS;
		}
		else if (tokenType.equals(EolTypes.EOL_ANNOTATION)
				|| tokenType.equals(EolTypes.EOL_EXEC_ANNOT)
				|| tokenType.equals(EolTypes.EOL_EXECUTABLE_ANNOTATION)) {
			return EOL_ANNOTATIONS_KEYS;
		}
		else if (tokenType.equals(EolTypes.EOL_FLOAT)) {
			return EOL_NUMBER_KEYS;
		}
		else if (tokenType.equals(EolTypes.EOL_SELF_BIN)
				|| tokenType.equals(EolTypes.EOL_LOOP_CNT_BIN)
				|| tokenType.equals(EolTypes.EOL_HAS_MORE_BIN)) {
			return EOL_BUILTIN_KEYS;
		}
		else if (tokenType.equals(EolTypes.EOL_STRING)) {
			return EOL_STRING_KEYS;
		}
		else if (tokenType.equals(EolTypes.EOL_BLOCK_COMMENT) || tokenType.equals(EolTypes.EOL_LINE_COMMENT)) {
			return EOL_COMMENT_KEYS;
		}
		else if (tokenType.equals(TokenType.BAD_CHARACTER)) {
			return BAD_CHAR_KEYS;
		}
		else {
			return EMPTY_KEYS;
		}
	}
}
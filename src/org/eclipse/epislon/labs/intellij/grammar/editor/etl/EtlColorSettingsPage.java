// THIS FILE IS GENERATED. DO NOT MODIFY IT
/*******************************************************************************
 * Copyright (c) 2008 The University of York.
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *     Horacio Hoyos Rodriguez - initial API and implementation
 ******************************************************************************/
package org.eclipse.epislon.labs.intellij.grammar.editor.etl;

import com.intellij.openapi.fileTypes.SyntaxHighlighter;
import icons.EpsilonIcons;
import org.eclipse.epislon.labs.intellij.grammar.editor.eol.EolColorSettingsPage;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.swing.*;

public class EtlColorSettingsPage extends EolColorSettingsPage {

	@Nullable
	@Override
	public Icon getIcon() {
		return EpsilonIcons.ETL_FILE;
	}

	@NotNull
	@Override
	public SyntaxHighlighter getHighlighter() {
		return new EtlSyntaxHighlighter();
	}

	@NotNull
	@Override
	public String getDemoText() {
		return "// This is a demo ETL file\n" +
		       "\n" +
		       "pre {\n" +
		       "\tvar weight = 3.24\n" +
		       "}\n" +
		       "\n" +
		       "/** Transform a Tree node to a Graph node */\n" +
		       "@greedy\n" +
		       "rule Tree2Node extends BranchToNode\n" +
		       "\ttransform t : Tree!Tree\n" +
		       "\tto n : Graph!Node {\n" +
		       "\t\tn.label = t.label;\n" +
		       "\t\tif (t.parent.isDefined()) {\n" +
		       "\t\t\tvar edge = new Graph!Edge;\n" +
		       "\t\t\tedge.source = n;\n" +
		       "\t\t\tedge.name = \"myEdge\";\n" +
		       "\t\t\tedge.target = t.parent.equivalent();\n" +
		       "\t\t}\n" +
		       "\t}\n";

	}

	@NotNull
	@Override
	public String getDisplayName() {
		return "ETL";
	}
}

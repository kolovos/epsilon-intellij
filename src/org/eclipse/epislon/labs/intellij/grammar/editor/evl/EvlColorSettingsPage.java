// THIS FILE IS GENERATED. DO NOT MODIFY IT
/*******************************************************************************
 * Copyright (c) 2008 The University of York.
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *     Horacio Hoyos Rodriguez - initial API and implementation
 ******************************************************************************/
package org.eclipse.epislon.labs.intellij.grammar.editor.evl;

import com.intellij.openapi.fileTypes.SyntaxHighlighter;
import icons.EpsilonIcons;
import org.eclipse.epislon.labs.intellij.grammar.editor.eol.EolColorSettingsPage;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.swing.*;

public class EvlColorSettingsPage extends EolColorSettingsPage {

	@Nullable
	@Override
	public Icon getIcon() {
		return EpsilonIcons.EVL_FILE;
	}

	@NotNull
	@Override
	public SyntaxHighlighter getHighlighter() {
		return new EvlSyntaxHighlighter();
	}

	@NotNull
	@Override
	public String getDemoText() {
		return "// This is a demo EVL file\n" +
		       "\n" +
		       "pre {\n" +
		       "\tvar weight = 3.24\n" +
		       "}\n" +
		       "\n" +
		       "@lazy\n" +
		       "context Singleton {\n" +
		       "\tguard : self.isSingleton\n" +
		       "\tconstraint DefinesGetInstance {\n" +
		       "\t\tcheck : self.getGetInstanceOperation().isDefined()\n" +
		       "\t\tmessage : \"Singleton \" + self.name\n" +
		       "\t\tfix {\n" +
		       "\t\t\ttitle : \"Add a getInstance() operation to \" + self.name\n" +
		       "\t\t\tdo {\n" +
		       "\t\t\t\t// Create the getInstance operation\n" +
		       "\t\t\t\tvar op : new Operation;\n" +
		       "\t\t\t\top.name = \"getInstance\n" +
		       "\t\t\t}\n" +
		       "\t\t}\n" +
		       "}";

	}

	@NotNull
	@Override
	public String getDisplayName() {
		return "EVL";
	}
}

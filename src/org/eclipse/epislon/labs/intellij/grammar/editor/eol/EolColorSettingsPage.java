/*******************************************************************************
 * Copyright (c) 2008 The University of York.
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *     Horacio Hoyos Rodriguez - initial API and implementation
 ******************************************************************************/
package org.eclipse.epislon.labs.intellij.grammar.editor.eol;

import com.intellij.openapi.editor.colors.TextAttributesKey;
import com.intellij.openapi.fileTypes.SyntaxHighlighter;
import com.intellij.openapi.options.colors.AttributesDescriptor;
import com.intellij.openapi.options.colors.ColorDescriptor;
import com.intellij.openapi.options.colors.ColorSettingsPage;
import icons.EpsilonIcons;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.swing.*;
import java.util.Map;

public class EolColorSettingsPage implements ColorSettingsPage {

	private static final AttributesDescriptor[] DESCRIPTORS = new AttributesDescriptor[]{
			new AttributesDescriptor("Operator", EolSyntaxHighlighter.EOL_OPERATORS),
			new AttributesDescriptor("Text Operator", EolSyntaxHighlighter.EOL_TEXT_OPERATORS),
			new AttributesDescriptor("Keyword", EolSyntaxHighlighter.EOL_KEYWORD),
			new AttributesDescriptor("Annotation", EolSyntaxHighlighter.EOL_ANNOTATIONS),
			new AttributesDescriptor("String", EolSyntaxHighlighter.EOL_STRING),
			new AttributesDescriptor("Number", EolSyntaxHighlighter.EOL_NUMBERS),
			new AttributesDescriptor("Built-in", EolSyntaxHighlighter.EOL_BUILTIN),
	};

	@Nullable
	@Override
	public Icon getIcon() {
		return EpsilonIcons.EOL_FILE;
	}

	@NotNull
	@Override
	public SyntaxHighlighter getHighlighter() {
		return new EolSyntaxHighlighter();
	}

	@NotNull
	@Override
	public String getDemoText() {
		return "// This is a demo eol file\n" +
				"\n" +
				"import \"other.eol\";" +
				"\n" +
				"/** Operations should have documentation */\n" +
				"operation add(a, b) { return a+b;}\n" +
				"\n" +
				"/**\n" +
				" * Better if javadoc style\n" +
				" */\n" +
				"@cached\n" +
				"$a = 2 + 3\n" +
				"operation M!Some::Type getInfo() : Sequence {\n" +
				"\tvar result : Set{1,2};\n" +
				"\tvar cont = 3.54;\n" +
				"   for (v in self.values) {\n" +
				"       if (hasMore and self.log) {\n" +
				"           result.add(v + loopCount);\n" +
				"           v.println(M!Some::Enum#One);\n" +
				"       }\n" +
				"   }\n" +
				"";

	}

	@Nullable
	@Override
	public Map<String, TextAttributesKey> getAdditionalHighlightingTagToDescriptorMap() {
		return null;
	}

	@NotNull
	@Override
	public AttributesDescriptor[] getAttributeDescriptors() {
		return DESCRIPTORS;
	}

	@NotNull
	@Override
	public ColorDescriptor[] getColorDescriptors() {
		return ColorDescriptor.EMPTY_ARRAY;
	}

	@NotNull
	@Override
	public String getDisplayName() {
		return "EOL";
	}
}

// THIS FILE IS GENERATED. DO NOT MODIFY IT
/*******************************************************************************
 * Copyright (c) 2008 The University of York.
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *     Horacio Hoyos Rodriguez - initial API and implementation
 ******************************************************************************/
package org.eclipse.epislon.labs.intellij.grammar.editor.ewl;

import com.intellij.openapi.fileTypes.SyntaxHighlighter;
import icons.EpsilonIcons;
import org.eclipse.epislon.labs.intellij.grammar.editor.eol.EolColorSettingsPage;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.swing.*;

public class EwlColorSettingsPage extends EolColorSettingsPage {

	@Nullable
	@Override
	public Icon getIcon() {
		return EpsilonIcons.EWL_FILE;
	}

	@NotNull
	@Override
	public SyntaxHighlighter getHighlighter() {
		return new EwlSyntaxHighlighter();
	}

	@NotNull
	@Override
	public String getDemoText() {
		return "// This is a demo EWL file\n" +
		       				"\n" +
		       				"pre {\n" +
		       				"\tvar weight = 3.24\n" +
		       				"}\n" +
		       				"\n" +
		       				"wizard ClassToSingleton {\n" +
		       				"\tguard : self.isTypeOf(Class)\n" +
		       				"\ttitle : \"Convert \" + self.name + \" to a singleton\"\n" +
		       				"\tdo {\n" +
		       				"\t\t// Create the getInstance() operation" +
		       				"\t\tvar gi = new Operation;\n" +
		       				"\t\tgi.name = \"getInstance\";\n" +
		       				"\t\tgi.owner = self\n" +
		       				"\t}\n";

	}

	@NotNull
	@Override
	public String getDisplayName() {
		return "EWL";
	}
}

// THIS FILE IS GENERATED. DO NOT MODIFY IT
/*******************************************************************************
 * Copyright (c) 2008 The University of York.
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *     Horacio Hoyos Rodriguez - initial API and implementation
 ******************************************************************************/
package org.eclipse.epislon.labs.intellij.grammar.editor.erl;

import com.intellij.openapi.editor.colors.TextAttributesKey;
import com.intellij.psi.tree.IElementType;
import org.eclipse.epislon.labs.intellij.grammar.editor.eol.EolSyntaxHighlighter;
import org.eclipse.epislon.labs.intellij.grammar.psi.erl.ErlTypes;
import org.jetbrains.annotations.NotNull;

public class ErlSyntaxHighlighter extends EolSyntaxHighlighter {


	@NotNull
	@Override
	public TextAttributesKey[] getTokenHighlights(IElementType tokenType) {
		if (tokenType.equals(ErlTypes.ERL_EXTENDS_KEY)
            || tokenType.equals(ErlTypes.ERL_GUARD_KEY)
            || tokenType.equals(ErlTypes.ERL_POST_KEY)
            || tokenType.equals(ErlTypes.ERL_PRE_KEY)
		) {
			return EOL_KEYWORD_KEYS;
		}
		else {
/* protected region superTokens on begin */
    return super.getTokenHighlights(tokenType);
/* protected region superTokens end */
		}
		
	}
}
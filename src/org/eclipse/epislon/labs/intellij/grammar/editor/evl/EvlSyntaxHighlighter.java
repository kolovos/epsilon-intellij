// THIS FILE IS GENERATED. DO NOT MODIFY IT
/*******************************************************************************
 * Copyright (c) 2008 The University of York.
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *     Horacio Hoyos Rodriguez - initial API and implementation
 ******************************************************************************/
package org.eclipse.epislon.labs.intellij.grammar.editor.evl;

import com.intellij.lexer.Lexer;
import com.intellij.openapi.editor.colors.TextAttributesKey;
import com.intellij.psi.tree.IElementType;
import org.eclipse.epislon.labs.intellij.grammar.editor.erl.ErlSyntaxHighlighter;
import org.eclipse.epislon.labs.intellij.grammar.parser.evl.EvlLexerAdapter;
import org.eclipse.epislon.labs.intellij.grammar.psi.evl.EvlTypes;
import org.jetbrains.annotations.NotNull;

public class EvlSyntaxHighlighter extends ErlSyntaxHighlighter {

	@NotNull
	@Override
	public Lexer getHighlightingLexer() {
		return new EvlLexerAdapter();
	}

	@NotNull
	@Override
	public TextAttributesKey[] getTokenHighlights(IElementType tokenType) {
		if (tokenType.equals(EvlTypes.EVL_CHECK_KEY)
            || tokenType.equals(EvlTypes.EVL_CONSTRAINT_KEY)
            || tokenType.equals(EvlTypes.EVL_CONTEXT_KEY)
            || tokenType.equals(EvlTypes.EVL_CRITIQUE_KEY)
            || tokenType.equals(EvlTypes.EVL_DO_KEY)
            || tokenType.equals(EvlTypes.EVL_FIX_KEY)
            || tokenType.equals(EvlTypes.EVL_MESSAGE_KEY)
            || tokenType.equals(EvlTypes.EVL_TITLE_KEY)
		) {
			return EOL_KEYWORD_KEYS;
		}
		else {
/* protected region superTokens on begin */
    return super.getTokenHighlights(tokenType);
/* protected region superTokens end */
		}
		
	}
}
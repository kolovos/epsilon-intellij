// THIS FILE IS GENERATED. DO NOT MODIFY IT
/*******************************************************************************
 * Copyright (c) 2008 The University of York.
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *     Horacio Hoyos Rodriguez - initial API and implementation
 ******************************************************************************/
package org.eclipse.epislon.labs.intellij.grammar.language.etl;

import com.intellij.openapi.fileTypes.LanguageFileType;
import icons.EpsilonIcons;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.swing.*;

public class EtlFileType extends LanguageFileType {

	public static final EtlFileType INSTANCE = new EtlFileType();

	private EtlFileType() {
		super(EtlLanguage.INSTANCE);
	}

	@NotNull
	@Override
	public String getName() {
		return "ETL file";
	}

	@NotNull
	@Override
	public String getDescription() {
		return "Epsilon Transformation Language file";
	}

	@NotNull
	@Override
	public String getDefaultExtension() {
		return "etl";
	}

	@Nullable
	@Override
	public Icon getIcon() {
		return EpsilonIcons.ETL_FILE;
	}
}

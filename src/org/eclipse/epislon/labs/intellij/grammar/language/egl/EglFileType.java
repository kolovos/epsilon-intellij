// THIS FILE IS GENERATED. DO NOT MODIFY IT
/*******************************************************************************
 * Copyright (c) 2008 The University of York.
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *     Horacio Hoyos Rodriguez - initial API and implementation
 ******************************************************************************/
package org.eclipse.epislon.labs.intellij.grammar.language.egl;

import com.intellij.openapi.fileTypes.LanguageFileType;
import icons.EpsilonIcons;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.swing.*;

public class EglFileType extends LanguageFileType {

	public static final EglFileType INSTANCE = new EglFileType();

	private EglFileType() {
		super(EglLanguage.INSTANCE);
	}

	@NotNull
	@Override
	public String getName() {
		return "EGL file";
	}

	@NotNull
	@Override
	public String getDescription() {
		return "Epsilon Generation Language file";
	}

	@NotNull
	@Override
	public String getDefaultExtension() {
		return "egl";
	}

	@Nullable
	@Override
	public Icon getIcon() {
		return EpsilonIcons.EGL_FILE;
	}
}

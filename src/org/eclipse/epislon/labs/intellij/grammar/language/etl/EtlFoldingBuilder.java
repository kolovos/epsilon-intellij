// THIS FILE IS GENERATED. DO NOT MODIFY IT
/*******************************************************************************
 * Copyright (c) 2008 The University of York.
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *     Horacio Hoyos Rodriguez - initial API and implementation
 ******************************************************************************/
package org.eclipse.epislon.labs.intellij.grammar.language.etl;

import com.intellij.lang.ASTNode;
import com.intellij.lang.folding.FoldingDescriptor;
import com.intellij.openapi.editor.Document;
import org.eclipse.epislon.labs.intellij.grammar.language.erl.ErlFoldingBuilder;
import org.eclipse.epislon.labs.intellij.grammar.psi.etl.*;
/* protected region imports on begin */
import com.intellij.openapi.util.TextRange;
import com.intellij.psi.PsiElement;
import com.intellij.psi.util.PsiTreeUtil;
/* protected region imports end */
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class EtlFoldingBuilder extends ErlFoldingBuilder {


	@NotNull
	@Override
	public FoldingDescriptor[] buildFoldRegions(@NotNull PsiElement root, @NotNull Document document, boolean quick) {

		List<FoldingDescriptor> descriptors = new ArrayList<>(Arrays.asList(super.buildFoldRegions(root, document, quick)));
		if (!quick) {
			/* protected region folds on begin */
			for (final EtlTransformationRule stmt : PsiTreeUtil.findChildrenOfAnyType(root, false, EtlTransformationRule.class)) {
				descriptors.add(new FoldingDescriptor(stmt,
						new TextRange(
								stmt.getLeftBrace().getTextRange().getEndOffset(),
								stmt.getTextRange().getEndOffset() - 1)));
			}
			for (final EtlPre stmt : PsiTreeUtil.findChildrenOfAnyType(root, false, EtlPre.class)) {
				descriptors.add(new FoldingDescriptor(stmt,
						new TextRange(
								stmt.getTextRange().getStartOffset() + 5,
								stmt.getTextRange().getEndOffset() - 1)));
			}
			for (final EtlPost stmt : PsiTreeUtil.findChildrenOfAnyType(root, false, EtlPost.class)) {
				descriptors.add(new FoldingDescriptor(stmt,
						new TextRange(
								stmt.getTextRange().getStartOffset() + 6,
								stmt.getTextRange().getEndOffset() - 1)));
			}
            /* protected region folds end */
		}
		return descriptors.toArray(new FoldingDescriptor[descriptors.size()]);
	}

	@Nullable
	@Override
	public String getPlaceholderText(@NotNull ASTNode node) {
		return "...";
	}
	
	@Override
	public boolean isCollapsedByDefault(@NotNull ASTNode node) {
		if (node.getElementType().equals(EtlTypes.ETL_MODULE)) {
			return true;
		}
		return false;
	}
}

// THIS FILE IS GENERATED. DO NOT MODIFY IT
/*******************************************************************************
 * Copyright (c) 2008 The University of York.
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *     Horacio Hoyos Rodriguez - initial API and implementation
 ******************************************************************************/
package org.eclipse.epislon.labs.intellij.grammar.language.evl;

import com.intellij.openapi.fileTypes.LanguageFileType;
import icons.EpsilonIcons;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.swing.*;

public class EvlFileType extends LanguageFileType {

	public static final EvlFileType INSTANCE = new EvlFileType();

	private EvlFileType() {
		super(EvlLanguage.INSTANCE);
	}

	@NotNull
	@Override
	public String getName() {
		return "EVL file";
	}

	@NotNull
	@Override
	public String getDescription() {
		return "Epsilon Validation Language file";
	}

	@NotNull
	@Override
	public String getDefaultExtension() {
		return "evl";
	}

	@Nullable
	@Override
	public Icon getIcon() {
		return EpsilonIcons.EVL_FILE;
	}
}

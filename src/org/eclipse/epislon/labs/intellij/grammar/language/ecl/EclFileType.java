// THIS FILE IS GENERATED. DO NOT MODIFY IT
/*******************************************************************************
 * Copyright (c) 2008 The University of York.
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *     Horacio Hoyos Rodriguez - initial API and implementation
 ******************************************************************************/
package org.eclipse.epislon.labs.intellij.grammar.language.ecl;

import com.intellij.openapi.fileTypes.LanguageFileType;
import icons.EpsilonIcons;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.swing.*;

public class EclFileType extends LanguageFileType {

	public static final EclFileType INSTANCE = new EclFileType();

	private EclFileType() {
		super(EclLanguage.INSTANCE);
	}

	@NotNull
	@Override
	public String getName() {
		return "ECL file";
	}

	@NotNull
	@Override
	public String getDescription() {
		return "Epsilon Comparison Language file";
	}

	@NotNull
	@Override
	public String getDefaultExtension() {
		return "ecl";
	}

	@Nullable
	@Override
	public Icon getIcon() {
		return EpsilonIcons.ECL_FILE;
	}
}

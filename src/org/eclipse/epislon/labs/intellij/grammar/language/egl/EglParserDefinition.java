// THIS FILE IS GENERATED. DO NOT MODIFY IT
/*******************************************************************************
 * Copyright (c) 2008 The University of York.
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *     Horacio Hoyos Rodriguez - initial API and implementation
 ******************************************************************************/
package org.eclipse.epislon.labs.intellij.grammar.language.egl;

import com.intellij.lang.ASTNode;
import com.intellij.lang.ParserDefinition;
import com.intellij.lang.PsiParser;
import com.intellij.lexer.Lexer;
import com.intellij.openapi.project.Project;
import com.intellij.psi.FileViewProvider;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiFile;
import com.intellij.psi.TokenType;
import com.intellij.psi.tree.IFileElementType;
import com.intellij.psi.tree.TokenSet;
import org.eclipse.epislon.labs.intellij.grammar.parser.egl.EglLexerAdapter;
import org.eclipse.epislon.labs.intellij.grammar.parser.egl.EglParser;
import org.eclipse.epislon.labs.intellij.grammar.psi.eol.EolTypes;
import org.eclipse.epislon.labs.intellij.grammar.psi.egl.EglFile;
import org.eclipse.epislon.labs.intellij.grammar.psi.egl.EglTypes;
import org.jetbrains.annotations.NotNull;

public class EglParserDefinition implements ParserDefinition {

	public static final TokenSet WHITE_SPACES = TokenSet.create(TokenType.WHITE_SPACE, EglTypes.EGL_CODE_LIM, EglTypes.EGL_VERBATIM_TEXT);
    public static final TokenSet COMMENTS = TokenSet.create(EglTypes.EGL_BLOCK_COMMENT, EglTypes.EGL_LINE_COMMENT);
	
	public static final IFileElementType FILE = new IFileElementType(EglLanguage.INSTANCE);

	@NotNull
	@Override
	public Lexer createLexer(Project project) {
		return new EglLexerAdapter();
	}

	@NotNull
	public TokenSet getWhitespaceTokens() {
		return WHITE_SPACES;
	}

	@NotNull
	public TokenSet getCommentTokens() {
		return COMMENTS;
	}

	@NotNull
	public TokenSet getStringLiteralElements() {
		return TokenSet.EMPTY;
	}

	@NotNull
	public PsiParser createParser(final Project project) {
		return new EglParser();
	}

	@Override
	public IFileElementType getFileNodeType() {
		return FILE;
	}

	public PsiFile createFile(FileViewProvider viewProvider) {
		return new EglFile(viewProvider);
	}

	public SpaceRequirements spaceExistenceTypeBetweenTokens(ASTNode left, ASTNode right) {
		return SpaceRequirements.MAY;
	}

	@NotNull
	public PsiElement createElement(ASTNode node) {
		return EglTypes.Factory.createElement(node);
	}
}

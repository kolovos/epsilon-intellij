/*******************************************************************************
 * Copyright (c) 2008 The University of York.
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *     Horacio Hoyos Rodriguez - initial API and implementation
 ******************************************************************************/
package org.eclipse.epislon.labs.intellij.grammar.language.eol;

import com.intellij.lang.ASTNode;
import com.intellij.lang.folding.*;
import com.intellij.openapi.editor.*;
import com.intellij.openapi.util.TextRange;
import com.intellij.psi.*;
import com.intellij.psi.util.PsiTreeUtil;
import org.eclipse.epislon.labs.intellij.grammar.psi.EpsilonModule;
import org.eclipse.epislon.labs.intellij.grammar.psi.eol.*;
import org.jetbrains.annotations.*;

import java.util.*;

public class EolFoldingBuilder extends FoldingBuilderEx {

	public class EolStatementBlockFolding extends FoldingDescriptor {

		public EolStatementBlockFolding(@NotNull ASTNode node, @NotNull TextRange range) {
			super(node, range);
		}

		@Nullable
		@Override
		public String getPlaceholderText() {
			return "...";
		}
	}

	public class ImportStatementBlockFolding extends FoldingDescriptor {

		public ImportStatementBlockFolding(@NotNull ASTNode node, @NotNull TextRange range) {
			super(node, range);
		}

		@Nullable
		@Override
		public String getPlaceholderText() {
			return "...";
		}
	}


	@NotNull
	@Override
	public FoldingDescriptor[] buildFoldRegions(@NotNull PsiElement root, @NotNull Document document, boolean quick) {

		List<FoldingDescriptor> descriptors = new ArrayList<>();
		// imports
		EpsilonModule module = PsiTreeUtil.findChildOfAnyType(root, false, EpsilonModule.class);
		if ((module != null) && !module.getImportStatementList().isEmpty()) {
			descriptors.add(new ImportStatementBlockFolding(
					module.getNode(),
					new TextRange(module.getImportStatementList().get(0).getTextRange().getStartOffset() + 7,
								module.getImportStatementList().get(module.getImportStatementList().size()-1).getTextRange().getEndOffset() + 1))
			);
		}
		if (!quick) {
			// all StatementBlocks are collapsible, and since most expressions with a block use a StatementBlock we cover
			// most required foldings
			for (final EolStatementBlock stmt : PsiTreeUtil.findChildrenOfAnyType(root, false, EolStatementBlock.class)) {
				createBlockFoldingDescriptor(descriptors, stmt, stmt.getNode());
			}
		}
		return descriptors.toArray(new FoldingDescriptor[descriptors.size()]);
	}

	@Nullable
	@Override
	public String getPlaceholderText(@NotNull ASTNode node) {
		return "...";
	}

	// Only calls for nodes that have a FoldingDescriptor
	@Override
	public boolean isCollapsedByDefault(@NotNull ASTNode node) {
		if (node.getElementType().equals(EolTypes.EOL_MODULE)) {
			return true;
		}
		return false;
	}

	protected void createBlockFoldingDescriptor(
			List<FoldingDescriptor> descriptors,
			EolStatementBlock statementBlock,
			ASTNode node) {
		if (statementBlock != null) {
			descriptors.add(new EolStatementBlockFolding(
					node,
					new TextRange(
							statementBlock.getTextRange().getStartOffset() + 1,
							statementBlock.getTextRange().getEndOffset() - 1)));
		}
	}
}

// THIS FILE IS GENERATED. DO NOT MODIFY IT
/*******************************************************************************
 * Copyright (c) 2008 The University of York.
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *     Horacio Hoyos Rodriguez - initial API and implementation
 ******************************************************************************/
package org.eclipse.epislon.labs.intellij.grammar.language.ecl;

import com.intellij.lang.ASTNode;
import com.intellij.lang.folding.FoldingDescriptor;
import com.intellij.openapi.editor.Document;
import org.eclipse.epislon.labs.intellij.grammar.language.erl.ErlFoldingBuilder;
import org.eclipse.epislon.labs.intellij.grammar.psi.ecl.*;
/* protected region imports on begin */
import com.intellij.openapi.util.TextRange;
import com.intellij.psi.PsiElement;
/* protected region imports end */
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class EclFoldingBuilder extends ErlFoldingBuilder {


	@NotNull
	@Override
	public FoldingDescriptor[] buildFoldRegions(@NotNull PsiElement root, @NotNull Document document, boolean quick) {

		List<FoldingDescriptor> descriptors = new ArrayList<>(Arrays.asList(super.buildFoldRegions(root, document, quick)));
		if (!quick) {
			/* protected region folds on begin */
            // Add required folding code
            /* protected region folds end */
		}
		return descriptors.toArray(new FoldingDescriptor[descriptors.size()]);
	}

	@Nullable
	@Override
	public String getPlaceholderText(@NotNull ASTNode node) {
		return "...";
	}
	
	@Override
	public boolean isCollapsedByDefault(@NotNull ASTNode node) {
		if (node.getElementType().equals(EclTypes.ECL_MODULE)) {
			return true;
		}
		return false;
	}
}

/*******************************************************************************
 * Copyright (c) 2008 The University of York.
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *     Horacio Hoyos Rodriguez - initial API and implementation
 ******************************************************************************/
package org.eclipse.epislon.labs.intellij.grammar.parser.ecl;

import com.intellij.lang.PsiBuilder;
import com.intellij.lang.PsiBuilder.Marker;
import static org.eclipse.epislon.labs.intellij.grammar.psi.ecl.EclTypes.*;
import static com.intellij.lang.parser.GeneratedParserUtilBase.*;
import com.intellij.psi.tree.IElementType;
import com.intellij.lang.ASTNode;
import com.intellij.psi.tree.TokenSet;
import com.intellij.lang.PsiParser;
import com.intellij.lang.LightPsiParser;
import static org.eclipse.epislon.labs.intellij.grammar.parser.EpsilonParserUtil.*;

@SuppressWarnings({"SimplifiableIfStatement", "UnusedAssignment"})
public class EclParser implements PsiParser, LightPsiParser {

  public ASTNode parse(IElementType t, PsiBuilder b) {
    parseLight(t, b);
    return b.getTreeBuilt();
  }

  public void parseLight(IElementType t, PsiBuilder b) {
    boolean r;
    b = adapt_builder_(t, b, this, null);
    Marker m = enter_section_(b, 0, _COLLAPSE_, null);
    r = parse_root_(t, b);
    exit_section_(b, 0, m, t, r, true, TRUE_CONDITION);
  }

  protected boolean parse_root_(IElementType t, PsiBuilder b) {
    return parse_root_(t, b, 0);
  }

  static boolean parse_root_(IElementType t, PsiBuilder b, int l) {
    return root(b, l + 1);
  }

  /* ********************************************************** */
  // 'compare' expressionOrStatementBlock
  public static boolean compareBlock(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "compareBlock")) return false;
    if (!nextTokenIs(b, ECL_COMPARE_KEY)) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = consumeToken(b, ECL_COMPARE_KEY);
    r = r && eolExprOrStatementBlock(b, l + 1);
    exit_section_(b, m, ECL_COMPARE_BLOCK, r);
    return r;
  }

  /* ********************************************************** */
  // (matchRule | pre | post | annotationBlock |  operationDeclaration)*
  public static boolean content(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "content")) return false;
    Marker m = enter_section_(b, l, _NONE_, ECL_CONTENT, "<content>");
    while (true) {
      int c = current_position_(b);
      if (!content_0(b, l + 1)) break;
      if (!empty_element_parsed_guard_(b, "content", c)) break;
    }
    exit_section_(b, l, m, true, false, null);
    return true;
  }

  // matchRule | pre | post | annotationBlock |  operationDeclaration
  private static boolean content_0(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "content_0")) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = matchRule(b, l + 1);
    if (!r) r = pre(b, l + 1);
    if (!r) r = post(b, l + 1);
    if (!r) r = eolAnnotationBlock(b, l + 1);
    if (!r) r = eolOperationDeclaration(b, l + 1);
    exit_section_(b, m, null, r);
    return r;
  }

  /* ********************************************************** */
  // 'do' statementBlock
  public static boolean doBlock(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "doBlock")) return false;
    if (!nextTokenIs(b, ECL_DO_KEY)) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = consumeToken(b, ECL_DO_KEY);
    r = r && eolStatementBlock(b, l + 1);
    exit_section_(b, m, ECL_DO_BLOCK, r);
    return r;
  }

  /* ********************************************************** */
  // erlExtend
  public static boolean extends_$(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "extends_$")) return false;
    boolean r;
    Marker m = enter_section_(b, l, _NONE_, ECL_EXTENDS, "<extends $>");
    r = erlExtend(b, l + 1);
    exit_section_(b, l, m, r, false, null);
    return r;
  }

  /* ********************************************************** */
  // eolFormalParameter
  public static boolean formalParameter(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "formalParameter")) return false;
    boolean r;
    Marker m = enter_section_(b, l, _NONE_, ECL_FORMAL_PARAMETER, "<formal parameter>");
    r = eolFormalParameter(b, l + 1);
    exit_section_(b, l, m, r, false, null);
    return r;
  }

  /* ********************************************************** */
  // erlGuard
  public static boolean guard(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "guard")) return false;
    boolean r;
    Marker m = enter_section_(b, l, _NONE_, ECL_GUARD, "<guard>");
    r = erlGuard(b, l + 1);
    exit_section_(b, l, m, r, false, null);
    return r;
  }

  /* ********************************************************** */
  // 'rule' ID 'match' formalParameter 'with' formalParameter
  // 	extends? EOL_LB guard? compareBlock? doBlock? EOL_RB
  public static boolean matchRule(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "matchRule")) return false;
    if (!nextTokenIs(b, ECL_RULE_KEY)) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = consumeToken(b, ECL_RULE_KEY);
    r = r && eolId(b, l + 1);
    r = r && consumeToken(b, ECL_MATCH_KEY);
    r = r && formalParameter(b, l + 1);
    r = r && consumeToken(b, ECL_WITH_KEY);
    r = r && formalParameter(b, l + 1);
    r = r && matchRule_6(b, l + 1);
    r = r && eolLeftBrace(b, l + 1);
    r = r && matchRule_8(b, l + 1);
    r = r && matchRule_9(b, l + 1);
    r = r && matchRule_10(b, l + 1);
    r = r && eolRightBrace(b, l + 1);
    exit_section_(b, m, ECL_MATCH_RULE, r);
    return r;
  }

  // extends?
  private static boolean matchRule_6(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "matchRule_6")) return false;
    extends_$(b, l + 1);
    return true;
  }

  // guard?
  private static boolean matchRule_8(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "matchRule_8")) return false;
    guard(b, l + 1);
    return true;
  }

  // compareBlock?
  private static boolean matchRule_9(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "matchRule_9")) return false;
    compareBlock(b, l + 1);
    return true;
  }

  // doBlock?
  private static boolean matchRule_10(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "matchRule_10")) return false;
    doBlock(b, l + 1);
    return true;
  }

  /* ********************************************************** */
  // importStatement* modelDeclaration* content
  public static boolean module(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "module")) return false;
    boolean r;
    Marker m = enter_section_(b, l, _NONE_, ECL_MODULE, "<module>");
    r = module_0(b, l + 1);
    r = r && module_1(b, l + 1);
    r = r && content(b, l + 1);
    exit_section_(b, l, m, r, false, null);
    return r;
  }

  // importStatement*
  private static boolean module_0(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "module_0")) return false;
    while (true) {
      int c = current_position_(b);
      if (!eolImportStatement(b, l + 1)) break;
      if (!empty_element_parsed_guard_(b, "module_0", c)) break;
    }
    return true;
  }

  // modelDeclaration*
  private static boolean module_1(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "module_1")) return false;
    while (true) {
      int c = current_position_(b);
      if (!eolModelDeclaration(b, l + 1)) break;
      if (!empty_element_parsed_guard_(b, "module_1", c)) break;
    }
    return true;
  }

  /* ********************************************************** */
  // ERL_POST ID? EOL_LB statement* EOL_RB
  public static boolean post(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "post")) return false;
    boolean r;
    Marker m = enter_section_(b, l, _NONE_, ECL_POST, "<post>");
    r = erlPost(b, l + 1);
    r = r && post_1(b, l + 1);
    r = r && eolLeftBrace(b, l + 1);
    r = r && post_3(b, l + 1);
    r = r && eolRightBrace(b, l + 1);
    exit_section_(b, l, m, r, false, null);
    return r;
  }

  // ID?
  private static boolean post_1(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "post_1")) return false;
    eolId(b, l + 1);
    return true;
  }

  // statement*
  private static boolean post_3(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "post_3")) return false;
    while (true) {
      int c = current_position_(b);
      if (!statement(b, l + 1)) break;
      if (!empty_element_parsed_guard_(b, "post_3", c)) break;
    }
    return true;
  }

  /* ********************************************************** */
  // ERL_PRE ID? EOL_LB statement* EOL_RB
  public static boolean pre(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "pre")) return false;
    boolean r;
    Marker m = enter_section_(b, l, _NONE_, ECL_PRE, "<pre>");
    r = erlPre(b, l + 1);
    r = r && pre_1(b, l + 1);
    r = r && eolLeftBrace(b, l + 1);
    r = r && pre_3(b, l + 1);
    r = r && eolRightBrace(b, l + 1);
    exit_section_(b, l, m, r, false, null);
    return r;
  }

  // ID?
  private static boolean pre_1(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "pre_1")) return false;
    eolId(b, l + 1);
    return true;
  }

  // statement*
  private static boolean pre_3(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "pre_3")) return false;
    while (true) {
      int c = current_position_(b);
      if (!statement(b, l + 1)) break;
      if (!empty_element_parsed_guard_(b, "pre_3", c)) break;
    }
    return true;
  }

  /* ********************************************************** */
  // module <<eof>>
  static boolean root(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "root")) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = module(b, l + 1);
    r = r && eof(b, l + 1);
    exit_section_(b, m, null, r);
    return r;
  }

  /* ********************************************************** */
  // eolStatement
  public static boolean statement(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "statement")) return false;
    boolean r;
    Marker m = enter_section_(b, l, _NONE_, ECL_STATEMENT, "<statement>");
    r = eolStatement(b, l + 1);
    exit_section_(b, l, m, r, false, null);
    return r;
  }

}

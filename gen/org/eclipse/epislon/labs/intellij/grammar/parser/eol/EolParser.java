/*******************************************************************************
 * Copyright (c) 2008 The University of York.
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *     Horacio Hoyos Rodriguez - initial API and implementation
 ******************************************************************************/
package org.eclipse.epislon.labs.intellij.grammar.parser.eol;

import com.intellij.lang.PsiBuilder;
import com.intellij.lang.PsiBuilder.Marker;
import static org.eclipse.epislon.labs.intellij.grammar.psi.eol.EolTypes.*;
import static org.eclipse.epislon.labs.intellij.grammar.parser.eol.EolParserUtil.*;
import com.intellij.psi.tree.IElementType;
import com.intellij.lang.ASTNode;
import com.intellij.psi.tree.TokenSet;
import com.intellij.lang.PsiParser;
import com.intellij.lang.LightPsiParser;

@SuppressWarnings({"SimplifiableIfStatement", "UnusedAssignment"})
public class EolParser implements PsiParser, LightPsiParser {

  public ASTNode parse(IElementType t, PsiBuilder b) {
    parseLight(t, b);
    return b.getTreeBuilt();
  }

  public void parseLight(IElementType t, PsiBuilder b) {
    boolean r;
    b = adapt_builder_(t, b, this, EXTENDS_SETS_);
    Marker m = enter_section_(b, 0, _COLLAPSE_, null);
    r = parse_root_(t, b);
    exit_section_(b, 0, m, t, r, true, TRUE_CONDITION);
  }

  protected boolean parse_root_(IElementType t, PsiBuilder b) {
    return parse_root_(t, b, 0);
  }

  static boolean parse_root_(IElementType t, PsiBuilder b, int l) {
    return root(b, l + 1);
  }

  public static final TokenSet[] EXTENDS_SETS_ = new TokenSet[] {
    create_token_set_(EOL_ADD_EXPR, EOL_BOOL_EXPR, EOL_EQ_EXPR, EOL_EXPR,
      EOL_ITEM_SELECTOR_EXPR, EOL_MULT_EXPR, EOL_NEW_EXPR, EOL_POSTFIX_EXPR,
      EOL_PRIMITIVE_EXPR, EOL_REL_EXPR, EOL_SHORTCUT_OPERATOR_EXPR, EOL_VARIABLE_DECLARATION_EXPR),
  };

  /* ********************************************************** */
  // 'abort' ';'
  public static boolean abortStatement(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "abortStatement")) return false;
    if (!nextTokenIs(b, EOL_ABORT_KEY)) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = consumeTokens(b, 0, EOL_ABORT_KEY, EOL_SEMICOLON);
    exit_section_(b, m, EOL_ABORT_STATEMENT, r);
    return r;
  }

  /* ********************************************************** */
  // mult_elmnt add_expr *
  static boolean add_elmnt(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "add_elmnt")) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = mult_elmnt(b, l + 1);
    r = r && add_elmnt_1(b, l + 1);
    exit_section_(b, m, null, r);
    return r;
  }

  // add_expr *
  private static boolean add_elmnt_1(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "add_elmnt_1")) return false;
    while (true) {
      int c = current_position_(b);
      if (!add_expr(b, l + 1)) break;
      if (!empty_element_parsed_guard_(b, "add_elmnt_1", c)) break;
    }
    return true;
  }

  /* ********************************************************** */
  // add_op mult_elmnt
  public static boolean add_expr(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "add_expr")) return false;
    if (!nextTokenIs(b, "<add expr>", EOL_NEG_OP, EOL_PLUS_OP)) return false;
    boolean r;
    Marker m = enter_section_(b, l, _LEFT_, EOL_ADD_EXPR, "<add expr>");
    r = add_op(b, l + 1);
    r = r && mult_elmnt(b, l + 1);
    exit_section_(b, l, m, r, false, null);
    return r;
  }

  /* ********************************************************** */
  // '+' | '-'
  static boolean add_op(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "add_op")) return false;
    if (!nextTokenIs(b, "", EOL_NEG_OP, EOL_PLUS_OP)) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = consumeToken(b, EOL_PLUS_OP);
    if (!r) r = consumeToken(b, EOL_NEG_OP);
    exit_section_(b, m, null, r);
    return r;
  }

  /* ********************************************************** */
  // (simpleAnnotation | executableAnnotation)+
  public static boolean annotationBlock(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "annotationBlock")) return false;
    boolean r;
    Marker m = enter_section_(b, l, _NONE_, EOL_ANNOTATION_BLOCK, "<annotation block>");
    r = annotationBlock_0(b, l + 1);
    while (r) {
      int c = current_position_(b);
      if (!annotationBlock_0(b, l + 1)) break;
      if (!empty_element_parsed_guard_(b, "annotationBlock", c)) break;
    }
    exit_section_(b, l, m, r, false, null);
    return r;
  }

  // simpleAnnotation | executableAnnotation
  private static boolean annotationBlock_0(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "annotationBlock_0")) return false;
    boolean r;
    r = simpleAnnotation(b, l + 1);
    if (!r) r = executableAnnotation(b, l + 1);
    return r;
  }

  /* ********************************************************** */
  // expr (':=' | '+=' | '-=' | '*=' | '/=' | '::=') expr ';'
  public static boolean assignmentStatement(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "assignmentStatement")) return false;
    boolean r;
    Marker m = enter_section_(b, l, _NONE_, EOL_ASSIGNMENT_STATEMENT, "<assignment statement>");
    r = expr(b, l + 1);
    r = r && assignmentStatement_1(b, l + 1);
    r = r && expr(b, l + 1);
    r = r && consumeToken(b, EOL_SEMICOLON);
    exit_section_(b, l, m, r, false, null);
    return r;
  }

  // ':=' | '+=' | '-=' | '*=' | '/=' | '::='
  private static boolean assignmentStatement_1(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "assignmentStatement_1")) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = consumeToken(b, EOL_SPECIAL_ASSIGN_OP);
    if (!r) r = consumeToken(b, EOL_PLUS_ASSIGN_OP);
    if (!r) r = consumeToken(b, EOL_MINUS_ASSIGN_OP);
    if (!r) r = consumeToken(b, EOL_TIMES_ASSIGN_OP);
    if (!r) r = consumeToken(b, EOL_DIV_ASSIG_OP);
    if (!r) r = consumeToken(b, EOL_EQUIV_ASSIGN_OP);
    exit_section_(b, m, null, r);
    return r;
  }

  /* ********************************************************** */
  // assignmentStatement | expressionStatement | forStatement | ifStatement | whileStatement
  //                      | switchStatement | transactionStatement
  static boolean blockStatement(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "blockStatement")) return false;
    boolean r;
    r = assignmentStatement(b, l + 1);
    if (!r) r = expressionStatement(b, l + 1);
    if (!r) r = forStatement(b, l + 1);
    if (!r) r = ifStatement(b, l + 1);
    if (!r) r = whileStatement(b, l + 1);
    if (!r) r = switchStatement(b, l + 1);
    if (!r) r = transactionStatement(b, l + 1);
    return r;
  }

  /* ********************************************************** */
  // add_elmnt (eq_expr | rel_expr)*
  static boolean bool_elmnt(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "bool_elmnt")) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = add_elmnt(b, l + 1);
    r = r && bool_elmnt_1(b, l + 1);
    exit_section_(b, m, null, r);
    return r;
  }

  // (eq_expr | rel_expr)*
  private static boolean bool_elmnt_1(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "bool_elmnt_1")) return false;
    while (true) {
      int c = current_position_(b);
      if (!bool_elmnt_1_0(b, l + 1)) break;
      if (!empty_element_parsed_guard_(b, "bool_elmnt_1", c)) break;
    }
    return true;
  }

  // eq_expr | rel_expr
  private static boolean bool_elmnt_1_0(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "bool_elmnt_1_0")) return false;
    boolean r;
    r = eq_expr(b, l + 1);
    if (!r) r = rel_expr(b, l + 1);
    return r;
  }

  /* ********************************************************** */
  // bool_op bool_elmnt
  public static boolean bool_expr(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "bool_expr")) return false;
    boolean r;
    Marker m = enter_section_(b, l, _LEFT_, EOL_BOOL_EXPR, "<bool expr>");
    r = bool_op(b, l + 1);
    r = r && bool_elmnt(b, l + 1);
    exit_section_(b, l, m, r, false, null);
    return r;
  }

  /* ********************************************************** */
  // 'or' | 'and' | 'xor' | 'implies'
  static boolean bool_op(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "bool_op")) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = consumeToken(b, EOL_OR_OP);
    if (!r) r = consumeToken(b, EOL_AND_OP);
    if (!r) r = consumeToken(b, EOL_XOR_OP);
    if (!r) r = consumeToken(b, EOL_IMPLIES_OP);
    exit_section_(b, m, null, r);
    return r;
  }

  /* ********************************************************** */
  // 'true' | 'false'
  public static boolean boolean_$(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "boolean_$")) return false;
    if (!nextTokenIs(b, "<boolean $>", EOL_FALSE_KEY, EOL_TRUE_KEY)) return false;
    boolean r;
    Marker m = enter_section_(b, l, _NONE_, EOL_BOOLEAN, "<boolean $>");
    r = consumeToken(b, EOL_TRUE_KEY);
    if (!r) r = consumeToken(b, EOL_FALSE_KEY);
    exit_section_(b, l, m, r, false, null);
    return r;
  }

  /* ********************************************************** */
  // 'breakAll' ';'
  public static boolean breakAllStatement(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "breakAllStatement")) return false;
    if (!nextTokenIs(b, EOL_BREAK_ALL_KEY)) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = consumeTokens(b, 0, EOL_BREAK_ALL_KEY, EOL_SEMICOLON);
    exit_section_(b, m, EOL_BREAK_ALL_STATEMENT, r);
    return r;
  }

  /* ********************************************************** */
  // 'break' ';'
  public static boolean breakStatement(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "breakStatement")) return false;
    if (!nextTokenIs(b, EOL_BREAK_KEY)) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = consumeTokens(b, 0, EOL_BREAK_KEY, EOL_SEMICOLON);
    exit_section_(b, m, EOL_BREAK_STATEMENT, r);
    return r;
  }

  /* ********************************************************** */
  // EQUIVALENT_BIN |
  //         EQUIVALENTS_BIN |
  //         SATISFIES_BIN |
  //         SATISFIES_ONE_BIN |
  //         SATISFIES_ALL_BIN |
  //         MATCHES_BIN
  public static boolean built_in(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "built_in")) return false;
    boolean r;
    Marker m = enter_section_(b, l, _NONE_, EOL_BUILT_IN, "<built in>");
    r = consumeToken(b, EOL_EQUIVALENT_BIN);
    if (!r) r = consumeToken(b, EOL_EQUIVALENTS_BIN);
    if (!r) r = consumeToken(b, EOL_SATISFIES_BIN);
    if (!r) r = consumeToken(b, EOL_SATISFIES_ONE_BIN);
    if (!r) r = consumeToken(b, EOL_SATISFIES_ALL_BIN);
    if (!r) r = consumeToken(b, EOL_MATCHES_BIN);
    exit_section_(b, l, m, r, false, null);
    return r;
  }

  /* ********************************************************** */
  // 'case' expr ':' (statementBlock | statement* )
  public static boolean caseStatement(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "caseStatement")) return false;
    if (!nextTokenIs(b, EOL_CASE_KEY)) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = consumeToken(b, EOL_CASE_KEY);
    r = r && expr(b, l + 1);
    r = r && consumeToken(b, EOL_COLON_OP);
    r = r && caseStatement_3(b, l + 1);
    exit_section_(b, m, EOL_CASE_STATEMENT, r);
    return r;
  }

  // statementBlock | statement*
  private static boolean caseStatement_3(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "caseStatement_3")) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = statementBlock(b, l + 1);
    if (!r) r = caseStatement_3_1(b, l + 1);
    exit_section_(b, m, null, r);
    return r;
  }

  // statement*
  private static boolean caseStatement_3_1(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "caseStatement_3_1")) return false;
    while (true) {
      int c = current_position_(b);
      if (!statement(b, l + 1)) break;
      if (!empty_element_parsed_guard_(b, "caseStatement_3_1", c)) break;
    }
    return true;
  }

  /* ********************************************************** */
  // ('Collection'|'Sequence'|'List'|'Bag'|'Set'|'OrderedSet'|'Map')
  //     [('(' typeNameList ')') | ('<' typeNameList '>')]
  public static boolean collectionType(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "collectionType")) return false;
    boolean r;
    Marker m = enter_section_(b, l, _NONE_, EOL_COLLECTION_TYPE, "<collection type>");
    r = collectionType_0(b, l + 1);
    r = r && collectionType_1(b, l + 1);
    exit_section_(b, l, m, r, false, null);
    return r;
  }

  // 'Collection'|'Sequence'|'List'|'Bag'|'Set'|'OrderedSet'|'Map'
  private static boolean collectionType_0(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "collectionType_0")) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = consumeToken(b, EOL_COL_KEY);
    if (!r) r = consumeToken(b, EOL_SEQ_KEY);
    if (!r) r = consumeToken(b, EOL_LIST_KEY);
    if (!r) r = consumeToken(b, EOL_BAG_KEY);
    if (!r) r = consumeToken(b, EOL_SET_KEY);
    if (!r) r = consumeToken(b, EOL_ORDSET_KEY);
    if (!r) r = consumeToken(b, EOL_MAP_KEY);
    exit_section_(b, m, null, r);
    return r;
  }

  // [('(' typeNameList ')') | ('<' typeNameList '>')]
  private static boolean collectionType_1(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "collectionType_1")) return false;
    collectionType_1_0(b, l + 1);
    return true;
  }

  // ('(' typeNameList ')') | ('<' typeNameList '>')
  private static boolean collectionType_1_0(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "collectionType_1_0")) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = collectionType_1_0_0(b, l + 1);
    if (!r) r = collectionType_1_0_1(b, l + 1);
    exit_section_(b, m, null, r);
    return r;
  }

  // '(' typeNameList ')'
  private static boolean collectionType_1_0_0(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "collectionType_1_0_0")) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = consumeToken(b, EOL_LEFT_PAREN);
    r = r && typeNameList(b, l + 1);
    r = r && consumeToken(b, EOL_RIGHT_PAREN);
    exit_section_(b, m, null, r);
    return r;
  }

  // '<' typeNameList '>'
  private static boolean collectionType_1_0_1(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "collectionType_1_0_1")) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = consumeToken(b, EOL_LT_OP);
    r = r && typeNameList(b, l + 1);
    r = r && consumeToken(b, EOL_GT_OP);
    exit_section_(b, m, null, r);
    return r;
  }

  /* ********************************************************** */
  // ID '(' (lambdaExpression | lambdaExpressionInBrackets) (',' (expr | lambdaExpressionInBrackets))* ')'
  public static boolean complexFeatureCall(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "complexFeatureCall")) return false;
    if (!nextTokenIs(b, EOL_ID)) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = consumeTokens(b, 0, EOL_ID, EOL_LEFT_PAREN);
    r = r && complexFeatureCall_2(b, l + 1);
    r = r && complexFeatureCall_3(b, l + 1);
    r = r && consumeToken(b, EOL_RIGHT_PAREN);
    exit_section_(b, m, EOL_COMPLEX_FEATURE_CALL, r);
    return r;
  }

  // lambdaExpression | lambdaExpressionInBrackets
  private static boolean complexFeatureCall_2(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "complexFeatureCall_2")) return false;
    boolean r;
    r = lambdaExpression(b, l + 1);
    if (!r) r = lambdaExpressionInBrackets(b, l + 1);
    return r;
  }

  // (',' (expr | lambdaExpressionInBrackets))*
  private static boolean complexFeatureCall_3(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "complexFeatureCall_3")) return false;
    while (true) {
      int c = current_position_(b);
      if (!complexFeatureCall_3_0(b, l + 1)) break;
      if (!empty_element_parsed_guard_(b, "complexFeatureCall_3", c)) break;
    }
    return true;
  }

  // ',' (expr | lambdaExpressionInBrackets)
  private static boolean complexFeatureCall_3_0(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "complexFeatureCall_3_0")) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = consumeToken(b, EOL_COMMA);
    r = r && complexFeatureCall_3_0_1(b, l + 1);
    exit_section_(b, m, null, r);
    return r;
  }

  // expr | lambdaExpressionInBrackets
  private static boolean complexFeatureCall_3_0_1(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "complexFeatureCall_3_0_1")) return false;
    boolean r;
    r = expr(b, l + 1);
    if (!r) r = lambdaExpressionInBrackets(b, l + 1);
    return r;
  }

  /* ********************************************************** */
  // (statement | operationDeclarationOrAnnotationBlock)*
  public static boolean content(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "content")) return false;
    Marker m = enter_section_(b, l, _NONE_, EOL_CONTENT, "<content>");
    while (true) {
      int c = current_position_(b);
      if (!content_0(b, l + 1)) break;
      if (!empty_element_parsed_guard_(b, "content", c)) break;
    }
    exit_section_(b, l, m, true, false, null);
    return true;
  }

  // statement | operationDeclarationOrAnnotationBlock
  private static boolean content_0(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "content_0")) return false;
    boolean r;
    r = statement(b, l + 1);
    if (!r) r = operationDeclarationOrAnnotationBlock(b, l + 1);
    return r;
  }

  /* ********************************************************** */
  // 'continue' ';'
  public static boolean continueStatement(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "continueStatement")) return false;
    if (!nextTokenIs(b, EOL_CONTINUE_KEY)) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = consumeTokens(b, 0, EOL_CONTINUE_KEY, EOL_SEMICOLON);
    exit_section_(b, m, EOL_CONTINUE_STATEMENT, r);
    return r;
  }

  /* ********************************************************** */
  // 'default' ':' (statementBlock | statement* )
  public static boolean defaultStatement(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "defaultStatement")) return false;
    if (!nextTokenIs(b, EOL_DEFAULT_KEY)) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = consumeTokens(b, 0, EOL_DEFAULT_KEY, EOL_COLON_OP);
    r = r && defaultStatement_2(b, l + 1);
    exit_section_(b, m, EOL_DEFAULT_STATEMENT, r);
    return r;
  }

  // statementBlock | statement*
  private static boolean defaultStatement_2(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "defaultStatement_2")) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = statementBlock(b, l + 1);
    if (!r) r = defaultStatement_2_1(b, l + 1);
    exit_section_(b, m, null, r);
    return r;
  }

  // statement*
  private static boolean defaultStatement_2_1(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "defaultStatement_2_1")) return false;
    while (true) {
      int c = current_position_(b);
      if (!statement(b, l + 1)) break;
      if (!empty_element_parsed_guard_(b, "defaultStatement_2_1", c)) break;
    }
    return true;
  }

  /* ********************************************************** */
  // 'delete' expr? ';'
  public static boolean deleteStatement(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "deleteStatement")) return false;
    if (!nextTokenIs(b, EOL_DELETE_KEY)) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = consumeToken(b, EOL_DELETE_KEY);
    r = r && deleteStatement_1(b, l + 1);
    r = r && consumeToken(b, EOL_SEMICOLON);
    exit_section_(b, m, EOL_DELETE_STATEMENT, r);
    return r;
  }

  // expr?
  private static boolean deleteStatement_1(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "deleteStatement_1")) return false;
    expr(b, l + 1);
    return true;
  }

  /* ********************************************************** */
  // 'else' statementOrStatementBlock
  public static boolean elseStatement(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "elseStatement")) return false;
    if (!nextTokenIs(b, EOL_ELSE_KEY)) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = consumeToken(b, EOL_ELSE_KEY);
    r = r && statementOrStatementBlock(b, l + 1);
    exit_section_(b, m, EOL_ELSE_STATEMENT, r);
    return r;
  }

  /* ********************************************************** */
  // eq_op bool_elmnt
  public static boolean eq_expr(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "eq_expr")) return false;
    if (!nextTokenIs(b, "<eq expr>", EOL_ASSIGN_OP, EOL_EQ_OP)) return false;
    boolean r;
    Marker m = enter_section_(b, l, _LEFT_, EOL_EQ_EXPR, "<eq expr>");
    r = eq_op(b, l + 1);
    r = r && bool_elmnt(b, l + 1);
    exit_section_(b, l, m, r, false, null);
    return r;
  }

  /* ********************************************************** */
  // '==' | '='
  static boolean eq_op(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "eq_op")) return false;
    if (!nextTokenIs(b, "", EOL_ASSIGN_OP, EOL_EQ_OP)) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = consumeToken(b, EOL_EQ_OP);
    if (!r) r = consumeToken(b, EOL_ASSIGN_OP);
    exit_section_(b, m, null, r);
    return r;
  }

  /* ********************************************************** */
  // ('$' | '$pre' | '$post') expr
  public static boolean executableAnnotation(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "executableAnnotation")) return false;
    boolean r;
    Marker m = enter_section_(b, l, _NONE_, EOL_EXECUTABLE_ANNOTATION, "<executable annotation>");
    r = executableAnnotation_0(b, l + 1);
    r = r && expr(b, l + 1);
    exit_section_(b, l, m, r, false, null);
    return r;
  }

  // '$' | '$pre' | '$post'
  private static boolean executableAnnotation_0(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "executableAnnotation_0")) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = consumeToken(b, EOL_EXEC_ANNOT);
    if (!r) r = consumeToken(b, EOL_PRE_COND);
    if (!r) r = consumeToken(b, EOL_POST_COND);
    exit_section_(b, m, null, r);
    return r;
  }

  /* ********************************************************** */
  // bool_elmnt bool_expr *
  public static boolean expr(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "expr")) return false;
    boolean r;
    Marker m = enter_section_(b, l, _COLLAPSE_, EOL_EXPR, "<expr>");
    r = bool_elmnt(b, l + 1);
    r = r && expr_1(b, l + 1);
    exit_section_(b, l, m, r, false, null);
    return r;
  }

  // bool_expr *
  private static boolean expr_1(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "expr_1")) return false;
    while (true) {
      int c = current_position_(b);
      if (!bool_expr(b, l + 1)) break;
      if (!empty_element_parsed_guard_(b, "expr_1", c)) break;
    }
    return true;
  }

  /* ********************************************************** */
  // (':' expr) | statementBlock
  public static boolean exprOrStatementBlock(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "exprOrStatementBlock")) return false;
    if (!nextTokenIs(b, "<expr or statement block>", EOL_COLON_OP, EOL_LEFT_BRACE)) return false;
    boolean r;
    Marker m = enter_section_(b, l, _NONE_, EOL_EXPR_OR_STATEMENT_BLOCK, "<expr or statement block>");
    r = exprOrStatementBlock_0(b, l + 1);
    if (!r) r = statementBlock(b, l + 1);
    exit_section_(b, l, m, r, false, null);
    return r;
  }

  // ':' expr
  private static boolean exprOrStatementBlock_0(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "exprOrStatementBlock_0")) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = consumeToken(b, EOL_COLON_OP);
    r = r && expr(b, l + 1);
    exit_section_(b, m, null, r);
    return r;
  }

  /* ********************************************************** */
  // '(' expr ')'
  public static boolean expressionInBrackets(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "expressionInBrackets")) return false;
    if (!nextTokenIs(b, EOL_LEFT_PAREN)) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = consumeToken(b, EOL_LEFT_PAREN);
    r = r && expr(b, l + 1);
    r = r && consumeToken(b, EOL_RIGHT_PAREN);
    exit_section_(b, m, EOL_EXPRESSION_IN_BRACKETS, r);
    return r;
  }

  /* ********************************************************** */
  // <<list expr>>
  static boolean expressionList(PsiBuilder b, int l) {
    return list(b, l + 1, expr_parser_);
  }

  /* ********************************************************** */
  // expressionRange | expressionList
  public static boolean expressionListOrRange(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "expressionListOrRange")) return false;
    boolean r;
    Marker m = enter_section_(b, l, _NONE_, EOL_EXPRESSION_LIST_OR_RANGE, "<expression list or range>");
    r = expressionRange(b, l + 1);
    if (!r) r = expressionList(b, l + 1);
    exit_section_(b, l, m, r, false, null);
    return r;
  }

  /* ********************************************************** */
  // expr '..' expr
  public static boolean expressionRange(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "expressionRange")) return false;
    boolean r;
    Marker m = enter_section_(b, l, _NONE_, EOL_EXPRESSION_RANGE, "<expression range>");
    r = expr(b, l + 1);
    r = r && consumeToken(b, EOL_POINT_POINT_OP);
    r = r && expr(b, l + 1);
    exit_section_(b, l, m, r, false, null);
    return r;
  }

  /* ********************************************************** */
  // ((postfix_expr '=' expr) | expr) ';'
  public static boolean expressionStatement(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "expressionStatement")) return false;
    boolean r;
    Marker m = enter_section_(b, l, _NONE_, EOL_EXPRESSION_STATEMENT, "<expression statement>");
    r = expressionStatement_0(b, l + 1);
    r = r && consumeToken(b, EOL_SEMICOLON);
    exit_section_(b, l, m, r, false, null);
    return r;
  }

  // (postfix_expr '=' expr) | expr
  private static boolean expressionStatement_0(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "expressionStatement_0")) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = expressionStatement_0_0(b, l + 1);
    if (!r) r = expr(b, l + 1);
    exit_section_(b, m, null, r);
    return r;
  }

  // postfix_expr '=' expr
  private static boolean expressionStatement_0_0(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "expressionStatement_0_0")) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = postfix_expr(b, l + 1);
    r = r && consumeToken(b, EOL_ASSIGN_OP);
    r = r && expr(b, l + 1);
    exit_section_(b, m, null, r);
    return r;
  }

  /* ********************************************************** */
  // complexFeatureCall | simpleFeatureCall
  public static boolean featureCall(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "featureCall")) return false;
    boolean r;
    Marker m = enter_section_(b, l, _NONE_, EOL_FEATURE_CALL, "<feature call>");
    r = complexFeatureCall(b, l + 1);
    if (!r) r = simpleFeatureCall(b, l + 1);
    exit_section_(b, l, m, r, false, null);
    return r;
  }

  /* ********************************************************** */
  // 'for' '(' formalParameter 'in' expr ')' statementOrStatementBlock
  public static boolean forStatement(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "forStatement")) return false;
    if (!nextTokenIs(b, EOL_FOR_KEY)) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = consumeTokens(b, 0, EOL_FOR_KEY, EOL_LEFT_PAREN);
    r = r && formalParameter(b, l + 1);
    r = r && consumeToken(b, EOL_IN_KEY);
    r = r && expr(b, l + 1);
    r = r && consumeToken(b, EOL_RIGHT_PAREN);
    r = r && statementOrStatementBlock(b, l + 1);
    exit_section_(b, m, EOL_FOR_STATEMENT, r);
    return r;
  }

  /* ********************************************************** */
  // ID [':' typeName]
  public static boolean formalParameter(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "formalParameter")) return false;
    if (!nextTokenIs(b, EOL_ID)) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = consumeToken(b, EOL_ID);
    r = r && formalParameter_1(b, l + 1);
    exit_section_(b, m, EOL_FORMAL_PARAMETER, r);
    return r;
  }

  // [':' typeName]
  private static boolean formalParameter_1(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "formalParameter_1")) return false;
    formalParameter_1_0(b, l + 1);
    return true;
  }

  // ':' typeName
  private static boolean formalParameter_1_0(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "formalParameter_1_0")) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = consumeToken(b, EOL_COLON_OP);
    r = r && typeName(b, l + 1);
    exit_section_(b, m, null, r);
    return r;
  }

  /* ********************************************************** */
  // <<list formalParameter>>
  public static boolean formalParameterList(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "formalParameterList")) return false;
    if (!nextTokenIs(b, EOL_ID)) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = list(b, l + 1, formalParameter_parser_);
    exit_section_(b, m, EOL_FORMAL_PARAMETER_LIST, r);
    return r;
  }

  /* ********************************************************** */
  // <<list ID>>
  public static boolean idList(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "idList")) return false;
    if (!nextTokenIs(b, EOL_ID)) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = list(b, l + 1, EOL_ID_parser_);
    exit_section_(b, m, EOL_ID_LIST, r);
    return r;
  }

  /* ********************************************************** */
  // 'if' '(' expr ')' statementOrStatementBlock elseStatement?
  public static boolean ifStatement(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "ifStatement")) return false;
    if (!nextTokenIs(b, EOL_IF_KEY)) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = consumeTokens(b, 0, EOL_IF_KEY, EOL_LEFT_PAREN);
    r = r && expr(b, l + 1);
    r = r && consumeToken(b, EOL_RIGHT_PAREN);
    r = r && statementOrStatementBlock(b, l + 1);
    r = r && ifStatement_5(b, l + 1);
    exit_section_(b, m, EOL_IF_STATEMENT, r);
    return r;
  }

  // elseStatement?
  private static boolean ifStatement_5(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "ifStatement_5")) return false;
    elseStatement(b, l + 1);
    return true;
  }

  /* ********************************************************** */
  // 'import' STRING ';'
  public static boolean importStatement(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "importStatement")) return false;
    if (!nextTokenIs(b, EOL_IMPORT_KEY)) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = consumeTokens(b, 0, EOL_IMPORT_KEY, EOL_STRING, EOL_SEMICOLON);
    exit_section_(b, m, EOL_IMPORT_STATEMENT, r);
    return r;
  }

  /* ********************************************************** */
  // primitive_expr ('[' primitive_expr ']')*
  public static boolean itemSelector_expr(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "itemSelector_expr")) return false;
    boolean r;
    Marker m = enter_section_(b, l, _COLLAPSE_, EOL_ITEM_SELECTOR_EXPR, "<item selector expr>");
    r = primitive_expr(b, l + 1);
    r = r && itemSelector_expr_1(b, l + 1);
    exit_section_(b, l, m, r, false, null);
    return r;
  }

  // ('[' primitive_expr ']')*
  private static boolean itemSelector_expr_1(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "itemSelector_expr_1")) return false;
    while (true) {
      int c = current_position_(b);
      if (!itemSelector_expr_1_0(b, l + 1)) break;
      if (!empty_element_parsed_guard_(b, "itemSelector_expr_1", c)) break;
    }
    return true;
  }

  // '[' primitive_expr ']'
  private static boolean itemSelector_expr_1_0(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "itemSelector_expr_1_0")) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = consumeToken(b, EOL_LEFT_SQ);
    r = r && primitive_expr(b, l + 1);
    r = r && consumeToken(b, EOL_RIGHT_SQ);
    exit_section_(b, m, null, r);
    return r;
  }

  /* ********************************************************** */
  // expr '=' expr
  public static boolean keyvalExpression(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "keyvalExpression")) return false;
    boolean r;
    Marker m = enter_section_(b, l, _NONE_, EOL_KEYVAL_EXPRESSION, "<keyval expression>");
    r = expr(b, l + 1);
    r = r && consumeToken(b, EOL_ASSIGN_OP);
    r = r && expr(b, l + 1);
    exit_section_(b, l, m, r, false, null);
    return r;
  }

  /* ********************************************************** */
  // <<list keyvalExpression>>
  static boolean keyvalExpressionList(PsiBuilder b, int l) {
    return list(b, l + 1, keyvalExpression_parser_);
  }

  /* ********************************************************** */
  // formalParameterList? ('|' | '=>') expr
  public static boolean lambdaExpression(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "lambdaExpression")) return false;
    boolean r;
    Marker m = enter_section_(b, l, _NONE_, EOL_LAMBDA_EXPRESSION, "<lambda expression>");
    r = lambdaExpression_0(b, l + 1);
    r = r && lambdaExpression_1(b, l + 1);
    r = r && expr(b, l + 1);
    exit_section_(b, l, m, r, false, null);
    return r;
  }

  // formalParameterList?
  private static boolean lambdaExpression_0(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "lambdaExpression_0")) return false;
    formalParameterList(b, l + 1);
    return true;
  }

  // '|' | '=>'
  private static boolean lambdaExpression_1(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "lambdaExpression_1")) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = consumeToken(b, EOL_IN_OP);
    if (!r) r = consumeToken(b, EOL_THEN_OP);
    exit_section_(b, m, null, r);
    return r;
  }

  /* ********************************************************** */
  // ('(' lambdaExpression ')') | ('[' lambdaExpression ']')
  public static boolean lambdaExpressionInBrackets(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "lambdaExpressionInBrackets")) return false;
    if (!nextTokenIs(b, "<lambda expression in brackets>", EOL_LEFT_PAREN, EOL_LEFT_SQ)) return false;
    boolean r;
    Marker m = enter_section_(b, l, _NONE_, EOL_LAMBDA_EXPRESSION_IN_BRACKETS, "<lambda expression in brackets>");
    r = lambdaExpressionInBrackets_0(b, l + 1);
    if (!r) r = lambdaExpressionInBrackets_1(b, l + 1);
    exit_section_(b, l, m, r, false, null);
    return r;
  }

  // '(' lambdaExpression ')'
  private static boolean lambdaExpressionInBrackets_0(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "lambdaExpressionInBrackets_0")) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = consumeToken(b, EOL_LEFT_PAREN);
    r = r && lambdaExpression(b, l + 1);
    r = r && consumeToken(b, EOL_RIGHT_PAREN);
    exit_section_(b, m, null, r);
    return r;
  }

  // '[' lambdaExpression ']'
  private static boolean lambdaExpressionInBrackets_1(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "lambdaExpressionInBrackets_1")) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = consumeToken(b, EOL_LEFT_SQ);
    r = r && lambdaExpression(b, l + 1);
    r = r && consumeToken(b, EOL_RIGHT_SQ);
    exit_section_(b, m, null, r);
    return r;
  }

  /* ********************************************************** */
  // <<p>> (',' <<p>>) *
  static boolean list(PsiBuilder b, int l, Parser _p) {
    if (!recursion_guard_(b, l, "list")) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = _p.parse(b, l);
    r = r && list_1(b, l + 1, _p);
    exit_section_(b, m, null, r);
    return r;
  }

  // (',' <<p>>) *
  private static boolean list_1(PsiBuilder b, int l, Parser _p) {
    if (!recursion_guard_(b, l, "list_1")) return false;
    while (true) {
      int c = current_position_(b);
      if (!list_1_0(b, l + 1, _p)) break;
      if (!empty_element_parsed_guard_(b, "list_1", c)) break;
    }
    return true;
  }

  // ',' <<p>>
  private static boolean list_1_0(PsiBuilder b, int l, Parser _p) {
    if (!recursion_guard_(b, l, "list_1_0")) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = consumeToken(b, EOL_COMMA);
    r = r && _p.parse(b, l);
    exit_section_(b, m, null, r);
    return r;
  }

  /* ********************************************************** */
  // STRING | boolean | 'self' | 'loopCount' | 'hasMore' | FLOAT
  public static boolean literal(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "literal")) return false;
    boolean r;
    Marker m = enter_section_(b, l, _NONE_, EOL_LITERAL, "<literal>");
    r = consumeToken(b, EOL_STRING);
    if (!r) r = boolean_$(b, l + 1);
    if (!r) r = consumeToken(b, EOL_SELF_BIN);
    if (!r) r = consumeToken(b, EOL_LOOP_CNT_BIN);
    if (!r) r = consumeToken(b, EOL_HAS_MORE_BIN);
    if (!r) r = consumeToken(b, EOL_FLOAT);
    exit_section_(b, l, m, r, false, null);
    return r;
  }

  /* ********************************************************** */
  // 'Map' '{' keyvalExpressionList? '}'
  public static boolean literalMapCollection(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "literalMapCollection")) return false;
    if (!nextTokenIs(b, EOL_MAP_KEY)) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = consumeTokens(b, 0, EOL_MAP_KEY, EOL_LEFT_BRACE);
    r = r && literalMapCollection_2(b, l + 1);
    r = r && consumeToken(b, EOL_RIGHT_BRACE);
    exit_section_(b, m, EOL_LITERAL_MAP_COLLECTION, r);
    return r;
  }

  // keyvalExpressionList?
  private static boolean literalMapCollection_2(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "literalMapCollection_2")) return false;
    keyvalExpressionList(b, l + 1);
    return true;
  }

  /* ********************************************************** */
  // ('Collection' | 'Sequence' | 'List' | 'Bag' | 'Set' | 'OrderedSet') '{'  expressionListOrRange? '}'
  public static boolean literalSequentialCollection(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "literalSequentialCollection")) return false;
    boolean r;
    Marker m = enter_section_(b, l, _NONE_, EOL_LITERAL_SEQUENTIAL_COLLECTION, "<literal sequential collection>");
    r = literalSequentialCollection_0(b, l + 1);
    r = r && consumeToken(b, EOL_LEFT_BRACE);
    r = r && literalSequentialCollection_2(b, l + 1);
    r = r && consumeToken(b, EOL_RIGHT_BRACE);
    exit_section_(b, l, m, r, false, null);
    return r;
  }

  // 'Collection' | 'Sequence' | 'List' | 'Bag' | 'Set' | 'OrderedSet'
  private static boolean literalSequentialCollection_0(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "literalSequentialCollection_0")) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = consumeToken(b, EOL_COL_KEY);
    if (!r) r = consumeToken(b, EOL_SEQ_KEY);
    if (!r) r = consumeToken(b, EOL_LIST_KEY);
    if (!r) r = consumeToken(b, EOL_BAG_KEY);
    if (!r) r = consumeToken(b, EOL_SET_KEY);
    if (!r) r = consumeToken(b, EOL_ORDSET_KEY);
    exit_section_(b, m, null, r);
    return r;
  }

  // expressionListOrRange?
  private static boolean literalSequentialCollection_2(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "literalSequentialCollection_2")) return false;
    expressionListOrRange(b, l + 1);
    return true;
  }

  /* ********************************************************** */
  // 'alias' idList
  public static boolean modelAlias(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "modelAlias")) return false;
    boolean r;
    Marker m = enter_section_(b, l, _NONE_, EOL_MODEL_ALIAS, "<model alias>");
    r = consumeToken(b, "alias");
    r = r && idList(b, l + 1);
    exit_section_(b, l, m, r, false, null);
    return r;
  }

  /* ********************************************************** */
  // 'model' ID modelAlias? modelDriver? modelDeclarationParameters? ';'
  public static boolean modelDeclaration(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "modelDeclaration")) return false;
    boolean r;
    Marker m = enter_section_(b, l, _NONE_, EOL_MODEL_DECLARATION, "<model declaration>");
    r = consumeToken(b, "model");
    r = r && consumeToken(b, EOL_ID);
    r = r && modelDeclaration_2(b, l + 1);
    r = r && modelDeclaration_3(b, l + 1);
    r = r && modelDeclaration_4(b, l + 1);
    r = r && consumeToken(b, EOL_SEMICOLON);
    exit_section_(b, l, m, r, false, null);
    return r;
  }

  // modelAlias?
  private static boolean modelDeclaration_2(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "modelDeclaration_2")) return false;
    modelAlias(b, l + 1);
    return true;
  }

  // modelDriver?
  private static boolean modelDeclaration_3(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "modelDeclaration_3")) return false;
    modelDriver(b, l + 1);
    return true;
  }

  // modelDeclarationParameters?
  private static boolean modelDeclaration_4(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "modelDeclaration_4")) return false;
    modelDeclarationParameters(b, l + 1);
    return true;
  }

  /* ********************************************************** */
  // ID '=' STRING
  public static boolean modelDeclarationParameter(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "modelDeclarationParameter")) return false;
    if (!nextTokenIs(b, EOL_ID)) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = consumeTokens(b, 0, EOL_ID, EOL_ASSIGN_OP, EOL_STRING);
    exit_section_(b, m, EOL_MODEL_DECLARATION_PARAMETER, r);
    return r;
  }

  /* ********************************************************** */
  // '{' modelDeclarationParameter? (',' modelDeclarationParameter)* '}'
  public static boolean modelDeclarationParameters(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "modelDeclarationParameters")) return false;
    if (!nextTokenIs(b, EOL_LEFT_BRACE)) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = consumeToken(b, EOL_LEFT_BRACE);
    r = r && modelDeclarationParameters_1(b, l + 1);
    r = r && modelDeclarationParameters_2(b, l + 1);
    r = r && consumeToken(b, EOL_RIGHT_BRACE);
    exit_section_(b, m, EOL_MODEL_DECLARATION_PARAMETERS, r);
    return r;
  }

  // modelDeclarationParameter?
  private static boolean modelDeclarationParameters_1(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "modelDeclarationParameters_1")) return false;
    modelDeclarationParameter(b, l + 1);
    return true;
  }

  // (',' modelDeclarationParameter)*
  private static boolean modelDeclarationParameters_2(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "modelDeclarationParameters_2")) return false;
    while (true) {
      int c = current_position_(b);
      if (!modelDeclarationParameters_2_0(b, l + 1)) break;
      if (!empty_element_parsed_guard_(b, "modelDeclarationParameters_2", c)) break;
    }
    return true;
  }

  // ',' modelDeclarationParameter
  private static boolean modelDeclarationParameters_2_0(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "modelDeclarationParameters_2_0")) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = consumeToken(b, EOL_COMMA);
    r = r && modelDeclarationParameter(b, l + 1);
    exit_section_(b, m, null, r);
    return r;
  }

  /* ********************************************************** */
  // 'driver' ID
  public static boolean modelDriver(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "modelDriver")) return false;
    boolean r;
    Marker m = enter_section_(b, l, _NONE_, EOL_MODEL_DRIVER, "<model driver>");
    r = consumeToken(b, "driver");
    r = r && consumeToken(b, EOL_ID);
    exit_section_(b, l, m, r, false, null);
    return r;
  }

  /* ********************************************************** */
  // importStatement* modelDeclaration* content
  public static boolean module(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "module")) return false;
    boolean r;
    Marker m = enter_section_(b, l, _NONE_, EOL_MODULE, "<module>");
    r = module_0(b, l + 1);
    r = r && module_1(b, l + 1);
    r = r && content(b, l + 1);
    exit_section_(b, l, m, r, false, null);
    return r;
  }

  // importStatement*
  private static boolean module_0(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "module_0")) return false;
    while (true) {
      int c = current_position_(b);
      if (!importStatement(b, l + 1)) break;
      if (!empty_element_parsed_guard_(b, "module_0", c)) break;
    }
    return true;
  }

  // modelDeclaration*
  private static boolean module_1(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "module_1")) return false;
    while (true) {
      int c = current_position_(b);
      if (!modelDeclaration(b, l + 1)) break;
      if (!empty_element_parsed_guard_(b, "module_1", c)) break;
    }
    return true;
  }

  /* ********************************************************** */
  // uny_elmnt mult_expr *
  static boolean mult_elmnt(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "mult_elmnt")) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = uny_elmnt(b, l + 1);
    r = r && mult_elmnt_1(b, l + 1);
    exit_section_(b, m, null, r);
    return r;
  }

  // mult_expr *
  private static boolean mult_elmnt_1(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "mult_elmnt_1")) return false;
    while (true) {
      int c = current_position_(b);
      if (!mult_expr(b, l + 1)) break;
      if (!empty_element_parsed_guard_(b, "mult_elmnt_1", c)) break;
    }
    return true;
  }

  /* ********************************************************** */
  // mult_op uny_elmnt
  public static boolean mult_expr(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "mult_expr")) return false;
    if (!nextTokenIs(b, "<mult expr>", EOL_DIV_OP, EOL_TIMES_OP)) return false;
    boolean r;
    Marker m = enter_section_(b, l, _LEFT_, EOL_MULT_EXPR, "<mult expr>");
    r = mult_op(b, l + 1);
    r = r && uny_elmnt(b, l + 1);
    exit_section_(b, l, m, r, false, null);
    return r;
  }

  /* ********************************************************** */
  // '*' | '/'
  static boolean mult_op(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "mult_op")) return false;
    if (!nextTokenIs(b, "", EOL_DIV_OP, EOL_TIMES_OP)) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = consumeToken(b, EOL_TIMES_OP);
    if (!r) r = consumeToken(b, EOL_DIV_OP);
    exit_section_(b, m, null, r);
    return r;
  }

  /* ********************************************************** */
  // 'Native' '(' STRING ')'
  public static boolean nativeType(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "nativeType")) return false;
    if (!nextTokenIs(b, EOL_NATIVE_KEY)) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = consumeTokens(b, 0, EOL_NATIVE_KEY, EOL_LEFT_PAREN, EOL_STRING, EOL_RIGHT_PAREN);
    exit_section_(b, m, EOL_NATIVE_TYPE, r);
    return r;
  }

  /* ********************************************************** */
  // '.' | '->'
  static boolean navigation_op(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "navigation_op")) return false;
    if (!nextTokenIs(b, "", EOL_ARROW_OP, EOL_POINT_OP)) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = consumeToken(b, EOL_POINT_OP);
    if (!r) r = consumeToken(b, EOL_ARROW_OP);
    exit_section_(b, m, null, r);
    return r;
  }

  /* ********************************************************** */
  // 'new' typeName parameterList?
  public static boolean new_expr(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "new_expr")) return false;
    if (!nextTokenIs(b, EOL_NEW_KEY)) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = consumeToken(b, EOL_NEW_KEY);
    r = r && typeName(b, l + 1);
    r = r && new_expr_2(b, l + 1);
    exit_section_(b, m, EOL_NEW_EXPR, r);
    return r;
  }

  // parameterList?
  private static boolean new_expr_2(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "new_expr_2")) return false;
    parameterList(b, l + 1);
    return true;
  }

  /* ********************************************************** */
  // operation_start typeName ID? '(' formalParameterList? ')' [':' typeName] statementBlock
  public static boolean operationDeclaration(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "operationDeclaration")) return false;
    if (!nextTokenIs(b, "<operation declaration>", EOL_FUNCTION_KEY, EOL_OPERATION_KEY)) return false;
    boolean r;
    Marker m = enter_section_(b, l, _NONE_, EOL_OPERATION_DECLARATION, "<operation declaration>");
    r = operation_start(b, l + 1);
    r = r && typeName(b, l + 1);
    r = r && operationDeclaration_2(b, l + 1);
    r = r && consumeToken(b, EOL_LEFT_PAREN);
    r = r && operationDeclaration_4(b, l + 1);
    r = r && consumeToken(b, EOL_RIGHT_PAREN);
    r = r && operationDeclaration_6(b, l + 1);
    r = r && statementBlock(b, l + 1);
    exit_section_(b, l, m, r, false, null);
    return r;
  }

  // ID?
  private static boolean operationDeclaration_2(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "operationDeclaration_2")) return false;
    consumeToken(b, EOL_ID);
    return true;
  }

  // formalParameterList?
  private static boolean operationDeclaration_4(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "operationDeclaration_4")) return false;
    formalParameterList(b, l + 1);
    return true;
  }

  // [':' typeName]
  private static boolean operationDeclaration_6(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "operationDeclaration_6")) return false;
    operationDeclaration_6_0(b, l + 1);
    return true;
  }

  // ':' typeName
  private static boolean operationDeclaration_6_0(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "operationDeclaration_6_0")) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = consumeToken(b, EOL_COLON_OP);
    r = r && typeName(b, l + 1);
    exit_section_(b, m, null, r);
    return r;
  }

  /* ********************************************************** */
  // operationDeclaration | annotationBlock
  public static boolean operationDeclarationOrAnnotationBlock(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "operationDeclarationOrAnnotationBlock")) return false;
    boolean r;
    Marker m = enter_section_(b, l, _NONE_, EOL_OPERATION_DECLARATION_OR_ANNOTATION_BLOCK, "<operation declaration or annotation block>");
    r = operationDeclaration(b, l + 1);
    if (!r) r = annotationBlock(b, l + 1);
    exit_section_(b, l, m, r, false, null);
    return r;
  }

  /* ********************************************************** */
  // 'operation' | 'function'
  public static boolean operation_start(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "operation_start")) return false;
    if (!nextTokenIs(b, "<operation start>", EOL_FUNCTION_KEY, EOL_OPERATION_KEY)) return false;
    boolean r;
    Marker m = enter_section_(b, l, _NONE_, EOL_OPERATION_START, "<operation start>");
    r = consumeToken(b, EOL_OPERATION_KEY);
    if (!r) r = consumeToken(b, EOL_FUNCTION_KEY);
    exit_section_(b, l, m, r, false, null);
    return r;
  }

  /* ********************************************************** */
  // ID ('::' ID)*
  public static boolean packagedType(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "packagedType")) return false;
    if (!nextTokenIs(b, EOL_ID)) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = consumeToken(b, EOL_ID);
    r = r && packagedType_1(b, l + 1);
    exit_section_(b, m, EOL_PACKAGED_TYPE, r);
    return r;
  }

  // ('::' ID)*
  private static boolean packagedType_1(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "packagedType_1")) return false;
    while (true) {
      int c = current_position_(b);
      if (!packagedType_1_0(b, l + 1)) break;
      if (!empty_element_parsed_guard_(b, "packagedType_1", c)) break;
    }
    return true;
  }

  // '::' ID
  private static boolean packagedType_1_0(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "packagedType_1_0")) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = consumeTokens(b, 0, EOL_QUAL_OP, EOL_ID);
    exit_section_(b, m, null, r);
    return r;
  }

  /* ********************************************************** */
  // '(' expressionList? ')'
  public static boolean parameterList(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "parameterList")) return false;
    if (!nextTokenIs(b, EOL_LEFT_PAREN)) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = consumeToken(b, EOL_LEFT_PAREN);
    r = r && parameterList_1(b, l + 1);
    r = r && consumeToken(b, EOL_RIGHT_PAREN);
    exit_section_(b, m, EOL_PARAMETER_LIST, r);
    return r;
  }

  // expressionList?
  private static boolean parameterList_1(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "parameterList_1")) return false;
    expressionList(b, l + 1);
    return true;
  }

  /* ********************************************************** */
  // [ID '!'] packagedType ['#' ID]
  public static boolean pathName(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "pathName")) return false;
    if (!nextTokenIs(b, EOL_ID)) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = pathName_0(b, l + 1);
    r = r && packagedType(b, l + 1);
    r = r && pathName_2(b, l + 1);
    exit_section_(b, m, EOL_PATH_NAME, r);
    return r;
  }

  // [ID '!']
  private static boolean pathName_0(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "pathName_0")) return false;
    parseTokens(b, 0, EOL_ID, EOL_MODEL_OP);
    return true;
  }

  // ['#' ID]
  private static boolean pathName_2(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "pathName_2")) return false;
    parseTokens(b, 0, EOL_ENUM_OP, EOL_ID);
    return true;
  }

  /* ********************************************************** */
  // itemSelector_expr ( navigation_op (featureCall | ID) ('[' expr ']')*)*
  public static boolean postfix_expr(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "postfix_expr")) return false;
    boolean r;
    Marker m = enter_section_(b, l, _COLLAPSE_, EOL_POSTFIX_EXPR, "<postfix expr>");
    r = itemSelector_expr(b, l + 1);
    r = r && postfix_expr_1(b, l + 1);
    exit_section_(b, l, m, r, false, null);
    return r;
  }

  // ( navigation_op (featureCall | ID) ('[' expr ']')*)*
  private static boolean postfix_expr_1(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "postfix_expr_1")) return false;
    while (true) {
      int c = current_position_(b);
      if (!postfix_expr_1_0(b, l + 1)) break;
      if (!empty_element_parsed_guard_(b, "postfix_expr_1", c)) break;
    }
    return true;
  }

  // navigation_op (featureCall | ID) ('[' expr ']')*
  private static boolean postfix_expr_1_0(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "postfix_expr_1_0")) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = navigation_op(b, l + 1);
    r = r && postfix_expr_1_0_1(b, l + 1);
    r = r && postfix_expr_1_0_2(b, l + 1);
    exit_section_(b, m, null, r);
    return r;
  }

  // featureCall | ID
  private static boolean postfix_expr_1_0_1(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "postfix_expr_1_0_1")) return false;
    boolean r;
    r = featureCall(b, l + 1);
    if (!r) r = consumeToken(b, EOL_ID);
    return r;
  }

  // ('[' expr ']')*
  private static boolean postfix_expr_1_0_2(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "postfix_expr_1_0_2")) return false;
    while (true) {
      int c = current_position_(b);
      if (!postfix_expr_1_0_2_0(b, l + 1)) break;
      if (!empty_element_parsed_guard_(b, "postfix_expr_1_0_2", c)) break;
    }
    return true;
  }

  // '[' expr ']'
  private static boolean postfix_expr_1_0_2_0(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "postfix_expr_1_0_2_0")) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = consumeToken(b, EOL_LEFT_SQ);
    r = r && expr(b, l + 1);
    r = r && consumeToken(b, EOL_RIGHT_SQ);
    exit_section_(b, m, null, r);
    return r;
  }

  /* ********************************************************** */
  // variableDeclaration_expr | literalSequentialCollection | literalMapCollection
  //                  | nativeType | collectionType | expressionInBrackets | new_expr
  //                  | featureCall | pathName | literal
  public static boolean primitive_expr(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "primitive_expr")) return false;
    boolean r;
    Marker m = enter_section_(b, l, _COLLAPSE_, EOL_PRIMITIVE_EXPR, "<primitive expr>");
    r = variableDeclaration_expr(b, l + 1);
    if (!r) r = literalSequentialCollection(b, l + 1);
    if (!r) r = literalMapCollection(b, l + 1);
    if (!r) r = nativeType(b, l + 1);
    if (!r) r = collectionType(b, l + 1);
    if (!r) r = expressionInBrackets(b, l + 1);
    if (!r) r = new_expr(b, l + 1);
    if (!r) r = featureCall(b, l + 1);
    if (!r) r = pathName(b, l + 1);
    if (!r) r = literal(b, l + 1);
    exit_section_(b, l, m, r, false, null);
    return r;
  }

  /* ********************************************************** */
  // rel_op add_elmnt
  public static boolean rel_expr(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "rel_expr")) return false;
    boolean r;
    Marker m = enter_section_(b, l, _LEFT_, EOL_REL_EXPR, "<rel expr>");
    r = rel_op(b, l + 1);
    r = r && add_elmnt(b, l + 1);
    exit_section_(b, l, m, r, false, null);
    return r;
  }

  /* ********************************************************** */
  // '>' | '<' | '>=' | '<=' | '<>'
  static boolean rel_op(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "rel_op")) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = consumeToken(b, EOL_GT_OP);
    if (!r) r = consumeToken(b, EOL_LT_OP);
    if (!r) r = consumeToken(b, EOL_GE_OP);
    if (!r) r = consumeToken(b, EOL_LE_OP);
    if (!r) r = consumeToken(b, EOL_NE_OP);
    exit_section_(b, m, null, r);
    return r;
  }

  /* ********************************************************** */
  // 'return' expr? ';'
  public static boolean returnStatement(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "returnStatement")) return false;
    if (!nextTokenIs(b, EOL_RETURN_KEY)) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = consumeToken(b, EOL_RETURN_KEY);
    r = r && returnStatement_1(b, l + 1);
    r = r && consumeToken(b, EOL_SEMICOLON);
    exit_section_(b, m, EOL_RETURN_STATEMENT, r);
    return r;
  }

  // expr?
  private static boolean returnStatement_1(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "returnStatement_1")) return false;
    expr(b, l + 1);
    return true;
  }

  /* ********************************************************** */
  // module <<eof>>
  static boolean root(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "root")) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = module(b, l + 1);
    r = r && eof(b, l + 1);
    exit_section_(b, m, null, r);
    return r;
  }

  /* ********************************************************** */
  // postfix_expr ('++')?
  public static boolean shortcutOperator_expr(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "shortcutOperator_expr")) return false;
    boolean r;
    Marker m = enter_section_(b, l, _COLLAPSE_, EOL_SHORTCUT_OPERATOR_EXPR, "<shortcut operator expr>");
    r = postfix_expr(b, l + 1);
    r = r && shortcutOperator_expr_1(b, l + 1);
    exit_section_(b, l, m, r, false, null);
    return r;
  }

  // ('++')?
  private static boolean shortcutOperator_expr_1(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "shortcutOperator_expr_1")) return false;
    consumeToken(b, EOL_INC_OP);
    return true;
  }

  /* ********************************************************** */
  // ANNOTATION
  public static boolean simpleAnnotation(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "simpleAnnotation")) return false;
    if (!nextTokenIs(b, EOL_ANNOTATION)) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = consumeToken(b, EOL_ANNOTATION);
    exit_section_(b, m, EOL_SIMPLE_ANNOTATION, r);
    return r;
  }

  /* ********************************************************** */
  // (built_in | ID) parameterList
  public static boolean simpleFeatureCall(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "simpleFeatureCall")) return false;
    boolean r;
    Marker m = enter_section_(b, l, _NONE_, EOL_SIMPLE_FEATURE_CALL, "<simple feature call>");
    r = simpleFeatureCall_0(b, l + 1);
    r = r && parameterList(b, l + 1);
    exit_section_(b, l, m, r, false, null);
    return r;
  }

  // built_in | ID
  private static boolean simpleFeatureCall_0(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "simpleFeatureCall_0")) return false;
    boolean r;
    r = built_in(b, l + 1);
    if (!r) r = consumeToken(b, EOL_ID);
    return r;
  }

  /* ********************************************************** */
  // returnStatement | breakStatement | breakAllStatement | abortStatement | continueStatement
  //                      | throwStatement | deleteStatement
  static boolean simpleStatement(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "simpleStatement")) return false;
    boolean r;
    r = returnStatement(b, l + 1);
    if (!r) r = breakStatement(b, l + 1);
    if (!r) r = breakAllStatement(b, l + 1);
    if (!r) r = abortStatement(b, l + 1);
    if (!r) r = continueStatement(b, l + 1);
    if (!r) r = throwStatement(b, l + 1);
    if (!r) r = deleteStatement(b, l + 1);
    return r;
  }

  /* ********************************************************** */
  // blockStatement | simpleStatement
  public static boolean statement(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "statement")) return false;
    boolean r;
    Marker m = enter_section_(b, l, _NONE_, EOL_STATEMENT, "<statement>");
    r = blockStatement(b, l + 1);
    if (!r) r = simpleStatement(b, l + 1);
    exit_section_(b, l, m, r, false, null);
    return r;
  }

  /* ********************************************************** */
  // '{' statement* '}'
  public static boolean statementBlock(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "statementBlock")) return false;
    if (!nextTokenIs(b, EOL_LEFT_BRACE)) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = consumeToken(b, EOL_LEFT_BRACE);
    r = r && statementBlock_1(b, l + 1);
    r = r && consumeToken(b, EOL_RIGHT_BRACE);
    exit_section_(b, m, EOL_STATEMENT_BLOCK, r);
    return r;
  }

  // statement*
  private static boolean statementBlock_1(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "statementBlock_1")) return false;
    while (true) {
      int c = current_position_(b);
      if (!statement(b, l + 1)) break;
      if (!empty_element_parsed_guard_(b, "statementBlock_1", c)) break;
    }
    return true;
  }

  /* ********************************************************** */
  // statement | statementBlock
  static boolean statementOrStatementBlock(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "statementOrStatementBlock")) return false;
    boolean r;
    r = statement(b, l + 1);
    if (!r) r = statementBlock(b, l + 1);
    return r;
  }

  /* ********************************************************** */
  // 'switch' '(' expr ')'  '{' caseStatement* defaultStatement? '}'
  public static boolean switchStatement(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "switchStatement")) return false;
    if (!nextTokenIs(b, EOL_SWITCH_KEY)) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = consumeTokens(b, 0, EOL_SWITCH_KEY, EOL_LEFT_PAREN);
    r = r && expr(b, l + 1);
    r = r && consumeTokens(b, 0, EOL_RIGHT_PAREN, EOL_LEFT_BRACE);
    r = r && switchStatement_5(b, l + 1);
    r = r && switchStatement_6(b, l + 1);
    r = r && consumeToken(b, EOL_RIGHT_BRACE);
    exit_section_(b, m, EOL_SWITCH_STATEMENT, r);
    return r;
  }

  // caseStatement*
  private static boolean switchStatement_5(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "switchStatement_5")) return false;
    while (true) {
      int c = current_position_(b);
      if (!caseStatement(b, l + 1)) break;
      if (!empty_element_parsed_guard_(b, "switchStatement_5", c)) break;
    }
    return true;
  }

  // defaultStatement?
  private static boolean switchStatement_6(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "switchStatement_6")) return false;
    defaultStatement(b, l + 1);
    return true;
  }

  /* ********************************************************** */
  // 'throw' expr? ';'
  public static boolean throwStatement(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "throwStatement")) return false;
    if (!nextTokenIs(b, EOL_THROW_KEY)) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = consumeToken(b, EOL_THROW_KEY);
    r = r && throwStatement_1(b, l + 1);
    r = r && consumeToken(b, EOL_SEMICOLON);
    exit_section_(b, m, EOL_THROW_STATEMENT, r);
    return r;
  }

  // expr?
  private static boolean throwStatement_1(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "throwStatement_1")) return false;
    expr(b, l + 1);
    return true;
  }

  /* ********************************************************** */
  // 'transaction' idList statementOrStatementBlock
  public static boolean transactionStatement(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "transactionStatement")) return false;
    if (!nextTokenIs(b, EOL_TRANS_KEY)) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = consumeToken(b, EOL_TRANS_KEY);
    r = r && idList(b, l + 1);
    r = r && statementOrStatementBlock(b, l + 1);
    exit_section_(b, m, EOL_TRANSACTION_STATEMENT, r);
    return r;
  }

  /* ********************************************************** */
  // nativeType | collectionType | pathName
  public static boolean typeName(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "typeName")) return false;
    boolean r;
    Marker m = enter_section_(b, l, _NONE_, EOL_TYPE_NAME, "<type name>");
    r = nativeType(b, l + 1);
    if (!r) r = collectionType(b, l + 1);
    if (!r) r = pathName(b, l + 1);
    exit_section_(b, l, m, r, false, null);
    return r;
  }

  /* ********************************************************** */
  // <<list typeName>>
  public static boolean typeNameList(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "typeNameList")) return false;
    boolean r;
    Marker m = enter_section_(b, l, _NONE_, EOL_TYPE_NAME_LIST, "<type name list>");
    r = list(b, l + 1, typeName_parser_);
    exit_section_(b, l, m, r, false, null);
    return r;
  }

  /* ********************************************************** */
  // uny_op? shortcutOperator_expr
  static boolean uny_elmnt(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "uny_elmnt")) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = uny_elmnt_0(b, l + 1);
    r = r && shortcutOperator_expr(b, l + 1);
    exit_section_(b, m, null, r);
    return r;
  }

  // uny_op?
  private static boolean uny_elmnt_0(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "uny_elmnt_0")) return false;
    uny_op(b, l + 1);
    return true;
  }

  /* ********************************************************** */
  // 'not' | '-'
  static boolean uny_op(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "uny_op")) return false;
    if (!nextTokenIs(b, "", EOL_NEG_OP, EOL_NOT_OP)) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = consumeToken(b, EOL_NOT_OP);
    if (!r) r = consumeToken(b, EOL_NEG_OP);
    exit_section_(b, m, null, r);
    return r;
  }

  /* ********************************************************** */
  // variable_type ID (':' 'new'? typeName parameterList?)?
  public static boolean variableDeclaration_expr(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "variableDeclaration_expr")) return false;
    if (!nextTokenIs(b, "<variable declaration expr>", EOL_EXT_KEY, EOL_VAR_KEY)) return false;
    boolean r;
    Marker m = enter_section_(b, l, _NONE_, EOL_VARIABLE_DECLARATION_EXPR, "<variable declaration expr>");
    r = variable_type(b, l + 1);
    r = r && consumeToken(b, EOL_ID);
    r = r && variableDeclaration_expr_2(b, l + 1);
    exit_section_(b, l, m, r, false, null);
    return r;
  }

  // (':' 'new'? typeName parameterList?)?
  private static boolean variableDeclaration_expr_2(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "variableDeclaration_expr_2")) return false;
    variableDeclaration_expr_2_0(b, l + 1);
    return true;
  }

  // ':' 'new'? typeName parameterList?
  private static boolean variableDeclaration_expr_2_0(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "variableDeclaration_expr_2_0")) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = consumeToken(b, EOL_COLON_OP);
    r = r && variableDeclaration_expr_2_0_1(b, l + 1);
    r = r && typeName(b, l + 1);
    r = r && variableDeclaration_expr_2_0_3(b, l + 1);
    exit_section_(b, m, null, r);
    return r;
  }

  // 'new'?
  private static boolean variableDeclaration_expr_2_0_1(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "variableDeclaration_expr_2_0_1")) return false;
    consumeToken(b, EOL_NEW_KEY);
    return true;
  }

  // parameterList?
  private static boolean variableDeclaration_expr_2_0_3(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "variableDeclaration_expr_2_0_3")) return false;
    parameterList(b, l + 1);
    return true;
  }

  /* ********************************************************** */
  // 'var' | 'ext'
  static boolean variable_type(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "variable_type")) return false;
    if (!nextTokenIs(b, "", EOL_EXT_KEY, EOL_VAR_KEY)) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = consumeToken(b, EOL_VAR_KEY);
    if (!r) r = consumeToken(b, EOL_EXT_KEY);
    exit_section_(b, m, null, r);
    return r;
  }

  /* ********************************************************** */
  // 'while' '(' expr ')' statementOrStatementBlock
  public static boolean whileStatement(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "whileStatement")) return false;
    if (!nextTokenIs(b, EOL_WHILE_KEY)) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = consumeTokens(b, 0, EOL_WHILE_KEY, EOL_LEFT_PAREN);
    r = r && expr(b, l + 1);
    r = r && consumeToken(b, EOL_RIGHT_PAREN);
    r = r && statementOrStatementBlock(b, l + 1);
    exit_section_(b, m, EOL_WHILE_STATEMENT, r);
    return r;
  }

  static final Parser EOL_ID_parser_ = new Parser() {
    public boolean parse(PsiBuilder b, int l) {
      return consumeToken(b, EOL_ID);
    }
  };
  static final Parser expr_parser_ = new Parser() {
    public boolean parse(PsiBuilder b, int l) {
      return expr(b, l + 1);
    }
  };
  static final Parser formalParameter_parser_ = new Parser() {
    public boolean parse(PsiBuilder b, int l) {
      return formalParameter(b, l + 1);
    }
  };
  static final Parser keyvalExpression_parser_ = new Parser() {
    public boolean parse(PsiBuilder b, int l) {
      return keyvalExpression(b, l + 1);
    }
  };
  static final Parser typeName_parser_ = new Parser() {
    public boolean parse(PsiBuilder b, int l) {
      return typeName(b, l + 1);
    }
  };
}

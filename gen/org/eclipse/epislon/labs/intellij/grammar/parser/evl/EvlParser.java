/*******************************************************************************
 * Copyright (c) 2008 The University of York.
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *     Horacio Hoyos Rodriguez - initial API and implementation
 ******************************************************************************/
package org.eclipse.epislon.labs.intellij.grammar.parser.evl;

import com.intellij.lang.PsiBuilder;
import com.intellij.lang.PsiBuilder.Marker;
import static org.eclipse.epislon.labs.intellij.grammar.psi.evl.EvlTypes.*;
import static com.intellij.lang.parser.GeneratedParserUtilBase.*;
import com.intellij.psi.tree.IElementType;
import com.intellij.lang.ASTNode;
import com.intellij.psi.tree.TokenSet;
import com.intellij.lang.PsiParser;
import com.intellij.lang.LightPsiParser;
import static org.eclipse.epislon.labs.intellij.grammar.parser.EpsilonParserUtil.*;

@SuppressWarnings({"SimplifiableIfStatement", "UnusedAssignment"})
public class EvlParser implements PsiParser, LightPsiParser {

  public ASTNode parse(IElementType t, PsiBuilder b) {
    parseLight(t, b);
    return b.getTreeBuilt();
  }

  public void parseLight(IElementType t, PsiBuilder b) {
    boolean r;
    b = adapt_builder_(t, b, this, null);
    Marker m = enter_section_(b, 0, _COLLAPSE_, null);
    r = parse_root_(t, b);
    exit_section_(b, 0, m, t, r, true, TRUE_CONDITION);
  }

  protected boolean parse_root_(IElementType t, PsiBuilder b) {
    return parse_root_(t, b, 0);
  }

  static boolean parse_root_(IElementType t, PsiBuilder b, int l) {
    return root(b, l + 1);
  }

  /* ********************************************************** */
  // 'check' expressionOrStatementBlock
  public static boolean check(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "check")) return false;
    if (!nextTokenIs(b, EVL_CHECK_KEY)) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = consumeToken(b, EVL_CHECK_KEY);
    r = r && eolExprOrStatementBlock(b, l + 1);
    exit_section_(b, m, EVL_CHECK, r);
    return r;
  }

  /* ********************************************************** */
  // 'constraint' ID invariantBlock
  public static boolean constraint(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "constraint")) return false;
    if (!nextTokenIs(b, EVL_CONSTRAINT_KEY)) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = consumeToken(b, EVL_CONSTRAINT_KEY);
    r = r && eolId(b, l + 1);
    r = r && invariantBlock(b, l + 1);
    exit_section_(b, m, EVL_CONSTRAINT, r);
    return r;
  }

  /* ********************************************************** */
  // (context | constraint | critique | pre | post | annotationBlock |  operationDeclaration)*
  public static boolean content(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "content")) return false;
    Marker m = enter_section_(b, l, _NONE_, EVL_CONTENT, "<content>");
    while (true) {
      int c = current_position_(b);
      if (!content_0(b, l + 1)) break;
      if (!empty_element_parsed_guard_(b, "content", c)) break;
    }
    exit_section_(b, l, m, true, false, null);
    return true;
  }

  // context | constraint | critique | pre | post | annotationBlock |  operationDeclaration
  private static boolean content_0(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "content_0")) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = context(b, l + 1);
    if (!r) r = constraint(b, l + 1);
    if (!r) r = critique(b, l + 1);
    if (!r) r = pre(b, l + 1);
    if (!r) r = post(b, l + 1);
    if (!r) r = eolAnnotationBlock(b, l + 1);
    if (!r) r = eolOperationDeclaration(b, l + 1);
    exit_section_(b, m, null, r);
    return r;
  }

  /* ********************************************************** */
  // 'context' typeName contextBlock
  public static boolean context(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "context")) return false;
    if (!nextTokenIs(b, EVL_CONTEXT_KEY)) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = consumeToken(b, EVL_CONTEXT_KEY);
    r = r && eolTypeName(b, l + 1);
    r = r && contextBlock(b, l + 1);
    exit_section_(b, m, EVL_CONTEXT, r);
    return r;
  }

  /* ********************************************************** */
  // EOL_LB guard? contextContent* EOL_RB
  public static boolean contextBlock(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "contextBlock")) return false;
    boolean r;
    Marker m = enter_section_(b, l, _NONE_, EVL_CONTEXT_BLOCK, "<context block>");
    r = eolLeftBrace(b, l + 1);
    r = r && contextBlock_1(b, l + 1);
    r = r && contextBlock_2(b, l + 1);
    r = r && eolRightBrace(b, l + 1);
    exit_section_(b, l, m, r, false, null);
    return r;
  }

  // guard?
  private static boolean contextBlock_1(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "contextBlock_1")) return false;
    guard(b, l + 1);
    return true;
  }

  // contextContent*
  private static boolean contextBlock_2(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "contextBlock_2")) return false;
    while (true) {
      int c = current_position_(b);
      if (!contextContent(b, l + 1)) break;
      if (!empty_element_parsed_guard_(b, "contextBlock_2", c)) break;
    }
    return true;
  }

  /* ********************************************************** */
  // constraint | critique | annotationBlock
  public static boolean contextContent(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "contextContent")) return false;
    boolean r;
    Marker m = enter_section_(b, l, _NONE_, EVL_CONTEXT_CONTENT, "<context content>");
    r = constraint(b, l + 1);
    if (!r) r = critique(b, l + 1);
    if (!r) r = eolAnnotationBlock(b, l + 1);
    exit_section_(b, l, m, r, false, null);
    return r;
  }

  /* ********************************************************** */
  // 'critique' ID invariantBlock
  public static boolean critique(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "critique")) return false;
    if (!nextTokenIs(b, EVL_CRITIQUE_KEY)) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = consumeToken(b, EVL_CRITIQUE_KEY);
    r = r && eolId(b, l + 1);
    r = r && invariantBlock(b, l + 1);
    exit_section_(b, m, EVL_CRITIQUE, r);
    return r;
  }

  /* ********************************************************** */
  // 'fix' fixBlock
  public static boolean fix(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "fix")) return false;
    if (!nextTokenIs(b, EVL_FIX_KEY)) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = consumeToken(b, EVL_FIX_KEY);
    r = r && fixBlock(b, l + 1);
    exit_section_(b, m, EVL_FIX, r);
    return r;
  }

  /* ********************************************************** */
  // EOL_LB guard? title fixBody EOL_RB
  public static boolean fixBlock(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "fixBlock")) return false;
    boolean r;
    Marker m = enter_section_(b, l, _NONE_, EVL_FIX_BLOCK, "<fix block>");
    r = eolLeftBrace(b, l + 1);
    r = r && fixBlock_1(b, l + 1);
    r = r && title(b, l + 1);
    r = r && fixBody(b, l + 1);
    r = r && eolRightBrace(b, l + 1);
    exit_section_(b, l, m, r, false, null);
    return r;
  }

  // guard?
  private static boolean fixBlock_1(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "fixBlock_1")) return false;
    guard(b, l + 1);
    return true;
  }

  /* ********************************************************** */
  // 'do' statementBlock
  public static boolean fixBody(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "fixBody")) return false;
    if (!nextTokenIs(b, EVL_DO_KEY)) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = consumeToken(b, EVL_DO_KEY);
    r = r && eolStatementBlock(b, l + 1);
    exit_section_(b, m, EVL_FIX_BODY, r);
    return r;
  }

  /* ********************************************************** */
  // erlGuard
  public static boolean guard(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "guard")) return false;
    boolean r;
    Marker m = enter_section_(b, l, _NONE_, EVL_GUARD, "<guard>");
    r = erlGuard(b, l + 1);
    exit_section_(b, l, m, r, false, null);
    return r;
  }

  /* ********************************************************** */
  // EOL_LB guard? check message? fix* EOL_RB
  public static boolean invariantBlock(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "invariantBlock")) return false;
    boolean r;
    Marker m = enter_section_(b, l, _NONE_, EVL_INVARIANT_BLOCK, "<invariant block>");
    r = eolLeftBrace(b, l + 1);
    r = r && invariantBlock_1(b, l + 1);
    r = r && check(b, l + 1);
    r = r && invariantBlock_3(b, l + 1);
    r = r && invariantBlock_4(b, l + 1);
    r = r && eolRightBrace(b, l + 1);
    exit_section_(b, l, m, r, false, null);
    return r;
  }

  // guard?
  private static boolean invariantBlock_1(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "invariantBlock_1")) return false;
    guard(b, l + 1);
    return true;
  }

  // message?
  private static boolean invariantBlock_3(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "invariantBlock_3")) return false;
    message(b, l + 1);
    return true;
  }

  // fix*
  private static boolean invariantBlock_4(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "invariantBlock_4")) return false;
    while (true) {
      int c = current_position_(b);
      if (!fix(b, l + 1)) break;
      if (!empty_element_parsed_guard_(b, "invariantBlock_4", c)) break;
    }
    return true;
  }

  /* ********************************************************** */
  // 'message' expressionOrStatementBlock
  public static boolean message(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "message")) return false;
    if (!nextTokenIs(b, EVL_MESSAGE_KEY)) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = consumeToken(b, EVL_MESSAGE_KEY);
    r = r && eolExprOrStatementBlock(b, l + 1);
    exit_section_(b, m, EVL_MESSAGE, r);
    return r;
  }

  /* ********************************************************** */
  // importStatement* modelDeclaration* content
  public static boolean module(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "module")) return false;
    boolean r;
    Marker m = enter_section_(b, l, _NONE_, EVL_MODULE, "<module>");
    r = module_0(b, l + 1);
    r = r && module_1(b, l + 1);
    r = r && content(b, l + 1);
    exit_section_(b, l, m, r, false, null);
    return r;
  }

  // importStatement*
  private static boolean module_0(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "module_0")) return false;
    while (true) {
      int c = current_position_(b);
      if (!eolImportStatement(b, l + 1)) break;
      if (!empty_element_parsed_guard_(b, "module_0", c)) break;
    }
    return true;
  }

  // modelDeclaration*
  private static boolean module_1(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "module_1")) return false;
    while (true) {
      int c = current_position_(b);
      if (!eolModelDeclaration(b, l + 1)) break;
      if (!empty_element_parsed_guard_(b, "module_1", c)) break;
    }
    return true;
  }

  /* ********************************************************** */
  // ERL_POST ID? EOL_LB statement* EOL_RB
  public static boolean post(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "post")) return false;
    boolean r;
    Marker m = enter_section_(b, l, _NONE_, EVL_POST, "<post>");
    r = erlPost(b, l + 1);
    r = r && post_1(b, l + 1);
    r = r && eolLeftBrace(b, l + 1);
    r = r && post_3(b, l + 1);
    r = r && eolRightBrace(b, l + 1);
    exit_section_(b, l, m, r, false, null);
    return r;
  }

  // ID?
  private static boolean post_1(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "post_1")) return false;
    eolId(b, l + 1);
    return true;
  }

  // statement*
  private static boolean post_3(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "post_3")) return false;
    while (true) {
      int c = current_position_(b);
      if (!statement(b, l + 1)) break;
      if (!empty_element_parsed_guard_(b, "post_3", c)) break;
    }
    return true;
  }

  /* ********************************************************** */
  // ERL_PRE ID? EOL_LB statement* EOL_RB
  public static boolean pre(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "pre")) return false;
    boolean r;
    Marker m = enter_section_(b, l, _NONE_, EVL_PRE, "<pre>");
    r = erlPre(b, l + 1);
    r = r && pre_1(b, l + 1);
    r = r && eolLeftBrace(b, l + 1);
    r = r && pre_3(b, l + 1);
    r = r && eolRightBrace(b, l + 1);
    exit_section_(b, l, m, r, false, null);
    return r;
  }

  // ID?
  private static boolean pre_1(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "pre_1")) return false;
    eolId(b, l + 1);
    return true;
  }

  // statement*
  private static boolean pre_3(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "pre_3")) return false;
    while (true) {
      int c = current_position_(b);
      if (!statement(b, l + 1)) break;
      if (!empty_element_parsed_guard_(b, "pre_3", c)) break;
    }
    return true;
  }

  /* ********************************************************** */
  // module <<eof>>
  static boolean root(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "root")) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = module(b, l + 1);
    r = r && eof(b, l + 1);
    exit_section_(b, m, null, r);
    return r;
  }

  /* ********************************************************** */
  // eolStatement
  public static boolean statement(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "statement")) return false;
    boolean r;
    Marker m = enter_section_(b, l, _NONE_, EVL_STATEMENT, "<statement>");
    r = eolStatement(b, l + 1);
    exit_section_(b, l, m, r, false, null);
    return r;
  }

  /* ********************************************************** */
  // 'title' expressionOrStatementBlock
  public static boolean title(PsiBuilder b, int l) {
    if (!recursion_guard_(b, l, "title")) return false;
    if (!nextTokenIs(b, EVL_TITLE_KEY)) return false;
    boolean r;
    Marker m = enter_section_(b);
    r = consumeToken(b, EVL_TITLE_KEY);
    r = r && eolExprOrStatementBlock(b, l + 1);
    exit_section_(b, m, EVL_TITLE, r);
    return r;
  }

}

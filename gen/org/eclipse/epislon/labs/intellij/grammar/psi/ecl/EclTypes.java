/*******************************************************************************
 * Copyright (c) 2008 The University of York.
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *     Horacio Hoyos Rodriguez - initial API and implementation
 ******************************************************************************/
package org.eclipse.epislon.labs.intellij.grammar.psi.ecl;

import com.intellij.psi.tree.IElementType;
import com.intellij.psi.PsiElement;
import com.intellij.lang.ASTNode;
import org.eclipse.epislon.labs.intellij.grammar.psi.ecl.impl.*;

public interface EclTypes {

  IElementType ECL_COMPARE_BLOCK = new EclElementType("ECL_COMPARE_BLOCK");
  IElementType ECL_CONTENT = new EclElementType("ECL_CONTENT");
  IElementType ECL_DO_BLOCK = new EclElementType("ECL_DO_BLOCK");
  IElementType ECL_EXTENDS = new EclElementType("ECL_EXTENDS");
  IElementType ECL_FORMAL_PARAMETER = new EclElementType("ECL_FORMAL_PARAMETER");
  IElementType ECL_GUARD = new EclElementType("ECL_GUARD");
  IElementType ECL_MATCH_RULE = new EclElementType("ECL_MATCH_RULE");
  IElementType ECL_MODULE = new EclElementType("ECL_MODULE");
  IElementType ECL_POST = new EclElementType("ECL_POST");
  IElementType ECL_PRE = new EclElementType("ECL_PRE");
  IElementType ECL_STATEMENT = new EclElementType("ECL_STATEMENT");

  IElementType ECL_COMPARE_KEY = new EclTokenType("compare");
  IElementType ECL_DO_KEY = new EclTokenType("do");
  IElementType ECL_MATCH_KEY = new EclTokenType("match");
  IElementType ECL_RULE_KEY = new EclTokenType("rule");
  IElementType ECL_WITH_KEY = new EclTokenType("with");

  class Factory {
    public static PsiElement createElement(ASTNode node) {
      IElementType type = node.getElementType();
      if (type == ECL_COMPARE_BLOCK) {
        return new EclCompareBlockImpl(node);
      }
      else if (type == ECL_CONTENT) {
        return new EclContentImpl(node);
      }
      else if (type == ECL_DO_BLOCK) {
        return new EclDoBlockImpl(node);
      }
      else if (type == ECL_EXTENDS) {
        return new EclExtendsImpl(node);
      }
      else if (type == ECL_FORMAL_PARAMETER) {
        return new EclFormalParameterImpl(node);
      }
      else if (type == ECL_GUARD) {
        return new EclGuardImpl(node);
      }
      else if (type == ECL_MATCH_RULE) {
        return new EclMatchRuleImpl(node);
      }
      else if (type == ECL_MODULE) {
        return new EclModuleImpl(node);
      }
      else if (type == ECL_POST) {
        return new EclPostImpl(node);
      }
      else if (type == ECL_PRE) {
        return new EclPreImpl(node);
      }
      else if (type == ECL_STATEMENT) {
        return new EclStatementImpl(node);
      }
      throw new AssertionError("Unknown element type: " + type);
    }
  }
}

/*******************************************************************************
 * Copyright (c) 2008 The University of York.
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *     Horacio Hoyos Rodriguez - initial API and implementation
 ******************************************************************************/
package org.eclipse.epislon.labs.intellij.grammar.psi.eol.impl;

import java.util.List;
import org.jetbrains.annotations.*;
import com.intellij.lang.ASTNode;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiElementVisitor;
import com.intellij.psi.util.PsiTreeUtil;
import static org.eclipse.epislon.labs.intellij.grammar.psi.eol.EolTypes.*;
import com.intellij.extapi.psi.ASTWrapperPsiElement;
import org.eclipse.epislon.labs.intellij.grammar.psi.eol.*;

public class EolStatementImpl extends ASTWrapperPsiElement implements EolStatement {

  public EolStatementImpl(@NotNull ASTNode node) {
    super(node);
  }

  public void accept(@NotNull EolVisitor visitor) {
    visitor.visitStatement(this);
  }

  public void accept(@NotNull PsiElementVisitor visitor) {
    if (visitor instanceof EolVisitor) accept((EolVisitor)visitor);
    else super.accept(visitor);
  }

  @Override
  @Nullable
  public EolAbortStatement getAbortStatement() {
    return findChildByClass(EolAbortStatement.class);
  }

  @Override
  @Nullable
  public EolAssignmentStatement getAssignmentStatement() {
    return findChildByClass(EolAssignmentStatement.class);
  }

  @Override
  @Nullable
  public EolBreakAllStatement getBreakAllStatement() {
    return findChildByClass(EolBreakAllStatement.class);
  }

  @Override
  @Nullable
  public EolBreakStatement getBreakStatement() {
    return findChildByClass(EolBreakStatement.class);
  }

  @Override
  @Nullable
  public EolContinueStatement getContinueStatement() {
    return findChildByClass(EolContinueStatement.class);
  }

  @Override
  @Nullable
  public EolDeleteStatement getDeleteStatement() {
    return findChildByClass(EolDeleteStatement.class);
  }

  @Override
  @Nullable
  public EolExpressionStatement getExpressionStatement() {
    return findChildByClass(EolExpressionStatement.class);
  }

  @Override
  @Nullable
  public EolForStatement getForStatement() {
    return findChildByClass(EolForStatement.class);
  }

  @Override
  @Nullable
  public EolIfStatement getIfStatement() {
    return findChildByClass(EolIfStatement.class);
  }

  @Override
  @Nullable
  public EolReturnStatement getReturnStatement() {
    return findChildByClass(EolReturnStatement.class);
  }

  @Override
  @Nullable
  public EolSwitchStatement getSwitchStatement() {
    return findChildByClass(EolSwitchStatement.class);
  }

  @Override
  @Nullable
  public EolThrowStatement getThrowStatement() {
    return findChildByClass(EolThrowStatement.class);
  }

  @Override
  @Nullable
  public EolTransactionStatement getTransactionStatement() {
    return findChildByClass(EolTransactionStatement.class);
  }

  @Override
  @Nullable
  public EolWhileStatement getWhileStatement() {
    return findChildByClass(EolWhileStatement.class);
  }

}

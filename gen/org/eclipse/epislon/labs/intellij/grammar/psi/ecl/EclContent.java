/*******************************************************************************
 * Copyright (c) 2008 The University of York.
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *     Horacio Hoyos Rodriguez - initial API and implementation
 ******************************************************************************/
package org.eclipse.epislon.labs.intellij.grammar.psi.ecl;

import java.util.List;
import org.jetbrains.annotations.*;
import com.intellij.psi.PsiElement;
import org.eclipse.epislon.labs.intellij.grammar.psi.eol.EolAnnotationBlock;
import org.eclipse.epislon.labs.intellij.grammar.psi.eol.EolOperationDeclaration;

public interface EclContent extends PsiElement {

  @NotNull
  List<EclMatchRule> getMatchRuleList();

  @NotNull
  List<EclPost> getPostList();

  @NotNull
  List<EclPre> getPreList();

  @NotNull
  List<EolAnnotationBlock> getAnnotationBlockList();

  @NotNull
  List<EolOperationDeclaration> getOperationDeclarationList();

}

/*******************************************************************************
 * Copyright (c) 2008 The University of York.
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *     Horacio Hoyos Rodriguez - initial API and implementation
 ******************************************************************************/
package org.eclipse.epislon.labs.intellij.grammar.psi.eol;

import com.intellij.psi.tree.IElementType;
import com.intellij.psi.PsiElement;
import com.intellij.lang.ASTNode;
import org.eclipse.epislon.labs.intellij.grammar.psi.eol.impl.*;

public interface EolTypes {

  IElementType EOL_ABORT_STATEMENT = new EolElementType("EOL_ABORT_STATEMENT");
  IElementType EOL_ADD_EXPR = new EolElementType("EOL_ADD_EXPR");
  IElementType EOL_ANNOTATION_BLOCK = new EolElementType("EOL_ANNOTATION_BLOCK");
  IElementType EOL_ASSIGNMENT_STATEMENT = new EolElementType("EOL_ASSIGNMENT_STATEMENT");
  IElementType EOL_BOOLEAN = new EolElementType("EOL_BOOLEAN");
  IElementType EOL_BOOL_EXPR = new EolElementType("EOL_BOOL_EXPR");
  IElementType EOL_BREAK_ALL_STATEMENT = new EolElementType("EOL_BREAK_ALL_STATEMENT");
  IElementType EOL_BREAK_STATEMENT = new EolElementType("EOL_BREAK_STATEMENT");
  IElementType EOL_BUILT_IN = new EolElementType("EOL_BUILT_IN");
  IElementType EOL_CASE_STATEMENT = new EolElementType("EOL_CASE_STATEMENT");
  IElementType EOL_COLLECTION_TYPE = new EolElementType("EOL_COLLECTION_TYPE");
  IElementType EOL_COMPLEX_FEATURE_CALL = new EolElementType("EOL_COMPLEX_FEATURE_CALL");
  IElementType EOL_CONTENT = new EolElementType("EOL_CONTENT");
  IElementType EOL_CONTINUE_STATEMENT = new EolElementType("EOL_CONTINUE_STATEMENT");
  IElementType EOL_DEFAULT_STATEMENT = new EolElementType("EOL_DEFAULT_STATEMENT");
  IElementType EOL_DELETE_STATEMENT = new EolElementType("EOL_DELETE_STATEMENT");
  IElementType EOL_ELSE_STATEMENT = new EolElementType("EOL_ELSE_STATEMENT");
  IElementType EOL_EQ_EXPR = new EolElementType("EOL_EQ_EXPR");
  IElementType EOL_EXECUTABLE_ANNOTATION = new EolElementType("EOL_EXECUTABLE_ANNOTATION");
  IElementType EOL_EXPR = new EolElementType("EOL_EXPR");
  IElementType EOL_EXPRESSION_IN_BRACKETS = new EolElementType("EOL_EXPRESSION_IN_BRACKETS");
  IElementType EOL_EXPRESSION_LIST_OR_RANGE = new EolElementType("EOL_EXPRESSION_LIST_OR_RANGE");
  IElementType EOL_EXPRESSION_RANGE = new EolElementType("EOL_EXPRESSION_RANGE");
  IElementType EOL_EXPRESSION_STATEMENT = new EolElementType("EOL_EXPRESSION_STATEMENT");
  IElementType EOL_EXPR_OR_STATEMENT_BLOCK = new EolElementType("EOL_EXPR_OR_STATEMENT_BLOCK");
  IElementType EOL_FEATURE_CALL = new EolElementType("EOL_FEATURE_CALL");
  IElementType EOL_FORMAL_PARAMETER = new EolElementType("EOL_FORMAL_PARAMETER");
  IElementType EOL_FORMAL_PARAMETER_LIST = new EolElementType("EOL_FORMAL_PARAMETER_LIST");
  IElementType EOL_FOR_STATEMENT = new EolElementType("EOL_FOR_STATEMENT");
  IElementType EOL_ID_LIST = new EolElementType("EOL_ID_LIST");
  IElementType EOL_IF_STATEMENT = new EolElementType("EOL_IF_STATEMENT");
  IElementType EOL_IMPORT_STATEMENT = new EolElementType("EOL_IMPORT_STATEMENT");
  IElementType EOL_ITEM_SELECTOR_EXPR = new EolElementType("EOL_ITEM_SELECTOR_EXPR");
  IElementType EOL_KEYVAL_EXPRESSION = new EolElementType("EOL_KEYVAL_EXPRESSION");
  IElementType EOL_LAMBDA_EXPRESSION = new EolElementType("EOL_LAMBDA_EXPRESSION");
  IElementType EOL_LAMBDA_EXPRESSION_IN_BRACKETS = new EolElementType("EOL_LAMBDA_EXPRESSION_IN_BRACKETS");
  IElementType EOL_LITERAL = new EolElementType("EOL_LITERAL");
  IElementType EOL_LITERAL_MAP_COLLECTION = new EolElementType("EOL_LITERAL_MAP_COLLECTION");
  IElementType EOL_LITERAL_SEQUENTIAL_COLLECTION = new EolElementType("EOL_LITERAL_SEQUENTIAL_COLLECTION");
  IElementType EOL_MODEL_ALIAS = new EolElementType("EOL_MODEL_ALIAS");
  IElementType EOL_MODEL_DECLARATION = new EolElementType("EOL_MODEL_DECLARATION");
  IElementType EOL_MODEL_DECLARATION_PARAMETER = new EolElementType("EOL_MODEL_DECLARATION_PARAMETER");
  IElementType EOL_MODEL_DECLARATION_PARAMETERS = new EolElementType("EOL_MODEL_DECLARATION_PARAMETERS");
  IElementType EOL_MODEL_DRIVER = new EolElementType("EOL_MODEL_DRIVER");
  IElementType EOL_MODULE = new EolElementType("EOL_MODULE");
  IElementType EOL_MULT_EXPR = new EolElementType("EOL_MULT_EXPR");
  IElementType EOL_NATIVE_TYPE = new EolElementType("EOL_NATIVE_TYPE");
  IElementType EOL_NEW_EXPR = new EolElementType("EOL_NEW_EXPR");
  IElementType EOL_OPERATION_DECLARATION = new EolElementType("EOL_OPERATION_DECLARATION");
  IElementType EOL_OPERATION_DECLARATION_OR_ANNOTATION_BLOCK = new EolElementType("EOL_OPERATION_DECLARATION_OR_ANNOTATION_BLOCK");
  IElementType EOL_OPERATION_START = new EolElementType("EOL_OPERATION_START");
  IElementType EOL_PACKAGED_TYPE = new EolElementType("EOL_PACKAGED_TYPE");
  IElementType EOL_PARAMETER_LIST = new EolElementType("EOL_PARAMETER_LIST");
  IElementType EOL_PATH_NAME = new EolElementType("EOL_PATH_NAME");
  IElementType EOL_POSTFIX_EXPR = new EolElementType("EOL_POSTFIX_EXPR");
  IElementType EOL_PRIMITIVE_EXPR = new EolElementType("EOL_PRIMITIVE_EXPR");
  IElementType EOL_REL_EXPR = new EolElementType("EOL_REL_EXPR");
  IElementType EOL_RETURN_STATEMENT = new EolElementType("EOL_RETURN_STATEMENT");
  IElementType EOL_SHORTCUT_OPERATOR_EXPR = new EolElementType("EOL_SHORTCUT_OPERATOR_EXPR");
  IElementType EOL_SIMPLE_ANNOTATION = new EolElementType("EOL_SIMPLE_ANNOTATION");
  IElementType EOL_SIMPLE_FEATURE_CALL = new EolElementType("EOL_SIMPLE_FEATURE_CALL");
  IElementType EOL_STATEMENT = new EolElementType("EOL_STATEMENT");
  IElementType EOL_STATEMENT_BLOCK = new EolElementType("EOL_STATEMENT_BLOCK");
  IElementType EOL_SWITCH_STATEMENT = new EolElementType("EOL_SWITCH_STATEMENT");
  IElementType EOL_THROW_STATEMENT = new EolElementType("EOL_THROW_STATEMENT");
  IElementType EOL_TRANSACTION_STATEMENT = new EolElementType("EOL_TRANSACTION_STATEMENT");
  IElementType EOL_TYPE_NAME = new EolElementType("EOL_TYPE_NAME");
  IElementType EOL_TYPE_NAME_LIST = new EolElementType("EOL_TYPE_NAME_LIST");
  IElementType EOL_VARIABLE_DECLARATION_EXPR = new EolElementType("EOL_VARIABLE_DECLARATION_EXPR");
  IElementType EOL_WHILE_STATEMENT = new EolElementType("EOL_WHILE_STATEMENT");

  IElementType EOL_ABORT_KEY = new EolTokenType("abort");
  IElementType EOL_AND_OP = new EolTokenType("and");
  IElementType EOL_ANNOTATION = new EolTokenType("ANNOTATION");
  IElementType EOL_ARROW_OP = new EolTokenType("->");
  IElementType EOL_ASSIGN_OP = new EolTokenType("=");
  IElementType EOL_BAG_KEY = new EolTokenType("Bag");
  IElementType EOL_BLOCK_COMMENT = new EolTokenType("BLOCK_COMMENT");
  IElementType EOL_BREAK_ALL_KEY = new EolTokenType("breakAll");
  IElementType EOL_BREAK_KEY = new EolTokenType("break");
  IElementType EOL_CASE_KEY = new EolTokenType("case");
  IElementType EOL_COLON_OP = new EolTokenType(":");
  IElementType EOL_COL_KEY = new EolTokenType("Collection");
  IElementType EOL_COMMA = new EolTokenType(",");
  IElementType EOL_CONTINUE_KEY = new EolTokenType("continue");
  IElementType EOL_DEFAULT_KEY = new EolTokenType("default");
  IElementType EOL_DELETE_KEY = new EolTokenType("delete");
  IElementType EOL_DIV_ASSIG_OP = new EolTokenType("/=");
  IElementType EOL_DIV_OP = new EolTokenType("/");
  IElementType EOL_ELSE_KEY = new EolTokenType("else");
  IElementType EOL_ENUM_OP = new EolTokenType("#");
  IElementType EOL_EQUIVALENTS_BIN = new EolTokenType("equivalents");
  IElementType EOL_EQUIVALENT_BIN = new EolTokenType("equivalent");
  IElementType EOL_EQUIV_ASSIGN_OP = new EolTokenType("::=");
  IElementType EOL_EQ_OP = new EolTokenType("==");
  IElementType EOL_EXEC_ANNOT = new EolTokenType("$");
  IElementType EOL_EXT_KEY = new EolTokenType("ext");
  IElementType EOL_FALSE_KEY = new EolTokenType("false");
  IElementType EOL_FLOAT = new EolTokenType("FLOAT");
  IElementType EOL_FOR_KEY = new EolTokenType("for");
  IElementType EOL_FUNCTION_KEY = new EolTokenType("function");
  IElementType EOL_GE_OP = new EolTokenType(">=");
  IElementType EOL_GT_OP = new EolTokenType(">");
  IElementType EOL_HAS_MORE_BIN = new EolTokenType("hasMore");
  IElementType EOL_ID = new EolTokenType("ID");
  IElementType EOL_IF_KEY = new EolTokenType("if");
  IElementType EOL_IMPLIES_OP = new EolTokenType("implies");
  IElementType EOL_IMPORT_KEY = new EolTokenType("import");
  IElementType EOL_INC_OP = new EolTokenType("++");
  IElementType EOL_IN_KEY = new EolTokenType("in");
  IElementType EOL_IN_OP = new EolTokenType("|");
  IElementType EOL_LEFT_BRACE = new EolTokenType("{");
  IElementType EOL_LEFT_PAREN = new EolTokenType("(");
  IElementType EOL_LEFT_SQ = new EolTokenType("[");
  IElementType EOL_LE_OP = new EolTokenType("<=");
  IElementType EOL_LINE_COMMENT = new EolTokenType("LINE_COMMENT");
  IElementType EOL_LIST_KEY = new EolTokenType("List");
  IElementType EOL_LOOP_CNT_BIN = new EolTokenType("loopCount");
  IElementType EOL_LT_OP = new EolTokenType("<");
  IElementType EOL_MAP_KEY = new EolTokenType("Map");
  IElementType EOL_MATCHES_BIN = new EolTokenType("matches");
  IElementType EOL_MINUS_ASSIGN_OP = new EolTokenType("-=");
  IElementType EOL_MODEL_OP = new EolTokenType("!");
  IElementType EOL_NATIVE_KEY = new EolTokenType("Native");
  IElementType EOL_NEG_OP = new EolTokenType("-");
  IElementType EOL_NEW_KEY = new EolTokenType("new");
  IElementType EOL_NE_OP = new EolTokenType("<>");
  IElementType EOL_NOT_OP = new EolTokenType("not");
  IElementType EOL_OPERATION_KEY = new EolTokenType("operation");
  IElementType EOL_ORDSET_KEY = new EolTokenType("OrderedSet");
  IElementType EOL_OR_OP = new EolTokenType("or");
  IElementType EOL_PLUS_ASSIGN_OP = new EolTokenType("+=");
  IElementType EOL_PLUS_OP = new EolTokenType("+");
  IElementType EOL_POINT_OP = new EolTokenType(".");
  IElementType EOL_POINT_POINT_OP = new EolTokenType("..");
  IElementType EOL_POST_COND = new EolTokenType("$post");
  IElementType EOL_PRE_COND = new EolTokenType("$pre");
  IElementType EOL_QUAL_OP = new EolTokenType("::");
  IElementType EOL_RETURN_KEY = new EolTokenType("return");
  IElementType EOL_RIGHT_BRACE = new EolTokenType("}");
  IElementType EOL_RIGHT_PAREN = new EolTokenType(")");
  IElementType EOL_RIGHT_SQ = new EolTokenType("]");
  IElementType EOL_SATISFIES_ALL_BIN = new EolTokenType("satisfiesAll");
  IElementType EOL_SATISFIES_BIN = new EolTokenType("satisfies");
  IElementType EOL_SATISFIES_ONE_BIN = new EolTokenType("satisfiesOne");
  IElementType EOL_SELF_BIN = new EolTokenType("self");
  IElementType EOL_SEMICOLON = new EolTokenType(";");
  IElementType EOL_SEQ_KEY = new EolTokenType("Sequence");
  IElementType EOL_SET_KEY = new EolTokenType("Set");
  IElementType EOL_SPECIAL_ASSIGN_OP = new EolTokenType(":=");
  IElementType EOL_STRING = new EolTokenType("STRING");
  IElementType EOL_SWITCH_KEY = new EolTokenType("switch");
  IElementType EOL_THEN_OP = new EolTokenType("=>");
  IElementType EOL_THROW_KEY = new EolTokenType("throw");
  IElementType EOL_TIMES_ASSIGN_OP = new EolTokenType("*=");
  IElementType EOL_TIMES_OP = new EolTokenType("*");
  IElementType EOL_TRANS_KEY = new EolTokenType("transaction");
  IElementType EOL_TRUE_KEY = new EolTokenType("true");
  IElementType EOL_VAR_KEY = new EolTokenType("var");
  IElementType EOL_WHILE_KEY = new EolTokenType("while");
  IElementType EOL_XOR_OP = new EolTokenType("xor");

  class Factory {
    public static PsiElement createElement(ASTNode node) {
      IElementType type = node.getElementType();
      if (type == EOL_ABORT_STATEMENT) {
        return new EolAbortStatementImpl(node);
      }
      else if (type == EOL_ADD_EXPR) {
        return new EolAddExprImpl(node);
      }
      else if (type == EOL_ANNOTATION_BLOCK) {
        return new EolAnnotationBlockImpl(node);
      }
      else if (type == EOL_ASSIGNMENT_STATEMENT) {
        return new EolAssignmentStatementImpl(node);
      }
      else if (type == EOL_BOOLEAN) {
        return new EolBooleanImpl(node);
      }
      else if (type == EOL_BOOL_EXPR) {
        return new EolBoolExprImpl(node);
      }
      else if (type == EOL_BREAK_ALL_STATEMENT) {
        return new EolBreakAllStatementImpl(node);
      }
      else if (type == EOL_BREAK_STATEMENT) {
        return new EolBreakStatementImpl(node);
      }
      else if (type == EOL_BUILT_IN) {
        return new EolBuiltInImpl(node);
      }
      else if (type == EOL_CASE_STATEMENT) {
        return new EolCaseStatementImpl(node);
      }
      else if (type == EOL_COLLECTION_TYPE) {
        return new EolCollectionTypeImpl(node);
      }
      else if (type == EOL_COMPLEX_FEATURE_CALL) {
        return new EolComplexFeatureCallImpl(node);
      }
      else if (type == EOL_CONTENT) {
        return new EolContentImpl(node);
      }
      else if (type == EOL_CONTINUE_STATEMENT) {
        return new EolContinueStatementImpl(node);
      }
      else if (type == EOL_DEFAULT_STATEMENT) {
        return new EolDefaultStatementImpl(node);
      }
      else if (type == EOL_DELETE_STATEMENT) {
        return new EolDeleteStatementImpl(node);
      }
      else if (type == EOL_ELSE_STATEMENT) {
        return new EolElseStatementImpl(node);
      }
      else if (type == EOL_EQ_EXPR) {
        return new EolEqExprImpl(node);
      }
      else if (type == EOL_EXECUTABLE_ANNOTATION) {
        return new EolExecutableAnnotationImpl(node);
      }
      else if (type == EOL_EXPR) {
        return new EolExprImpl(node);
      }
      else if (type == EOL_EXPRESSION_IN_BRACKETS) {
        return new EolExpressionInBracketsImpl(node);
      }
      else if (type == EOL_EXPRESSION_LIST_OR_RANGE) {
        return new EolExpressionListOrRangeImpl(node);
      }
      else if (type == EOL_EXPRESSION_RANGE) {
        return new EolExpressionRangeImpl(node);
      }
      else if (type == EOL_EXPRESSION_STATEMENT) {
        return new EolExpressionStatementImpl(node);
      }
      else if (type == EOL_EXPR_OR_STATEMENT_BLOCK) {
        return new EolExprOrStatementBlockImpl(node);
      }
      else if (type == EOL_FEATURE_CALL) {
        return new EolFeatureCallImpl(node);
      }
      else if (type == EOL_FORMAL_PARAMETER) {
        return new EolFormalParameterImpl(node);
      }
      else if (type == EOL_FORMAL_PARAMETER_LIST) {
        return new EolFormalParameterListImpl(node);
      }
      else if (type == EOL_FOR_STATEMENT) {
        return new EolForStatementImpl(node);
      }
      else if (type == EOL_ID_LIST) {
        return new EolIdListImpl(node);
      }
      else if (type == EOL_IF_STATEMENT) {
        return new EolIfStatementImpl(node);
      }
      else if (type == EOL_IMPORT_STATEMENT) {
        return new EolImportStatementImpl(node);
      }
      else if (type == EOL_ITEM_SELECTOR_EXPR) {
        return new EolItemSelectorExprImpl(node);
      }
      else if (type == EOL_KEYVAL_EXPRESSION) {
        return new EolKeyvalExpressionImpl(node);
      }
      else if (type == EOL_LAMBDA_EXPRESSION) {
        return new EolLambdaExpressionImpl(node);
      }
      else if (type == EOL_LAMBDA_EXPRESSION_IN_BRACKETS) {
        return new EolLambdaExpressionInBracketsImpl(node);
      }
      else if (type == EOL_LITERAL) {
        return new EolLiteralImpl(node);
      }
      else if (type == EOL_LITERAL_MAP_COLLECTION) {
        return new EolLiteralMapCollectionImpl(node);
      }
      else if (type == EOL_LITERAL_SEQUENTIAL_COLLECTION) {
        return new EolLiteralSequentialCollectionImpl(node);
      }
      else if (type == EOL_MODEL_ALIAS) {
        return new EolModelAliasImpl(node);
      }
      else if (type == EOL_MODEL_DECLARATION) {
        return new EolModelDeclarationImpl(node);
      }
      else if (type == EOL_MODEL_DECLARATION_PARAMETER) {
        return new EolModelDeclarationParameterImpl(node);
      }
      else if (type == EOL_MODEL_DECLARATION_PARAMETERS) {
        return new EolModelDeclarationParametersImpl(node);
      }
      else if (type == EOL_MODEL_DRIVER) {
        return new EolModelDriverImpl(node);
      }
      else if (type == EOL_MODULE) {
        return new EolModuleImpl(node);
      }
      else if (type == EOL_MULT_EXPR) {
        return new EolMultExprImpl(node);
      }
      else if (type == EOL_NATIVE_TYPE) {
        return new EolNativeTypeImpl(node);
      }
      else if (type == EOL_NEW_EXPR) {
        return new EolNewExprImpl(node);
      }
      else if (type == EOL_OPERATION_DECLARATION) {
        return new EolOperationDeclarationImpl(node);
      }
      else if (type == EOL_OPERATION_DECLARATION_OR_ANNOTATION_BLOCK) {
        return new EolOperationDeclarationOrAnnotationBlockImpl(node);
      }
      else if (type == EOL_OPERATION_START) {
        return new EolOperationStartImpl(node);
      }
      else if (type == EOL_PACKAGED_TYPE) {
        return new EolPackagedTypeImpl(node);
      }
      else if (type == EOL_PARAMETER_LIST) {
        return new EolParameterListImpl(node);
      }
      else if (type == EOL_PATH_NAME) {
        return new EolPathNameImpl(node);
      }
      else if (type == EOL_POSTFIX_EXPR) {
        return new EolPostfixExprImpl(node);
      }
      else if (type == EOL_PRIMITIVE_EXPR) {
        return new EolPrimitiveExprImpl(node);
      }
      else if (type == EOL_REL_EXPR) {
        return new EolRelExprImpl(node);
      }
      else if (type == EOL_RETURN_STATEMENT) {
        return new EolReturnStatementImpl(node);
      }
      else if (type == EOL_SHORTCUT_OPERATOR_EXPR) {
        return new EolShortcutOperatorExprImpl(node);
      }
      else if (type == EOL_SIMPLE_ANNOTATION) {
        return new EolSimpleAnnotationImpl(node);
      }
      else if (type == EOL_SIMPLE_FEATURE_CALL) {
        return new EolSimpleFeatureCallImpl(node);
      }
      else if (type == EOL_STATEMENT) {
        return new EolStatementImpl(node);
      }
      else if (type == EOL_STATEMENT_BLOCK) {
        return new EolStatementBlockImpl(node);
      }
      else if (type == EOL_SWITCH_STATEMENT) {
        return new EolSwitchStatementImpl(node);
      }
      else if (type == EOL_THROW_STATEMENT) {
        return new EolThrowStatementImpl(node);
      }
      else if (type == EOL_TRANSACTION_STATEMENT) {
        return new EolTransactionStatementImpl(node);
      }
      else if (type == EOL_TYPE_NAME) {
        return new EolTypeNameImpl(node);
      }
      else if (type == EOL_TYPE_NAME_LIST) {
        return new EolTypeNameListImpl(node);
      }
      else if (type == EOL_VARIABLE_DECLARATION_EXPR) {
        return new EolVariableDeclarationExprImpl(node);
      }
      else if (type == EOL_WHILE_STATEMENT) {
        return new EolWhileStatementImpl(node);
      }
      throw new AssertionError("Unknown element type: " + type);
    }
  }
}

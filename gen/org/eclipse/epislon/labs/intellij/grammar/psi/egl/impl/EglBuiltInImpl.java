/*******************************************************************************
 * Copyright (c) 2008 The University of York.
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *     Horacio Hoyos Rodriguez - initial API and implementation
 ******************************************************************************/
package org.eclipse.epislon.labs.intellij.grammar.psi.egl.impl;

import java.util.List;
import org.jetbrains.annotations.*;
import com.intellij.lang.ASTNode;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiElementVisitor;
import com.intellij.psi.util.PsiTreeUtil;
import static org.eclipse.epislon.labs.intellij.grammar.psi.egl.EglTypes.*;
import com.intellij.extapi.psi.ASTWrapperPsiElement;
import org.eclipse.epislon.labs.intellij.grammar.psi.egl.*;

public class EglBuiltInImpl extends ASTWrapperPsiElement implements EglBuiltIn {

  public EglBuiltInImpl(@NotNull ASTNode node) {
    super(node);
  }

  public void accept(@NotNull EglVisitor visitor) {
    visitor.visitBuiltIn(this);
  }

  public void accept(@NotNull PsiElementVisitor visitor) {
    if (visitor instanceof EglVisitor) accept((EglVisitor)visitor);
    else super.accept(visitor);
  }

  @Override
  @Nullable
  public PsiElement getEquivalentsBin() {
    return findChildByType(EGL_EQUIVALENTS_BIN);
  }

  @Override
  @Nullable
  public PsiElement getEquivalentBin() {
    return findChildByType(EGL_EQUIVALENT_BIN);
  }

  @Override
  @Nullable
  public PsiElement getMatchesBin() {
    return findChildByType(EGL_MATCHES_BIN);
  }

  @Override
  @Nullable
  public PsiElement getSatisfiesAllBin() {
    return findChildByType(EGL_SATISFIES_ALL_BIN);
  }

  @Override
  @Nullable
  public PsiElement getSatisfiesBin() {
    return findChildByType(EGL_SATISFIES_BIN);
  }

  @Override
  @Nullable
  public PsiElement getSatisfiesOneBin() {
    return findChildByType(EGL_SATISFIES_ONE_BIN);
  }

}

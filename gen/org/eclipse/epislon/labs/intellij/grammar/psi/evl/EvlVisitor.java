/*******************************************************************************
 * Copyright (c) 2008 The University of York.
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *     Horacio Hoyos Rodriguez - initial API and implementation
 ******************************************************************************/
package org.eclipse.epislon.labs.intellij.grammar.psi.evl;

import org.jetbrains.annotations.*;
import com.intellij.psi.PsiElementVisitor;
import com.intellij.psi.PsiElement;
import org.eclipse.epislon.labs.intellij.grammar.psi.EpsilonModule;

public class EvlVisitor extends PsiElementVisitor {

  public void visitCheck(@NotNull EvlCheck o) {
    visitPsiElement(o);
  }

  public void visitConstraint(@NotNull EvlConstraint o) {
    visitPsiElement(o);
  }

  public void visitContent(@NotNull EvlContent o) {
    visitPsiElement(o);
  }

  public void visitContext(@NotNull EvlContext o) {
    visitPsiElement(o);
  }

  public void visitContextBlock(@NotNull EvlContextBlock o) {
    visitPsiElement(o);
  }

  public void visitContextContent(@NotNull EvlContextContent o) {
    visitPsiElement(o);
  }

  public void visitCritique(@NotNull EvlCritique o) {
    visitPsiElement(o);
  }

  public void visitFix(@NotNull EvlFix o) {
    visitPsiElement(o);
  }

  public void visitFixBlock(@NotNull EvlFixBlock o) {
    visitPsiElement(o);
  }

  public void visitFixBody(@NotNull EvlFixBody o) {
    visitPsiElement(o);
  }

  public void visitGuard(@NotNull EvlGuard o) {
    visitPsiElement(o);
  }

  public void visitInvariantBlock(@NotNull EvlInvariantBlock o) {
    visitPsiElement(o);
  }

  public void visitMessage(@NotNull EvlMessage o) {
    visitPsiElement(o);
  }

  public void visitModule(@NotNull EvlModule o) {
    visitEpsilonModule(o);
  }

  public void visitPost(@NotNull EvlPost o) {
    visitPsiElement(o);
  }

  public void visitPre(@NotNull EvlPre o) {
    visitPsiElement(o);
  }

  public void visitStatement(@NotNull EvlStatement o) {
    visitPsiElement(o);
  }

  public void visitTitle(@NotNull EvlTitle o) {
    visitPsiElement(o);
  }

  public void visitEpsilonModule(@NotNull EpsilonModule o) {
    visitElement(o);
  }

  public void visitPsiElement(@NotNull PsiElement o) {
    visitElement(o);
  }

}

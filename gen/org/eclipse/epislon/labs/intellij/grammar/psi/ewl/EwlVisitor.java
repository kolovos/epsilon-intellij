/*******************************************************************************
 * Copyright (c) 2008 The University of York.
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *     Horacio Hoyos Rodriguez - initial API and implementation
 ******************************************************************************/
package org.eclipse.epislon.labs.intellij.grammar.psi.ewl;

import org.jetbrains.annotations.*;
import com.intellij.psi.PsiElementVisitor;
import com.intellij.psi.PsiElement;
import org.eclipse.epislon.labs.intellij.grammar.psi.EpsilonModule;

public class EwlVisitor extends PsiElementVisitor {

  public void visitContent(@NotNull EwlContent o) {
    visitPsiElement(o);
  }

  public void visitDoBlock(@NotNull EwlDoBlock o) {
    visitPsiElement(o);
  }

  public void visitGuard(@NotNull EwlGuard o) {
    visitPsiElement(o);
  }

  public void visitModule(@NotNull EwlModule o) {
    visitEpsilonModule(o);
  }

  public void visitPost(@NotNull EwlPost o) {
    visitPsiElement(o);
  }

  public void visitPre(@NotNull EwlPre o) {
    visitPsiElement(o);
  }

  public void visitStatement(@NotNull EwlStatement o) {
    visitPsiElement(o);
  }

  public void visitTitleBlock(@NotNull EwlTitleBlock o) {
    visitPsiElement(o);
  }

  public void visitWizard(@NotNull EwlWizard o) {
    visitPsiElement(o);
  }

  public void visitEpsilonModule(@NotNull EpsilonModule o) {
    visitElement(o);
  }

  public void visitPsiElement(@NotNull PsiElement o) {
    visitElement(o);
  }

}

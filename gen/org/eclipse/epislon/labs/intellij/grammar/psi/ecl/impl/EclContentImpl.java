/*******************************************************************************
 * Copyright (c) 2008 The University of York.
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *     Horacio Hoyos Rodriguez - initial API and implementation
 ******************************************************************************/
package org.eclipse.epislon.labs.intellij.grammar.psi.ecl.impl;

import java.util.List;
import org.jetbrains.annotations.*;
import com.intellij.lang.ASTNode;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiElementVisitor;
import com.intellij.psi.util.PsiTreeUtil;
import static org.eclipse.epislon.labs.intellij.grammar.psi.ecl.EclTypes.*;
import com.intellij.extapi.psi.ASTWrapperPsiElement;
import org.eclipse.epislon.labs.intellij.grammar.psi.ecl.*;
import org.eclipse.epislon.labs.intellij.grammar.parser.ecl.EclParserUtil;
import org.eclipse.epislon.labs.intellij.grammar.psi.eol.EolAnnotationBlock;
import org.eclipse.epislon.labs.intellij.grammar.psi.eol.EolOperationDeclaration;

public class EclContentImpl extends ASTWrapperPsiElement implements EclContent {

  public EclContentImpl(@NotNull ASTNode node) {
    super(node);
  }

  public void accept(@NotNull EclVisitor visitor) {
    visitor.visitContent(this);
  }

  public void accept(@NotNull PsiElementVisitor visitor) {
    if (visitor instanceof EclVisitor) accept((EclVisitor)visitor);
    else super.accept(visitor);
  }

  @Override
  @NotNull
  public List<EclMatchRule> getMatchRuleList() {
    return PsiTreeUtil.getChildrenOfTypeAsList(this, EclMatchRule.class);
  }

  @Override
  @NotNull
  public List<EclPost> getPostList() {
    return PsiTreeUtil.getChildrenOfTypeAsList(this, EclPost.class);
  }

  @Override
  @NotNull
  public List<EclPre> getPreList() {
    return PsiTreeUtil.getChildrenOfTypeAsList(this, EclPre.class);
  }

  @Override
  @NotNull
  public List<EolAnnotationBlock> getAnnotationBlockList() {
    return EclParserUtil.getAnnotationBlockList(this);
  }

  @Override
  @NotNull
  public List<EolOperationDeclaration> getOperationDeclarationList() {
    return EclParserUtil.getOperationDeclarationList(this);
  }

}

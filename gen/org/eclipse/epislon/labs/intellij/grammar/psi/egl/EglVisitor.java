/*******************************************************************************
 * Copyright (c) 2008 The University of York.
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *     Horacio Hoyos Rodriguez - initial API and implementation
 ******************************************************************************/
package org.eclipse.epislon.labs.intellij.grammar.psi.egl;

import org.jetbrains.annotations.*;
import com.intellij.psi.PsiElementVisitor;
import com.intellij.psi.PsiElement;

public class EglVisitor extends PsiElementVisitor {

  public void visitAbortStatement(@NotNull EglAbortStatement o) {
    visitPsiElement(o);
  }

  public void visitAddExpr(@NotNull EglAddExpr o) {
    visitExpr(o);
  }

  public void visitAnnotationBlock(@NotNull EglAnnotationBlock o) {
    visitPsiElement(o);
  }

  public void visitAssignmentStatement(@NotNull EglAssignmentStatement o) {
    visitPsiElement(o);
  }

  public void visitBoolExpr(@NotNull EglBoolExpr o) {
    visitExpr(o);
  }

  public void visitBoolean(@NotNull EglBoolean o) {
    visitPsiElement(o);
  }

  public void visitBreakAllStatement(@NotNull EglBreakAllStatement o) {
    visitPsiElement(o);
  }

  public void visitBreakStatement(@NotNull EglBreakStatement o) {
    visitPsiElement(o);
  }

  public void visitBuiltIn(@NotNull EglBuiltIn o) {
    visitPsiElement(o);
  }

  public void visitCaseStatement(@NotNull EglCaseStatement o) {
    visitPsiElement(o);
  }

  public void visitCollectionType(@NotNull EglCollectionType o) {
    visitPsiElement(o);
  }

  public void visitComplexFeatureCall(@NotNull EglComplexFeatureCall o) {
    visitPsiElement(o);
  }

  public void visitContent(@NotNull EglContent o) {
    visitPsiElement(o);
  }

  public void visitContinueStatement(@NotNull EglContinueStatement o) {
    visitPsiElement(o);
  }

  public void visitDefaultStatement(@NotNull EglDefaultStatement o) {
    visitPsiElement(o);
  }

  public void visitDeleteStatement(@NotNull EglDeleteStatement o) {
    visitPsiElement(o);
  }

  public void visitElseStatement(@NotNull EglElseStatement o) {
    visitPsiElement(o);
  }

  public void visitEqExpr(@NotNull EglEqExpr o) {
    visitExpr(o);
  }

  public void visitExecutableAnnotation(@NotNull EglExecutableAnnotation o) {
    visitPsiElement(o);
  }

  public void visitExpr(@NotNull EglExpr o) {
    visitPsiElement(o);
  }

  public void visitExpressionInBrackets(@NotNull EglExpressionInBrackets o) {
    visitPsiElement(o);
  }

  public void visitExpressionListOrRange(@NotNull EglExpressionListOrRange o) {
    visitPsiElement(o);
  }

  public void visitExpressionRange(@NotNull EglExpressionRange o) {
    visitPsiElement(o);
  }

  public void visitExpressionStatement(@NotNull EglExpressionStatement o) {
    visitPsiElement(o);
  }

  public void visitFeatureCall(@NotNull EglFeatureCall o) {
    visitPsiElement(o);
  }

  public void visitForStatement(@NotNull EglForStatement o) {
    visitPsiElement(o);
  }

  public void visitFormalParameter(@NotNull EglFormalParameter o) {
    visitPsiElement(o);
  }

  public void visitFormalParameterList(@NotNull EglFormalParameterList o) {
    visitPsiElement(o);
  }

  public void visitIdList(@NotNull EglIdList o) {
    visitPsiElement(o);
  }

  public void visitIfStatement(@NotNull EglIfStatement o) {
    visitPsiElement(o);
  }

  public void visitImportStatement(@NotNull EglImportStatement o) {
    visitPsiElement(o);
  }

  public void visitItemSelectorExpr(@NotNull EglItemSelectorExpr o) {
    visitExpr(o);
  }

  public void visitKeyvalExpression(@NotNull EglKeyvalExpression o) {
    visitPsiElement(o);
  }

  public void visitLambdaExpression(@NotNull EglLambdaExpression o) {
    visitPsiElement(o);
  }

  public void visitLambdaExpressionInBrackets(@NotNull EglLambdaExpressionInBrackets o) {
    visitPsiElement(o);
  }

  public void visitLiteral(@NotNull EglLiteral o) {
    visitPsiElement(o);
  }

  public void visitLiteralMapCollection(@NotNull EglLiteralMapCollection o) {
    visitPsiElement(o);
  }

  public void visitLiteralSequentialCollection(@NotNull EglLiteralSequentialCollection o) {
    visitPsiElement(o);
  }

  public void visitModelAlias(@NotNull EglModelAlias o) {
    visitPsiElement(o);
  }

  public void visitModelDeclaration(@NotNull EglModelDeclaration o) {
    visitPsiElement(o);
  }

  public void visitModelDeclarationParameter(@NotNull EglModelDeclarationParameter o) {
    visitPsiElement(o);
  }

  public void visitModelDeclarationParameters(@NotNull EglModelDeclarationParameters o) {
    visitPsiElement(o);
  }

  public void visitModelDriver(@NotNull EglModelDriver o) {
    visitPsiElement(o);
  }

  public void visitModule(@NotNull EglModule o) {
    visitPsiElement(o);
  }

  public void visitMultExpr(@NotNull EglMultExpr o) {
    visitExpr(o);
  }

  public void visitNativeType(@NotNull EglNativeType o) {
    visitPsiElement(o);
  }

  public void visitNewExpr(@NotNull EglNewExpr o) {
    visitExpr(o);
  }

  public void visitOperationDeclaration(@NotNull EglOperationDeclaration o) {
    visitPsiElement(o);
  }

  public void visitOperationDeclarationOrAnnotationBlock(@NotNull EglOperationDeclarationOrAnnotationBlock o) {
    visitPsiElement(o);
  }

  public void visitOperationStart(@NotNull EglOperationStart o) {
    visitPsiElement(o);
  }

  public void visitPackagedType(@NotNull EglPackagedType o) {
    visitPsiElement(o);
  }

  public void visitParameterList(@NotNull EglParameterList o) {
    visitPsiElement(o);
  }

  public void visitPathName(@NotNull EglPathName o) {
    visitPsiElement(o);
  }

  public void visitPostfixExpr(@NotNull EglPostfixExpr o) {
    visitExpr(o);
  }

  public void visitPrimitiveExpr(@NotNull EglPrimitiveExpr o) {
    visitExpr(o);
  }

  public void visitPrint(@NotNull EglPrint o) {
    visitPsiElement(o);
  }

  public void visitRelExpr(@NotNull EglRelExpr o) {
    visitExpr(o);
  }

  public void visitReturnStatement(@NotNull EglReturnStatement o) {
    visitPsiElement(o);
  }

  public void visitShortcutOperatorExpr(@NotNull EglShortcutOperatorExpr o) {
    visitExpr(o);
  }

  public void visitSimpleAnnotation(@NotNull EglSimpleAnnotation o) {
    visitPsiElement(o);
  }

  public void visitSimpleFeatureCall(@NotNull EglSimpleFeatureCall o) {
    visitPsiElement(o);
  }

  public void visitStatement(@NotNull EglStatement o) {
    visitPsiElement(o);
  }

  public void visitStatementBlock(@NotNull EglStatementBlock o) {
    visitPsiElement(o);
  }

  public void visitSwitchStatement(@NotNull EglSwitchStatement o) {
    visitPsiElement(o);
  }

  public void visitThrowStatement(@NotNull EglThrowStatement o) {
    visitPsiElement(o);
  }

  public void visitTransactionStatement(@NotNull EglTransactionStatement o) {
    visitPsiElement(o);
  }

  public void visitTypeName(@NotNull EglTypeName o) {
    visitPsiElement(o);
  }

  public void visitTypeNameList(@NotNull EglTypeNameList o) {
    visitPsiElement(o);
  }

  public void visitVariableDeclarationExpr(@NotNull EglVariableDeclarationExpr o) {
    visitExpr(o);
  }

  public void visitWhileStatement(@NotNull EglWhileStatement o) {
    visitPsiElement(o);
  }

  public void visitPsiElement(@NotNull PsiElement o) {
    visitElement(o);
  }

}

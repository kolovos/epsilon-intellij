/*******************************************************************************
 * Copyright (c) 2008 The University of York.
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *     Horacio Hoyos Rodriguez - initial API and implementation
 ******************************************************************************/
package org.eclipse.epislon.labs.intellij.grammar.psi.ewl;

import com.intellij.psi.tree.IElementType;
import com.intellij.psi.PsiElement;
import com.intellij.lang.ASTNode;
import org.eclipse.epislon.labs.intellij.grammar.psi.ewl.impl.*;

public interface EwlTypes {

  IElementType EWL_CONTENT = new EwlElementType("EWL_CONTENT");
  IElementType EWL_DO_BLOCK = new EwlElementType("EWL_DO_BLOCK");
  IElementType EWL_GUARD = new EwlElementType("EWL_GUARD");
  IElementType EWL_MODULE = new EwlElementType("EWL_MODULE");
  IElementType EWL_POST = new EwlElementType("EWL_POST");
  IElementType EWL_PRE = new EwlElementType("EWL_PRE");
  IElementType EWL_STATEMENT = new EwlElementType("EWL_STATEMENT");
  IElementType EWL_TITLE_BLOCK = new EwlElementType("EWL_TITLE_BLOCK");
  IElementType EWL_WIZARD = new EwlElementType("EWL_WIZARD");

  IElementType EWL_DO_KEY = new EwlTokenType("do");
  IElementType EWL_TITLE_KEY = new EwlTokenType("title");
  IElementType EWL_WIZARD_KEY = new EwlTokenType("wizard");

  class Factory {
    public static PsiElement createElement(ASTNode node) {
      IElementType type = node.getElementType();
      if (type == EWL_CONTENT) {
        return new EwlContentImpl(node);
      }
      else if (type == EWL_DO_BLOCK) {
        return new EwlDoBlockImpl(node);
      }
      else if (type == EWL_GUARD) {
        return new EwlGuardImpl(node);
      }
      else if (type == EWL_MODULE) {
        return new EwlModuleImpl(node);
      }
      else if (type == EWL_POST) {
        return new EwlPostImpl(node);
      }
      else if (type == EWL_PRE) {
        return new EwlPreImpl(node);
      }
      else if (type == EWL_STATEMENT) {
        return new EwlStatementImpl(node);
      }
      else if (type == EWL_TITLE_BLOCK) {
        return new EwlTitleBlockImpl(node);
      }
      else if (type == EWL_WIZARD) {
        return new EwlWizardImpl(node);
      }
      throw new AssertionError("Unknown element type: " + type);
    }
  }
}

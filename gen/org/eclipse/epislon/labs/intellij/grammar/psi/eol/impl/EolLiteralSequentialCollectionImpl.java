/*******************************************************************************
 * Copyright (c) 2008 The University of York.
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *     Horacio Hoyos Rodriguez - initial API and implementation
 ******************************************************************************/
package org.eclipse.epislon.labs.intellij.grammar.psi.eol.impl;

import java.util.List;
import org.jetbrains.annotations.*;
import com.intellij.lang.ASTNode;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiElementVisitor;
import com.intellij.psi.util.PsiTreeUtil;
import static org.eclipse.epislon.labs.intellij.grammar.psi.eol.EolTypes.*;
import com.intellij.extapi.psi.ASTWrapperPsiElement;
import org.eclipse.epislon.labs.intellij.grammar.psi.eol.*;

public class EolLiteralSequentialCollectionImpl extends ASTWrapperPsiElement implements EolLiteralSequentialCollection {

  public EolLiteralSequentialCollectionImpl(@NotNull ASTNode node) {
    super(node);
  }

  public void accept(@NotNull EolVisitor visitor) {
    visitor.visitLiteralSequentialCollection(this);
  }

  public void accept(@NotNull PsiElementVisitor visitor) {
    if (visitor instanceof EolVisitor) accept((EolVisitor)visitor);
    else super.accept(visitor);
  }

  @Override
  @Nullable
  public EolExpressionListOrRange getExpressionListOrRange() {
    return findChildByClass(EolExpressionListOrRange.class);
  }

  @Override
  @Nullable
  public PsiElement getBagKey() {
    return findChildByType(EOL_BAG_KEY);
  }

  @Override
  @Nullable
  public PsiElement getColKey() {
    return findChildByType(EOL_COL_KEY);
  }

  @Override
  @NotNull
  public PsiElement getLeftBrace() {
    return findNotNullChildByType(EOL_LEFT_BRACE);
  }

  @Override
  @Nullable
  public PsiElement getListKey() {
    return findChildByType(EOL_LIST_KEY);
  }

  @Override
  @Nullable
  public PsiElement getOrdsetKey() {
    return findChildByType(EOL_ORDSET_KEY);
  }

  @Override
  @NotNull
  public PsiElement getRightBrace() {
    return findNotNullChildByType(EOL_RIGHT_BRACE);
  }

  @Override
  @Nullable
  public PsiElement getSeqKey() {
    return findChildByType(EOL_SEQ_KEY);
  }

  @Override
  @Nullable
  public PsiElement getSetKey() {
    return findChildByType(EOL_SET_KEY);
  }

}

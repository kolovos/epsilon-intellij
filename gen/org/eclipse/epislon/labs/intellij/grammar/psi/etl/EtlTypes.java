/*******************************************************************************
 * Copyright (c) 2008 The University of York.
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *     Horacio Hoyos Rodriguez - initial API and implementation
 ******************************************************************************/
package org.eclipse.epislon.labs.intellij.grammar.psi.etl;

import com.intellij.psi.tree.IElementType;
import com.intellij.psi.PsiElement;
import com.intellij.lang.ASTNode;
import org.eclipse.epislon.labs.intellij.grammar.psi.etl.impl.*;

public interface EtlTypes {

  IElementType ETL_CONTENT = new EtlElementType("ETL_CONTENT");
  IElementType ETL_EXTENDS = new EtlElementType("ETL_EXTENDS");
  IElementType ETL_FORMAL_PARAMETER = new EtlElementType("ETL_FORMAL_PARAMETER");
  IElementType ETL_FORMAL_PARAMETER_LIST = new EtlElementType("ETL_FORMAL_PARAMETER_LIST");
  IElementType ETL_GUARD = new EtlElementType("ETL_GUARD");
  IElementType ETL_MODULE = new EtlElementType("ETL_MODULE");
  IElementType ETL_POST = new EtlElementType("ETL_POST");
  IElementType ETL_PRE = new EtlElementType("ETL_PRE");
  IElementType ETL_STATEMENT = new EtlElementType("ETL_STATEMENT");
  IElementType ETL_TRANSFORMATION_RULE = new EtlElementType("ETL_TRANSFORMATION_RULE");

  IElementType ETL_EQUIVS_KEY = new EtlTokenType("equivalents");
  IElementType ETL_EQUIV_KEY = new EtlTokenType("equivalent");
  IElementType ETL_RULE_KEY = new EtlTokenType("rule");
  IElementType ETL_TO_KEY = new EtlTokenType("to");
  IElementType ETL_TRANSFORM_KEY = new EtlTokenType("transform");

  class Factory {
    public static PsiElement createElement(ASTNode node) {
      IElementType type = node.getElementType();
      if (type == ETL_CONTENT) {
        return new EtlContentImpl(node);
      }
      else if (type == ETL_EXTENDS) {
        return new EtlExtendsImpl(node);
      }
      else if (type == ETL_FORMAL_PARAMETER) {
        return new EtlFormalParameterImpl(node);
      }
      else if (type == ETL_FORMAL_PARAMETER_LIST) {
        return new EtlFormalParameterListImpl(node);
      }
      else if (type == ETL_GUARD) {
        return new EtlGuardImpl(node);
      }
      else if (type == ETL_MODULE) {
        return new EtlModuleImpl(node);
      }
      else if (type == ETL_POST) {
        return new EtlPostImpl(node);
      }
      else if (type == ETL_PRE) {
        return new EtlPreImpl(node);
      }
      else if (type == ETL_STATEMENT) {
        return new EtlStatementImpl(node);
      }
      else if (type == ETL_TRANSFORMATION_RULE) {
        return new EtlTransformationRuleImpl(node);
      }
      throw new AssertionError("Unknown element type: " + type);
    }
  }
}

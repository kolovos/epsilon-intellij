/*******************************************************************************
 * Copyright (c) 2008 The University of York.
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *     Horacio Hoyos Rodriguez - initial API and implementation
 ******************************************************************************/
package org.eclipse.epislon.labs.intellij.grammar.psi.erl;

import org.jetbrains.annotations.*;
import com.intellij.psi.PsiElementVisitor;
import com.intellij.psi.PsiElement;
import org.eclipse.epislon.labs.intellij.grammar.psi.eol.EolStatement;
import org.eclipse.epislon.labs.intellij.grammar.psi.EpsilonModule;

public class ErlVisitor extends PsiElementVisitor {

  public void visitContent(@NotNull ErlContent o) {
    visitPsiElement(o);
  }

  public void visitExtends(@NotNull ErlExtends o) {
    visitPsiElement(o);
  }

  public void visitGuard(@NotNull ErlGuard o) {
    visitPsiElement(o);
  }

  public void visitModule(@NotNull ErlModule o) {
    visitEpsilonModule(o);
  }

  public void visitPost(@NotNull ErlPost o) {
    visitPsiElement(o);
  }

  public void visitPre(@NotNull ErlPre o) {
    visitPsiElement(o);
  }

  public void visitStatement(@NotNull ErlStatement o) {
    visitEolStatement(o);
  }

  public void visitEolStatement(@NotNull EolStatement o) {
    visitElement(o);
  }

  public void visitEpsilonModule(@NotNull EpsilonModule o) {
    visitElement(o);
  }

  public void visitPsiElement(@NotNull PsiElement o) {
    visitElement(o);
  }

}

/*******************************************************************************
 * Copyright (c) 2008 The University of York.
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *     Horacio Hoyos Rodriguez - initial API and implementation
 ******************************************************************************/
package org.eclipse.epislon.labs.intellij.grammar.psi.egl.impl;

import java.util.List;
import org.jetbrains.annotations.*;
import com.intellij.lang.ASTNode;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiElementVisitor;
import com.intellij.psi.util.PsiTreeUtil;
import static org.eclipse.epislon.labs.intellij.grammar.psi.egl.EglTypes.*;
import org.eclipse.epislon.labs.intellij.grammar.psi.egl.*;

public class EglBoolExprImpl extends EglExprImpl implements EglBoolExpr {

  public EglBoolExprImpl(@NotNull ASTNode node) {
    super(node);
  }

  public void accept(@NotNull EglVisitor visitor) {
    visitor.visitBoolExpr(this);
  }

  public void accept(@NotNull PsiElementVisitor visitor) {
    if (visitor instanceof EglVisitor) accept((EglVisitor)visitor);
    else super.accept(visitor);
  }

  @Override
  @NotNull
  public List<EglExpr> getExprList() {
    return PsiTreeUtil.getChildrenOfTypeAsList(this, EglExpr.class);
  }

  @Override
  @Nullable
  public PsiElement getAndOp() {
    return findChildByType(EGL_AND_OP);
  }

  @Override
  @Nullable
  public PsiElement getImpliesOp() {
    return findChildByType(EGL_IMPLIES_OP);
  }

  @Override
  @Nullable
  public PsiElement getOrOp() {
    return findChildByType(EGL_OR_OP);
  }

  @Override
  @Nullable
  public PsiElement getXorOp() {
    return findChildByType(EGL_XOR_OP);
  }

}

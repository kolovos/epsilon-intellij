/*******************************************************************************
 * Copyright (c) 2008 The University of York.
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *     Horacio Hoyos Rodriguez - initial API and implementation
 ******************************************************************************/
package org.eclipse.epislon.labs.intellij.grammar.psi.eol.impl;

import java.util.List;
import org.jetbrains.annotations.*;
import com.intellij.lang.ASTNode;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiElementVisitor;
import com.intellij.psi.util.PsiTreeUtil;
import static org.eclipse.epislon.labs.intellij.grammar.psi.eol.EolTypes.*;
import org.eclipse.epislon.labs.intellij.grammar.psi.eol.*;

public class EolPrimitiveExprImpl extends EolExprImpl implements EolPrimitiveExpr {

  public EolPrimitiveExprImpl(@NotNull ASTNode node) {
    super(node);
  }

  public void accept(@NotNull EolVisitor visitor) {
    visitor.visitPrimitiveExpr(this);
  }

  public void accept(@NotNull PsiElementVisitor visitor) {
    if (visitor instanceof EolVisitor) accept((EolVisitor)visitor);
    else super.accept(visitor);
  }

  @Override
  @Nullable
  public EolCollectionType getCollectionType() {
    return findChildByClass(EolCollectionType.class);
  }

  @Override
  @Nullable
  public EolExpressionInBrackets getExpressionInBrackets() {
    return findChildByClass(EolExpressionInBrackets.class);
  }

  @Override
  @Nullable
  public EolFeatureCall getFeatureCall() {
    return findChildByClass(EolFeatureCall.class);
  }

  @Override
  @Nullable
  public EolLiteral getLiteral() {
    return findChildByClass(EolLiteral.class);
  }

  @Override
  @Nullable
  public EolLiteralMapCollection getLiteralMapCollection() {
    return findChildByClass(EolLiteralMapCollection.class);
  }

  @Override
  @Nullable
  public EolLiteralSequentialCollection getLiteralSequentialCollection() {
    return findChildByClass(EolLiteralSequentialCollection.class);
  }

  @Override
  @Nullable
  public EolNativeType getNativeType() {
    return findChildByClass(EolNativeType.class);
  }

  @Override
  @Nullable
  public EolPathName getPathName() {
    return findChildByClass(EolPathName.class);
  }

}

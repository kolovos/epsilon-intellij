/*******************************************************************************
 * Copyright (c) 2008 The University of York.
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *     Horacio Hoyos Rodriguez - initial API and implementation
 ******************************************************************************/
package org.eclipse.epislon.labs.intellij.grammar.psi.ewl.impl;

import java.util.List;
import org.jetbrains.annotations.*;
import com.intellij.lang.ASTNode;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiElementVisitor;
import com.intellij.psi.util.PsiTreeUtil;
import static org.eclipse.epislon.labs.intellij.grammar.psi.ewl.EwlTypes.*;
import com.intellij.extapi.psi.ASTWrapperPsiElement;
import org.eclipse.epislon.labs.intellij.grammar.psi.ewl.*;
import org.eclipse.epislon.labs.intellij.grammar.parser.ewl.EwlParserUtil;

public class EwlWizardImpl extends ASTWrapperPsiElement implements EwlWizard {

  public EwlWizardImpl(@NotNull ASTNode node) {
    super(node);
  }

  public void accept(@NotNull EwlVisitor visitor) {
    visitor.visitWizard(this);
  }

  public void accept(@NotNull PsiElementVisitor visitor) {
    if (visitor instanceof EwlVisitor) accept((EwlVisitor)visitor);
    else super.accept(visitor);
  }

  @Override
  @Nullable
  public EwlDoBlock getDoBlock() {
    return findChildByClass(EwlDoBlock.class);
  }

  @Override
  @Nullable
  public EwlGuard getGuard() {
    return findChildByClass(EwlGuard.class);
  }

  @Override
  @Nullable
  public EwlTitleBlock getTitleBlock() {
    return findChildByClass(EwlTitleBlock.class);
  }

  @Override
  @NotNull
  public PsiElement getWizardKey() {
    return findNotNullChildByType(EWL_WIZARD_KEY);
  }

  @Override
  @NotNull
  public PsiElement getLeftBrace() {
    return EwlParserUtil.getLeftBrace(this);
  }

}

/*******************************************************************************
 * Copyright (c) 2008 The University of York.
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *     Horacio Hoyos Rodriguez - initial API and implementation
 ******************************************************************************/
package org.eclipse.epislon.labs.intellij.grammar.psi.eol;

import java.util.List;
import org.jetbrains.annotations.*;
import com.intellij.psi.PsiElement;

public interface EolStatement extends PsiElement {

  @Nullable
  EolAbortStatement getAbortStatement();

  @Nullable
  EolAssignmentStatement getAssignmentStatement();

  @Nullable
  EolBreakAllStatement getBreakAllStatement();

  @Nullable
  EolBreakStatement getBreakStatement();

  @Nullable
  EolContinueStatement getContinueStatement();

  @Nullable
  EolDeleteStatement getDeleteStatement();

  @Nullable
  EolExpressionStatement getExpressionStatement();

  @Nullable
  EolForStatement getForStatement();

  @Nullable
  EolIfStatement getIfStatement();

  @Nullable
  EolReturnStatement getReturnStatement();

  @Nullable
  EolSwitchStatement getSwitchStatement();

  @Nullable
  EolThrowStatement getThrowStatement();

  @Nullable
  EolTransactionStatement getTransactionStatement();

  @Nullable
  EolWhileStatement getWhileStatement();

}

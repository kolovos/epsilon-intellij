/*******************************************************************************
 * Copyright (c) 2008 The University of York.
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *     Horacio Hoyos Rodriguez - initial API and implementation
 ******************************************************************************/
package org.eclipse.epislon.labs.intellij.grammar.psi.egl;

import java.util.List;
import org.jetbrains.annotations.*;
import com.intellij.psi.PsiElement;

public interface EglAssignmentStatement extends PsiElement {

  @NotNull
  List<EglExpr> getExprList();

  @Nullable
  EglPrint getPrint();

  @Nullable
  PsiElement getDivAssigOp();

  @Nullable
  PsiElement getEquivAssignOp();

  @Nullable
  PsiElement getMinusAssignOp();

  @Nullable
  PsiElement getPlusAssignOp();

  @Nullable
  PsiElement getSemicolon();

  @Nullable
  PsiElement getSpecialAssignOp();

  @Nullable
  PsiElement getTimesAssignOp();

}

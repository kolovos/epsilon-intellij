/*******************************************************************************
 * Copyright (c) 2008 The University of York.
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *     Horacio Hoyos Rodriguez - initial API and implementation
 ******************************************************************************/
package org.eclipse.epislon.labs.intellij.grammar.psi.egl;

import com.intellij.psi.tree.IElementType;
import com.intellij.psi.PsiElement;
import com.intellij.lang.ASTNode;
import org.eclipse.epislon.labs.intellij.grammar.psi.egl.impl.*;

public interface EglTypes {

  IElementType EGL_ABORT_STATEMENT = new EglElementType("EGL_ABORT_STATEMENT");
  IElementType EGL_ADD_EXPR = new EglElementType("EGL_ADD_EXPR");
  IElementType EGL_ANNOTATION_BLOCK = new EglElementType("EGL_ANNOTATION_BLOCK");
  IElementType EGL_ASSIGNMENT_STATEMENT = new EglElementType("EGL_ASSIGNMENT_STATEMENT");
  IElementType EGL_BOOLEAN = new EglElementType("EGL_BOOLEAN");
  IElementType EGL_BOOL_EXPR = new EglElementType("EGL_BOOL_EXPR");
  IElementType EGL_BREAK_ALL_STATEMENT = new EglElementType("EGL_BREAK_ALL_STATEMENT");
  IElementType EGL_BREAK_STATEMENT = new EglElementType("EGL_BREAK_STATEMENT");
  IElementType EGL_BUILT_IN = new EglElementType("EGL_BUILT_IN");
  IElementType EGL_CASE_STATEMENT = new EglElementType("EGL_CASE_STATEMENT");
  IElementType EGL_COLLECTION_TYPE = new EglElementType("EGL_COLLECTION_TYPE");
  IElementType EGL_COMPLEX_FEATURE_CALL = new EglElementType("EGL_COMPLEX_FEATURE_CALL");
  IElementType EGL_CONTENT = new EglElementType("EGL_CONTENT");
  IElementType EGL_CONTINUE_STATEMENT = new EglElementType("EGL_CONTINUE_STATEMENT");
  IElementType EGL_DEFAULT_STATEMENT = new EglElementType("EGL_DEFAULT_STATEMENT");
  IElementType EGL_DELETE_STATEMENT = new EglElementType("EGL_DELETE_STATEMENT");
  IElementType EGL_ELSE_STATEMENT = new EglElementType("EGL_ELSE_STATEMENT");
  IElementType EGL_EQ_EXPR = new EglElementType("EGL_EQ_EXPR");
  IElementType EGL_EXECUTABLE_ANNOTATION = new EglElementType("EGL_EXECUTABLE_ANNOTATION");
  IElementType EGL_EXPR = new EglElementType("EGL_EXPR");
  IElementType EGL_EXPRESSION_IN_BRACKETS = new EglElementType("EGL_EXPRESSION_IN_BRACKETS");
  IElementType EGL_EXPRESSION_LIST_OR_RANGE = new EglElementType("EGL_EXPRESSION_LIST_OR_RANGE");
  IElementType EGL_EXPRESSION_RANGE = new EglElementType("EGL_EXPRESSION_RANGE");
  IElementType EGL_EXPRESSION_STATEMENT = new EglElementType("EGL_EXPRESSION_STATEMENT");
  IElementType EGL_FEATURE_CALL = new EglElementType("EGL_FEATURE_CALL");
  IElementType EGL_FORMAL_PARAMETER = new EglElementType("EGL_FORMAL_PARAMETER");
  IElementType EGL_FORMAL_PARAMETER_LIST = new EglElementType("EGL_FORMAL_PARAMETER_LIST");
  IElementType EGL_FOR_STATEMENT = new EglElementType("EGL_FOR_STATEMENT");
  IElementType EGL_ID_LIST = new EglElementType("EGL_ID_LIST");
  IElementType EGL_IF_STATEMENT = new EglElementType("EGL_IF_STATEMENT");
  IElementType EGL_IMPORT_STATEMENT = new EglElementType("EGL_IMPORT_STATEMENT");
  IElementType EGL_ITEM_SELECTOR_EXPR = new EglElementType("EGL_ITEM_SELECTOR_EXPR");
  IElementType EGL_KEYVAL_EXPRESSION = new EglElementType("EGL_KEYVAL_EXPRESSION");
  IElementType EGL_LAMBDA_EXPRESSION = new EglElementType("EGL_LAMBDA_EXPRESSION");
  IElementType EGL_LAMBDA_EXPRESSION_IN_BRACKETS = new EglElementType("EGL_LAMBDA_EXPRESSION_IN_BRACKETS");
  IElementType EGL_LITERAL = new EglElementType("EGL_LITERAL");
  IElementType EGL_LITERAL_MAP_COLLECTION = new EglElementType("EGL_LITERAL_MAP_COLLECTION");
  IElementType EGL_LITERAL_SEQUENTIAL_COLLECTION = new EglElementType("EGL_LITERAL_SEQUENTIAL_COLLECTION");
  IElementType EGL_MODEL_ALIAS = new EglElementType("EGL_MODEL_ALIAS");
  IElementType EGL_MODEL_DECLARATION = new EglElementType("EGL_MODEL_DECLARATION");
  IElementType EGL_MODEL_DECLARATION_PARAMETER = new EglElementType("EGL_MODEL_DECLARATION_PARAMETER");
  IElementType EGL_MODEL_DECLARATION_PARAMETERS = new EglElementType("EGL_MODEL_DECLARATION_PARAMETERS");
  IElementType EGL_MODEL_DRIVER = new EglElementType("EGL_MODEL_DRIVER");
  IElementType EGL_MODULE = new EglElementType("EGL_MODULE");
  IElementType EGL_MULT_EXPR = new EglElementType("EGL_MULT_EXPR");
  IElementType EGL_NATIVE_TYPE = new EglElementType("EGL_NATIVE_TYPE");
  IElementType EGL_NEW_EXPR = new EglElementType("EGL_NEW_EXPR");
  IElementType EGL_OPERATION_DECLARATION = new EglElementType("EGL_OPERATION_DECLARATION");
  IElementType EGL_OPERATION_DECLARATION_OR_ANNOTATION_BLOCK = new EglElementType("EGL_OPERATION_DECLARATION_OR_ANNOTATION_BLOCK");
  IElementType EGL_OPERATION_START = new EglElementType("EGL_OPERATION_START");
  IElementType EGL_PACKAGED_TYPE = new EglElementType("EGL_PACKAGED_TYPE");
  IElementType EGL_PARAMETER_LIST = new EglElementType("EGL_PARAMETER_LIST");
  IElementType EGL_PATH_NAME = new EglElementType("EGL_PATH_NAME");
  IElementType EGL_POSTFIX_EXPR = new EglElementType("EGL_POSTFIX_EXPR");
  IElementType EGL_PRIMITIVE_EXPR = new EglElementType("EGL_PRIMITIVE_EXPR");
  IElementType EGL_PRINT = new EglElementType("EGL_PRINT");
  IElementType EGL_REL_EXPR = new EglElementType("EGL_REL_EXPR");
  IElementType EGL_RETURN_STATEMENT = new EglElementType("EGL_RETURN_STATEMENT");
  IElementType EGL_SHORTCUT_OPERATOR_EXPR = new EglElementType("EGL_SHORTCUT_OPERATOR_EXPR");
  IElementType EGL_SIMPLE_ANNOTATION = new EglElementType("EGL_SIMPLE_ANNOTATION");
  IElementType EGL_SIMPLE_FEATURE_CALL = new EglElementType("EGL_SIMPLE_FEATURE_CALL");
  IElementType EGL_STATEMENT = new EglElementType("EGL_STATEMENT");
  IElementType EGL_STATEMENT_BLOCK = new EglElementType("EGL_STATEMENT_BLOCK");
  IElementType EGL_SWITCH_STATEMENT = new EglElementType("EGL_SWITCH_STATEMENT");
  IElementType EGL_THROW_STATEMENT = new EglElementType("EGL_THROW_STATEMENT");
  IElementType EGL_TRANSACTION_STATEMENT = new EglElementType("EGL_TRANSACTION_STATEMENT");
  IElementType EGL_TYPE_NAME = new EglElementType("EGL_TYPE_NAME");
  IElementType EGL_TYPE_NAME_LIST = new EglElementType("EGL_TYPE_NAME_LIST");
  IElementType EGL_VARIABLE_DECLARATION_EXPR = new EglElementType("EGL_VARIABLE_DECLARATION_EXPR");
  IElementType EGL_WHILE_STATEMENT = new EglElementType("EGL_WHILE_STATEMENT");

  IElementType EGL_ABORT_KEY = new EglTokenType("abort");
  IElementType EGL_AND_OP = new EglTokenType("and");
  IElementType EGL_ANNOTATION = new EglTokenType("ANNOTATION");
  IElementType EGL_ARROW_OP = new EglTokenType("->");
  IElementType EGL_ASSIGN_OP = new EglTokenType("=");
  IElementType EGL_BAG_KEY = new EglTokenType("Bag");
  IElementType EGL_BLOCK_COMMENT = new EglTokenType("BLOCK_COMMENT");
  IElementType EGL_BREAK_ALL_KEY = new EglTokenType("breakAll");
  IElementType EGL_BREAK_KEY = new EglTokenType("break");
  IElementType EGL_CASE_KEY = new EglTokenType("case");
  IElementType EGL_CODE_LIM = new EglTokenType("%");
  IElementType EGL_COLON_OP = new EglTokenType(":");
  IElementType EGL_COL_KEY = new EglTokenType("Collection");
  IElementType EGL_COMMA = new EglTokenType(",");
  IElementType EGL_CONTINUE_KEY = new EglTokenType("continue");
  IElementType EGL_DEFAULT_KEY = new EglTokenType("default");
  IElementType EGL_DELETE_KEY = new EglTokenType("delete");
  IElementType EGL_DIV_ASSIG_OP = new EglTokenType("/=");
  IElementType EGL_DIV_OP = new EglTokenType("/");
  IElementType EGL_ELSE_KEY = new EglTokenType("else");
  IElementType EGL_ENUM_OP = new EglTokenType("#");
  IElementType EGL_EQUIVALENTS_BIN = new EglTokenType("equivalents");
  IElementType EGL_EQUIVALENT_BIN = new EglTokenType("equivalent");
  IElementType EGL_EQUIV_ASSIGN_OP = new EglTokenType("::=");
  IElementType EGL_EQ_OP = new EglTokenType("==");
  IElementType EGL_EXEC_ANNOT = new EglTokenType("$");
  IElementType EGL_EXTENDS_KEY = new EglTokenType("extends");
  IElementType EGL_EXT_KEY = new EglTokenType("ext");
  IElementType EGL_FALSE_KEY = new EglTokenType("false");
  IElementType EGL_FLOAT = new EglTokenType("FLOAT");
  IElementType EGL_FOR_KEY = new EglTokenType("for");
  IElementType EGL_FUNCTION_KEY = new EglTokenType("function");
  IElementType EGL_GE_OP = new EglTokenType(">=");
  IElementType EGL_GT_OP = new EglTokenType(">");
  IElementType EGL_GUARD_KEY = new EglTokenType("guard");
  IElementType EGL_HAS_MORE_BIN = new EglTokenType("hasMore");
  IElementType EGL_ID = new EglTokenType("ID");
  IElementType EGL_IF_KEY = new EglTokenType("if");
  IElementType EGL_IMPLIES_OP = new EglTokenType("implies");
  IElementType EGL_IMPORT_KEY = new EglTokenType("import");
  IElementType EGL_INC_OP = new EglTokenType("++");
  IElementType EGL_IN_KEY = new EglTokenType("in");
  IElementType EGL_IN_OP = new EglTokenType("|");
  IElementType EGL_LEFT_BRACE = new EglTokenType("{");
  IElementType EGL_LEFT_PAREN = new EglTokenType("(");
  IElementType EGL_LEFT_SQ = new EglTokenType("[");
  IElementType EGL_LE_OP = new EglTokenType("<=");
  IElementType EGL_LINE_COMMENT = new EglTokenType("LINE_COMMENT");
  IElementType EGL_LIST_KEY = new EglTokenType("List");
  IElementType EGL_LOOP_CNT_BIN = new EglTokenType("loopCount");
  IElementType EGL_LT_OP = new EglTokenType("<");
  IElementType EGL_MAP_KEY = new EglTokenType("Map");
  IElementType EGL_MATCHES_BIN = new EglTokenType("matches");
  IElementType EGL_MINUS_ASSIGN_OP = new EglTokenType("-=");
  IElementType EGL_MODEL_OP = new EglTokenType("!");
  IElementType EGL_NATIVE_KEY = new EglTokenType("Native");
  IElementType EGL_NEG_OP = new EglTokenType("-");
  IElementType EGL_NEW_KEY = new EglTokenType("new");
  IElementType EGL_NE_OP = new EglTokenType("<>");
  IElementType EGL_NOT_OP = new EglTokenType("not");
  IElementType EGL_OPERATION_KEY = new EglTokenType("operation");
  IElementType EGL_ORDSET_KEY = new EglTokenType("OrderedSet");
  IElementType EGL_OR_OP = new EglTokenType("or");
  IElementType EGL_PLUS_ASSIGN_OP = new EglTokenType("+=");
  IElementType EGL_PLUS_OP = new EglTokenType("+");
  IElementType EGL_POINT_OP = new EglTokenType(".");
  IElementType EGL_POINT_POINT_OP = new EglTokenType("..");
  IElementType EGL_POST_COND = new EglTokenType("$post");
  IElementType EGL_POST_KEY = new EglTokenType("post");
  IElementType EGL_PRE_COND = new EglTokenType("$pre");
  IElementType EGL_PRE_KEY = new EglTokenType("pre");
  IElementType EGL_QUAL_OP = new EglTokenType("::");
  IElementType EGL_RETURN_KEY = new EglTokenType("return");
  IElementType EGL_RIGHT_BRACE = new EglTokenType("}");
  IElementType EGL_RIGHT_PAREN = new EglTokenType(")");
  IElementType EGL_RIGHT_SQ = new EglTokenType("]");
  IElementType EGL_SATISFIES_ALL_BIN = new EglTokenType("satisfiesAll");
  IElementType EGL_SATISFIES_BIN = new EglTokenType("satisfies");
  IElementType EGL_SATISFIES_ONE_BIN = new EglTokenType("satisfiesOne");
  IElementType EGL_SELF_BIN = new EglTokenType("self");
  IElementType EGL_SEMICOLON = new EglTokenType(";");
  IElementType EGL_SEQ_KEY = new EglTokenType("Sequence");
  IElementType EGL_SET_KEY = new EglTokenType("Set");
  IElementType EGL_SPECIAL_ASSIGN_OP = new EglTokenType(":=");
  IElementType EGL_STRING = new EglTokenType("STRING");
  IElementType EGL_SWITCH_KEY = new EglTokenType("switch");
  IElementType EGL_THEN_OP = new EglTokenType("=>");
  IElementType EGL_THROW_KEY = new EglTokenType("throw");
  IElementType EGL_TIMES_ASSIGN_OP = new EglTokenType("*=");
  IElementType EGL_TIMES_OP = new EglTokenType("*");
  IElementType EGL_TRANS_KEY = new EglTokenType("transaction");
  IElementType EGL_TRUE_KEY = new EglTokenType("true");
  IElementType EGL_VAR_KEY = new EglTokenType("var");
  IElementType EGL_VERBATIM_TEXT = new EglTokenType("");
  IElementType EGL_WHILE_KEY = new EglTokenType("while");
  IElementType EGL_XOR_OP = new EglTokenType("xor");

  class Factory {
    public static PsiElement createElement(ASTNode node) {
      IElementType type = node.getElementType();
      if (type == EGL_ABORT_STATEMENT) {
        return new EglAbortStatementImpl(node);
      }
      else if (type == EGL_ADD_EXPR) {
        return new EglAddExprImpl(node);
      }
      else if (type == EGL_ANNOTATION_BLOCK) {
        return new EglAnnotationBlockImpl(node);
      }
      else if (type == EGL_ASSIGNMENT_STATEMENT) {
        return new EglAssignmentStatementImpl(node);
      }
      else if (type == EGL_BOOLEAN) {
        return new EglBooleanImpl(node);
      }
      else if (type == EGL_BOOL_EXPR) {
        return new EglBoolExprImpl(node);
      }
      else if (type == EGL_BREAK_ALL_STATEMENT) {
        return new EglBreakAllStatementImpl(node);
      }
      else if (type == EGL_BREAK_STATEMENT) {
        return new EglBreakStatementImpl(node);
      }
      else if (type == EGL_BUILT_IN) {
        return new EglBuiltInImpl(node);
      }
      else if (type == EGL_CASE_STATEMENT) {
        return new EglCaseStatementImpl(node);
      }
      else if (type == EGL_COLLECTION_TYPE) {
        return new EglCollectionTypeImpl(node);
      }
      else if (type == EGL_COMPLEX_FEATURE_CALL) {
        return new EglComplexFeatureCallImpl(node);
      }
      else if (type == EGL_CONTENT) {
        return new EglContentImpl(node);
      }
      else if (type == EGL_CONTINUE_STATEMENT) {
        return new EglContinueStatementImpl(node);
      }
      else if (type == EGL_DEFAULT_STATEMENT) {
        return new EglDefaultStatementImpl(node);
      }
      else if (type == EGL_DELETE_STATEMENT) {
        return new EglDeleteStatementImpl(node);
      }
      else if (type == EGL_ELSE_STATEMENT) {
        return new EglElseStatementImpl(node);
      }
      else if (type == EGL_EQ_EXPR) {
        return new EglEqExprImpl(node);
      }
      else if (type == EGL_EXECUTABLE_ANNOTATION) {
        return new EglExecutableAnnotationImpl(node);
      }
      else if (type == EGL_EXPR) {
        return new EglExprImpl(node);
      }
      else if (type == EGL_EXPRESSION_IN_BRACKETS) {
        return new EglExpressionInBracketsImpl(node);
      }
      else if (type == EGL_EXPRESSION_LIST_OR_RANGE) {
        return new EglExpressionListOrRangeImpl(node);
      }
      else if (type == EGL_EXPRESSION_RANGE) {
        return new EglExpressionRangeImpl(node);
      }
      else if (type == EGL_EXPRESSION_STATEMENT) {
        return new EglExpressionStatementImpl(node);
      }
      else if (type == EGL_FEATURE_CALL) {
        return new EglFeatureCallImpl(node);
      }
      else if (type == EGL_FORMAL_PARAMETER) {
        return new EglFormalParameterImpl(node);
      }
      else if (type == EGL_FORMAL_PARAMETER_LIST) {
        return new EglFormalParameterListImpl(node);
      }
      else if (type == EGL_FOR_STATEMENT) {
        return new EglForStatementImpl(node);
      }
      else if (type == EGL_ID_LIST) {
        return new EglIdListImpl(node);
      }
      else if (type == EGL_IF_STATEMENT) {
        return new EglIfStatementImpl(node);
      }
      else if (type == EGL_IMPORT_STATEMENT) {
        return new EglImportStatementImpl(node);
      }
      else if (type == EGL_ITEM_SELECTOR_EXPR) {
        return new EglItemSelectorExprImpl(node);
      }
      else if (type == EGL_KEYVAL_EXPRESSION) {
        return new EglKeyvalExpressionImpl(node);
      }
      else if (type == EGL_LAMBDA_EXPRESSION) {
        return new EglLambdaExpressionImpl(node);
      }
      else if (type == EGL_LAMBDA_EXPRESSION_IN_BRACKETS) {
        return new EglLambdaExpressionInBracketsImpl(node);
      }
      else if (type == EGL_LITERAL) {
        return new EglLiteralImpl(node);
      }
      else if (type == EGL_LITERAL_MAP_COLLECTION) {
        return new EglLiteralMapCollectionImpl(node);
      }
      else if (type == EGL_LITERAL_SEQUENTIAL_COLLECTION) {
        return new EglLiteralSequentialCollectionImpl(node);
      }
      else if (type == EGL_MODEL_ALIAS) {
        return new EglModelAliasImpl(node);
      }
      else if (type == EGL_MODEL_DECLARATION) {
        return new EglModelDeclarationImpl(node);
      }
      else if (type == EGL_MODEL_DECLARATION_PARAMETER) {
        return new EglModelDeclarationParameterImpl(node);
      }
      else if (type == EGL_MODEL_DECLARATION_PARAMETERS) {
        return new EglModelDeclarationParametersImpl(node);
      }
      else if (type == EGL_MODEL_DRIVER) {
        return new EglModelDriverImpl(node);
      }
      else if (type == EGL_MODULE) {
        return new EglModuleImpl(node);
      }
      else if (type == EGL_MULT_EXPR) {
        return new EglMultExprImpl(node);
      }
      else if (type == EGL_NATIVE_TYPE) {
        return new EglNativeTypeImpl(node);
      }
      else if (type == EGL_NEW_EXPR) {
        return new EglNewExprImpl(node);
      }
      else if (type == EGL_OPERATION_DECLARATION) {
        return new EglOperationDeclarationImpl(node);
      }
      else if (type == EGL_OPERATION_DECLARATION_OR_ANNOTATION_BLOCK) {
        return new EglOperationDeclarationOrAnnotationBlockImpl(node);
      }
      else if (type == EGL_OPERATION_START) {
        return new EglOperationStartImpl(node);
      }
      else if (type == EGL_PACKAGED_TYPE) {
        return new EglPackagedTypeImpl(node);
      }
      else if (type == EGL_PARAMETER_LIST) {
        return new EglParameterListImpl(node);
      }
      else if (type == EGL_PATH_NAME) {
        return new EglPathNameImpl(node);
      }
      else if (type == EGL_POSTFIX_EXPR) {
        return new EglPostfixExprImpl(node);
      }
      else if (type == EGL_PRIMITIVE_EXPR) {
        return new EglPrimitiveExprImpl(node);
      }
      else if (type == EGL_PRINT) {
        return new EglPrintImpl(node);
      }
      else if (type == EGL_REL_EXPR) {
        return new EglRelExprImpl(node);
      }
      else if (type == EGL_RETURN_STATEMENT) {
        return new EglReturnStatementImpl(node);
      }
      else if (type == EGL_SHORTCUT_OPERATOR_EXPR) {
        return new EglShortcutOperatorExprImpl(node);
      }
      else if (type == EGL_SIMPLE_ANNOTATION) {
        return new EglSimpleAnnotationImpl(node);
      }
      else if (type == EGL_SIMPLE_FEATURE_CALL) {
        return new EglSimpleFeatureCallImpl(node);
      }
      else if (type == EGL_STATEMENT) {
        return new EglStatementImpl(node);
      }
      else if (type == EGL_STATEMENT_BLOCK) {
        return new EglStatementBlockImpl(node);
      }
      else if (type == EGL_SWITCH_STATEMENT) {
        return new EglSwitchStatementImpl(node);
      }
      else if (type == EGL_THROW_STATEMENT) {
        return new EglThrowStatementImpl(node);
      }
      else if (type == EGL_TRANSACTION_STATEMENT) {
        return new EglTransactionStatementImpl(node);
      }
      else if (type == EGL_TYPE_NAME) {
        return new EglTypeNameImpl(node);
      }
      else if (type == EGL_TYPE_NAME_LIST) {
        return new EglTypeNameListImpl(node);
      }
      else if (type == EGL_VARIABLE_DECLARATION_EXPR) {
        return new EglVariableDeclarationExprImpl(node);
      }
      else if (type == EGL_WHILE_STATEMENT) {
        return new EglWhileStatementImpl(node);
      }
      throw new AssertionError("Unknown element type: " + type);
    }
  }
}

/*******************************************************************************
 * Copyright (c) 2008 The University of York.
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *     Horacio Hoyos Rodriguez - initial API and implementation
 ******************************************************************************/
package org.eclipse.epislon.labs.intellij.grammar.psi.ecl;

import org.jetbrains.annotations.*;
import com.intellij.psi.PsiElementVisitor;
import com.intellij.psi.PsiElement;
import org.eclipse.epislon.labs.intellij.grammar.psi.EpsilonModule;

public class EclVisitor extends PsiElementVisitor {

  public void visitCompareBlock(@NotNull EclCompareBlock o) {
    visitPsiElement(o);
  }

  public void visitContent(@NotNull EclContent o) {
    visitPsiElement(o);
  }

  public void visitDoBlock(@NotNull EclDoBlock o) {
    visitPsiElement(o);
  }

  public void visitExtends(@NotNull EclExtends o) {
    visitPsiElement(o);
  }

  public void visitFormalParameter(@NotNull EclFormalParameter o) {
    visitPsiElement(o);
  }

  public void visitGuard(@NotNull EclGuard o) {
    visitPsiElement(o);
  }

  public void visitMatchRule(@NotNull EclMatchRule o) {
    visitPsiElement(o);
  }

  public void visitModule(@NotNull EclModule o) {
    visitEpsilonModule(o);
  }

  public void visitPost(@NotNull EclPost o) {
    visitPsiElement(o);
  }

  public void visitPre(@NotNull EclPre o) {
    visitPsiElement(o);
  }

  public void visitStatement(@NotNull EclStatement o) {
    visitPsiElement(o);
  }

  public void visitEpsilonModule(@NotNull EpsilonModule o) {
    visitElement(o);
  }

  public void visitPsiElement(@NotNull PsiElement o) {
    visitElement(o);
  }

}

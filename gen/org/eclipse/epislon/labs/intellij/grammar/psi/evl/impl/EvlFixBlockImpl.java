/*******************************************************************************
 * Copyright (c) 2008 The University of York.
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *     Horacio Hoyos Rodriguez - initial API and implementation
 ******************************************************************************/
package org.eclipse.epislon.labs.intellij.grammar.psi.evl.impl;

import java.util.List;
import org.jetbrains.annotations.*;
import com.intellij.lang.ASTNode;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiElementVisitor;
import com.intellij.psi.util.PsiTreeUtil;
import static org.eclipse.epislon.labs.intellij.grammar.psi.evl.EvlTypes.*;
import com.intellij.extapi.psi.ASTWrapperPsiElement;
import org.eclipse.epislon.labs.intellij.grammar.psi.evl.*;
import org.eclipse.epislon.labs.intellij.grammar.parser.evl.EvlParserUtil;

public class EvlFixBlockImpl extends ASTWrapperPsiElement implements EvlFixBlock {

  public EvlFixBlockImpl(@NotNull ASTNode node) {
    super(node);
  }

  public void accept(@NotNull EvlVisitor visitor) {
    visitor.visitFixBlock(this);
  }

  public void accept(@NotNull PsiElementVisitor visitor) {
    if (visitor instanceof EvlVisitor) accept((EvlVisitor)visitor);
    else super.accept(visitor);
  }

  @Override
  @NotNull
  public EvlFixBody getFixBody() {
    return findNotNullChildByClass(EvlFixBody.class);
  }

  @Override
  @Nullable
  public EvlGuard getGuard() {
    return findChildByClass(EvlGuard.class);
  }

  @Override
  @NotNull
  public EvlTitle getTitle() {
    return findNotNullChildByClass(EvlTitle.class);
  }

  @Override
  @NotNull
  public PsiElement getLeftBrace() {
    return EvlParserUtil.getLeftBrace(this);
  }

  @Override
  @Nullable
  public PsiElement getGuardKey() {
    return EvlParserUtil.getGuardKey(this);
  }

  @Override
  @NotNull
  public PsiElement getRightBrace() {
    return EvlParserUtil.getRightBrace(this);
  }

}

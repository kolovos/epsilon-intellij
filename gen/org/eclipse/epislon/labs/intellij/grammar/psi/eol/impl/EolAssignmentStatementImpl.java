/*******************************************************************************
 * Copyright (c) 2008 The University of York.
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *     Horacio Hoyos Rodriguez - initial API and implementation
 ******************************************************************************/
package org.eclipse.epislon.labs.intellij.grammar.psi.eol.impl;

import java.util.List;
import org.jetbrains.annotations.*;
import com.intellij.lang.ASTNode;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiElementVisitor;
import com.intellij.psi.util.PsiTreeUtil;
import static org.eclipse.epislon.labs.intellij.grammar.psi.eol.EolTypes.*;
import com.intellij.extapi.psi.ASTWrapperPsiElement;
import org.eclipse.epislon.labs.intellij.grammar.psi.eol.*;

public class EolAssignmentStatementImpl extends ASTWrapperPsiElement implements EolAssignmentStatement {

  public EolAssignmentStatementImpl(@NotNull ASTNode node) {
    super(node);
  }

  public void accept(@NotNull EolVisitor visitor) {
    visitor.visitAssignmentStatement(this);
  }

  public void accept(@NotNull PsiElementVisitor visitor) {
    if (visitor instanceof EolVisitor) accept((EolVisitor)visitor);
    else super.accept(visitor);
  }

  @Override
  @NotNull
  public List<EolExpr> getExprList() {
    return PsiTreeUtil.getChildrenOfTypeAsList(this, EolExpr.class);
  }

  @Override
  @Nullable
  public PsiElement getDivAssigOp() {
    return findChildByType(EOL_DIV_ASSIG_OP);
  }

  @Override
  @Nullable
  public PsiElement getEquivAssignOp() {
    return findChildByType(EOL_EQUIV_ASSIGN_OP);
  }

  @Override
  @Nullable
  public PsiElement getMinusAssignOp() {
    return findChildByType(EOL_MINUS_ASSIGN_OP);
  }

  @Override
  @Nullable
  public PsiElement getPlusAssignOp() {
    return findChildByType(EOL_PLUS_ASSIGN_OP);
  }

  @Override
  @NotNull
  public PsiElement getSemicolon() {
    return findNotNullChildByType(EOL_SEMICOLON);
  }

  @Override
  @Nullable
  public PsiElement getSpecialAssignOp() {
    return findChildByType(EOL_SPECIAL_ASSIGN_OP);
  }

  @Override
  @Nullable
  public PsiElement getTimesAssignOp() {
    return findChildByType(EOL_TIMES_ASSIGN_OP);
  }

}

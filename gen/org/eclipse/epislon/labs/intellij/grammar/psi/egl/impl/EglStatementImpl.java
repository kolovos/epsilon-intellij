/*******************************************************************************
 * Copyright (c) 2008 The University of York.
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *     Horacio Hoyos Rodriguez - initial API and implementation
 ******************************************************************************/
package org.eclipse.epislon.labs.intellij.grammar.psi.egl.impl;

import java.util.List;
import org.jetbrains.annotations.*;
import com.intellij.lang.ASTNode;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiElementVisitor;
import com.intellij.psi.util.PsiTreeUtil;
import static org.eclipse.epislon.labs.intellij.grammar.psi.egl.EglTypes.*;
import com.intellij.extapi.psi.ASTWrapperPsiElement;
import org.eclipse.epislon.labs.intellij.grammar.psi.egl.*;

public class EglStatementImpl extends ASTWrapperPsiElement implements EglStatement {

  public EglStatementImpl(@NotNull ASTNode node) {
    super(node);
  }

  public void accept(@NotNull EglVisitor visitor) {
    visitor.visitStatement(this);
  }

  public void accept(@NotNull PsiElementVisitor visitor) {
    if (visitor instanceof EglVisitor) accept((EglVisitor)visitor);
    else super.accept(visitor);
  }

  @Override
  @Nullable
  public EglAbortStatement getAbortStatement() {
    return findChildByClass(EglAbortStatement.class);
  }

  @Override
  @Nullable
  public EglAssignmentStatement getAssignmentStatement() {
    return findChildByClass(EglAssignmentStatement.class);
  }

  @Override
  @Nullable
  public EglBreakAllStatement getBreakAllStatement() {
    return findChildByClass(EglBreakAllStatement.class);
  }

  @Override
  @Nullable
  public EglBreakStatement getBreakStatement() {
    return findChildByClass(EglBreakStatement.class);
  }

  @Override
  @Nullable
  public EglContinueStatement getContinueStatement() {
    return findChildByClass(EglContinueStatement.class);
  }

  @Override
  @Nullable
  public EglDeleteStatement getDeleteStatement() {
    return findChildByClass(EglDeleteStatement.class);
  }

  @Override
  @Nullable
  public EglExpressionStatement getExpressionStatement() {
    return findChildByClass(EglExpressionStatement.class);
  }

  @Override
  @Nullable
  public EglForStatement getForStatement() {
    return findChildByClass(EglForStatement.class);
  }

  @Override
  @Nullable
  public EglIfStatement getIfStatement() {
    return findChildByClass(EglIfStatement.class);
  }

  @Override
  @Nullable
  public EglPrint getPrint() {
    return findChildByClass(EglPrint.class);
  }

  @Override
  @Nullable
  public EglReturnStatement getReturnStatement() {
    return findChildByClass(EglReturnStatement.class);
  }

  @Override
  @Nullable
  public EglSwitchStatement getSwitchStatement() {
    return findChildByClass(EglSwitchStatement.class);
  }

  @Override
  @Nullable
  public EglThrowStatement getThrowStatement() {
    return findChildByClass(EglThrowStatement.class);
  }

  @Override
  @Nullable
  public EglTransactionStatement getTransactionStatement() {
    return findChildByClass(EglTransactionStatement.class);
  }

  @Override
  @Nullable
  public EglWhileStatement getWhileStatement() {
    return findChildByClass(EglWhileStatement.class);
  }

}

/*******************************************************************************
 * Copyright (c) 2008 The University of York.
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *     Horacio Hoyos Rodriguez - initial API and implementation
 ******************************************************************************/
package org.eclipse.epislon.labs.intellij.grammar.psi.etl.impl;

import java.util.List;
import org.jetbrains.annotations.*;
import com.intellij.lang.ASTNode;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiElementVisitor;
import com.intellij.psi.util.PsiTreeUtil;
import static org.eclipse.epislon.labs.intellij.grammar.psi.etl.EtlTypes.*;
import com.intellij.extapi.psi.ASTWrapperPsiElement;
import org.eclipse.epislon.labs.intellij.grammar.psi.etl.*;
import org.eclipse.epislon.labs.intellij.grammar.parser.etl.EtlParserUtil;

public class EtlExtendsImpl extends ASTWrapperPsiElement implements EtlExtends {

  public EtlExtendsImpl(@NotNull ASTNode node) {
    super(node);
  }

  public void accept(@NotNull EtlVisitor visitor) {
    visitor.visitExtends(this);
  }

  public void accept(@NotNull PsiElementVisitor visitor) {
    if (visitor instanceof EtlVisitor) accept((EtlVisitor)visitor);
    else super.accept(visitor);
  }

}

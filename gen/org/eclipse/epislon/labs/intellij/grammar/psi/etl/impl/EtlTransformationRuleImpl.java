/*******************************************************************************
 * Copyright (c) 2008 The University of York.
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *     Horacio Hoyos Rodriguez - initial API and implementation
 ******************************************************************************/
package org.eclipse.epislon.labs.intellij.grammar.psi.etl.impl;

import java.util.List;
import org.jetbrains.annotations.*;
import com.intellij.lang.ASTNode;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiElementVisitor;
import com.intellij.psi.util.PsiTreeUtil;
import static org.eclipse.epislon.labs.intellij.grammar.psi.etl.EtlTypes.*;
import com.intellij.extapi.psi.ASTWrapperPsiElement;
import org.eclipse.epislon.labs.intellij.grammar.psi.etl.*;
import org.eclipse.epislon.labs.intellij.grammar.parser.etl.EtlParserUtil;

public class EtlTransformationRuleImpl extends ASTWrapperPsiElement implements EtlTransformationRule {

  public EtlTransformationRuleImpl(@NotNull ASTNode node) {
    super(node);
  }

  public void accept(@NotNull EtlVisitor visitor) {
    visitor.visitTransformationRule(this);
  }

  public void accept(@NotNull PsiElementVisitor visitor) {
    if (visitor instanceof EtlVisitor) accept((EtlVisitor)visitor);
    else super.accept(visitor);
  }

  @Override
  @Nullable
  public EtlExtends getExtends() {
    return findChildByClass(EtlExtends.class);
  }

  @Override
  @NotNull
  public EtlFormalParameter getFormalParameter() {
    return findNotNullChildByClass(EtlFormalParameter.class);
  }

  @Override
  @NotNull
  public EtlFormalParameterList getFormalParameterList() {
    return findNotNullChildByClass(EtlFormalParameterList.class);
  }

  @Override
  @Nullable
  public EtlGuard getGuard() {
    return findChildByClass(EtlGuard.class);
  }

  @Override
  @NotNull
  public List<EtlStatement> getStatementList() {
    return PsiTreeUtil.getChildrenOfTypeAsList(this, EtlStatement.class);
  }

  @Override
  @NotNull
  public PsiElement getRuleKey() {
    return findNotNullChildByType(ETL_RULE_KEY);
  }

  @Override
  @NotNull
  public PsiElement getToKey() {
    return findNotNullChildByType(ETL_TO_KEY);
  }

  @Override
  @NotNull
  public PsiElement getTransformKey() {
    return findNotNullChildByType(ETL_TRANSFORM_KEY);
  }

  @Override
  @NotNull
  public PsiElement getLeftBrace() {
    return EtlParserUtil.getLeftBrace(this);
  }

}

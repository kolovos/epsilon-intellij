/*******************************************************************************
 * Copyright (c) 2008 The University of York.
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *     Horacio Hoyos Rodriguez - initial API and implementation
 ******************************************************************************/
package org.eclipse.epislon.labs.intellij.grammar.psi.evl.impl;

import java.util.List;
import org.jetbrains.annotations.*;
import com.intellij.lang.ASTNode;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiElementVisitor;
import com.intellij.psi.util.PsiTreeUtil;
import static org.eclipse.epislon.labs.intellij.grammar.psi.evl.EvlTypes.*;
import com.intellij.extapi.psi.ASTWrapperPsiElement;
import org.eclipse.epislon.labs.intellij.grammar.psi.evl.*;
import org.eclipse.epislon.labs.intellij.grammar.parser.evl.EvlParserUtil;
import org.eclipse.epislon.labs.intellij.grammar.psi.eol.EolAnnotationBlock;

public class EvlContextContentImpl extends ASTWrapperPsiElement implements EvlContextContent {

  public EvlContextContentImpl(@NotNull ASTNode node) {
    super(node);
  }

  public void accept(@NotNull EvlVisitor visitor) {
    visitor.visitContextContent(this);
  }

  public void accept(@NotNull PsiElementVisitor visitor) {
    if (visitor instanceof EvlVisitor) accept((EvlVisitor)visitor);
    else super.accept(visitor);
  }

  @Override
  @Nullable
  public EvlConstraint getConstraint() {
    return findChildByClass(EvlConstraint.class);
  }

  @Override
  @Nullable
  public EvlCritique getCritique() {
    return findChildByClass(EvlCritique.class);
  }

  @Override
  @Nullable
  public EolAnnotationBlock getAnnotationBlock() {
    return EvlParserUtil.getAnnotationBlock(this);
  }

}

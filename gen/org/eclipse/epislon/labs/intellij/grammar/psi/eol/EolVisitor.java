/*******************************************************************************
 * Copyright (c) 2008 The University of York.
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *     Horacio Hoyos Rodriguez - initial API and implementation
 ******************************************************************************/
package org.eclipse.epislon.labs.intellij.grammar.psi.eol;

import org.jetbrains.annotations.*;
import com.intellij.psi.PsiElementVisitor;
import com.intellij.psi.PsiElement;
import org.eclipse.epislon.labs.intellij.grammar.psi.EpsilonModule;

public class EolVisitor extends PsiElementVisitor {

  public void visitAbortStatement(@NotNull EolAbortStatement o) {
    visitPsiElement(o);
  }

  public void visitAddExpr(@NotNull EolAddExpr o) {
    visitExpr(o);
  }

  public void visitAnnotationBlock(@NotNull EolAnnotationBlock o) {
    visitPsiElement(o);
  }

  public void visitAssignmentStatement(@NotNull EolAssignmentStatement o) {
    visitPsiElement(o);
  }

  public void visitBoolExpr(@NotNull EolBoolExpr o) {
    visitExpr(o);
  }

  public void visitBoolean(@NotNull EolBoolean o) {
    visitPsiElement(o);
  }

  public void visitBreakAllStatement(@NotNull EolBreakAllStatement o) {
    visitPsiElement(o);
  }

  public void visitBreakStatement(@NotNull EolBreakStatement o) {
    visitPsiElement(o);
  }

  public void visitBuiltIn(@NotNull EolBuiltIn o) {
    visitPsiElement(o);
  }

  public void visitCaseStatement(@NotNull EolCaseStatement o) {
    visitPsiElement(o);
  }

  public void visitCollectionType(@NotNull EolCollectionType o) {
    visitPsiElement(o);
  }

  public void visitComplexFeatureCall(@NotNull EolComplexFeatureCall o) {
    visitPsiElement(o);
  }

  public void visitContent(@NotNull EolContent o) {
    visitPsiElement(o);
  }

  public void visitContinueStatement(@NotNull EolContinueStatement o) {
    visitPsiElement(o);
  }

  public void visitDefaultStatement(@NotNull EolDefaultStatement o) {
    visitPsiElement(o);
  }

  public void visitDeleteStatement(@NotNull EolDeleteStatement o) {
    visitPsiElement(o);
  }

  public void visitElseStatement(@NotNull EolElseStatement o) {
    visitPsiElement(o);
  }

  public void visitEqExpr(@NotNull EolEqExpr o) {
    visitExpr(o);
  }

  public void visitExecutableAnnotation(@NotNull EolExecutableAnnotation o) {
    visitPsiElement(o);
  }

  public void visitExpr(@NotNull EolExpr o) {
    visitPsiElement(o);
  }

  public void visitExprOrStatementBlock(@NotNull EolExprOrStatementBlock o) {
    visitPsiElement(o);
  }

  public void visitExpressionInBrackets(@NotNull EolExpressionInBrackets o) {
    visitPsiElement(o);
  }

  public void visitExpressionListOrRange(@NotNull EolExpressionListOrRange o) {
    visitPsiElement(o);
  }

  public void visitExpressionRange(@NotNull EolExpressionRange o) {
    visitPsiElement(o);
  }

  public void visitExpressionStatement(@NotNull EolExpressionStatement o) {
    visitPsiElement(o);
  }

  public void visitFeatureCall(@NotNull EolFeatureCall o) {
    visitPsiElement(o);
  }

  public void visitForStatement(@NotNull EolForStatement o) {
    visitPsiElement(o);
  }

  public void visitFormalParameter(@NotNull EolFormalParameter o) {
    visitPsiElement(o);
  }

  public void visitFormalParameterList(@NotNull EolFormalParameterList o) {
    visitPsiElement(o);
  }

  public void visitIdList(@NotNull EolIdList o) {
    visitPsiElement(o);
  }

  public void visitIfStatement(@NotNull EolIfStatement o) {
    visitPsiElement(o);
  }

  public void visitImportStatement(@NotNull EolImportStatement o) {
    visitPsiElement(o);
  }

  public void visitItemSelectorExpr(@NotNull EolItemSelectorExpr o) {
    visitExpr(o);
  }

  public void visitKeyvalExpression(@NotNull EolKeyvalExpression o) {
    visitPsiElement(o);
  }

  public void visitLambdaExpression(@NotNull EolLambdaExpression o) {
    visitPsiElement(o);
  }

  public void visitLambdaExpressionInBrackets(@NotNull EolLambdaExpressionInBrackets o) {
    visitPsiElement(o);
  }

  public void visitLiteral(@NotNull EolLiteral o) {
    visitPsiElement(o);
  }

  public void visitLiteralMapCollection(@NotNull EolLiteralMapCollection o) {
    visitPsiElement(o);
  }

  public void visitLiteralSequentialCollection(@NotNull EolLiteralSequentialCollection o) {
    visitPsiElement(o);
  }

  public void visitModelAlias(@NotNull EolModelAlias o) {
    visitPsiElement(o);
  }

  public void visitModelDeclaration(@NotNull EolModelDeclaration o) {
    visitPsiElement(o);
  }

  public void visitModelDeclarationParameter(@NotNull EolModelDeclarationParameter o) {
    visitPsiElement(o);
  }

  public void visitModelDeclarationParameters(@NotNull EolModelDeclarationParameters o) {
    visitPsiElement(o);
  }

  public void visitModelDriver(@NotNull EolModelDriver o) {
    visitPsiElement(o);
  }

  public void visitModule(@NotNull EolModule o) {
    visitEpsilonModule(o);
  }

  public void visitMultExpr(@NotNull EolMultExpr o) {
    visitExpr(o);
  }

  public void visitNativeType(@NotNull EolNativeType o) {
    visitPsiElement(o);
  }

  public void visitNewExpr(@NotNull EolNewExpr o) {
    visitExpr(o);
  }

  public void visitOperationDeclaration(@NotNull EolOperationDeclaration o) {
    visitPsiElement(o);
  }

  public void visitOperationDeclarationOrAnnotationBlock(@NotNull EolOperationDeclarationOrAnnotationBlock o) {
    visitPsiElement(o);
  }

  public void visitOperationStart(@NotNull EolOperationStart o) {
    visitPsiElement(o);
  }

  public void visitPackagedType(@NotNull EolPackagedType o) {
    visitPsiElement(o);
  }

  public void visitParameterList(@NotNull EolParameterList o) {
    visitPsiElement(o);
  }

  public void visitPathName(@NotNull EolPathName o) {
    visitPsiElement(o);
  }

  public void visitPostfixExpr(@NotNull EolPostfixExpr o) {
    visitExpr(o);
  }

  public void visitPrimitiveExpr(@NotNull EolPrimitiveExpr o) {
    visitExpr(o);
  }

  public void visitRelExpr(@NotNull EolRelExpr o) {
    visitExpr(o);
  }

  public void visitReturnStatement(@NotNull EolReturnStatement o) {
    visitPsiElement(o);
  }

  public void visitShortcutOperatorExpr(@NotNull EolShortcutOperatorExpr o) {
    visitExpr(o);
  }

  public void visitSimpleAnnotation(@NotNull EolSimpleAnnotation o) {
    visitPsiElement(o);
  }

  public void visitSimpleFeatureCall(@NotNull EolSimpleFeatureCall o) {
    visitPsiElement(o);
  }

  public void visitStatement(@NotNull EolStatement o) {
    visitPsiElement(o);
  }

  public void visitStatementBlock(@NotNull EolStatementBlock o) {
    visitPsiElement(o);
  }

  public void visitSwitchStatement(@NotNull EolSwitchStatement o) {
    visitPsiElement(o);
  }

  public void visitThrowStatement(@NotNull EolThrowStatement o) {
    visitPsiElement(o);
  }

  public void visitTransactionStatement(@NotNull EolTransactionStatement o) {
    visitPsiElement(o);
  }

  public void visitTypeName(@NotNull EolTypeName o) {
    visitPsiElement(o);
  }

  public void visitTypeNameList(@NotNull EolTypeNameList o) {
    visitPsiElement(o);
  }

  public void visitVariableDeclarationExpr(@NotNull EolVariableDeclarationExpr o) {
    visitExpr(o);
  }

  public void visitWhileStatement(@NotNull EolWhileStatement o) {
    visitPsiElement(o);
  }

  public void visitEpsilonModule(@NotNull EpsilonModule o) {
    visitElement(o);
  }

  public void visitPsiElement(@NotNull PsiElement o) {
    visitElement(o);
  }

}

/*******************************************************************************
 * Copyright (c) 2008 The University of York.
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *     Horacio Hoyos Rodriguez - initial API and implementation
 ******************************************************************************/
package org.eclipse.epislon.labs.intellij.grammar.psi.eol.impl;

import java.util.List;
import org.jetbrains.annotations.*;
import com.intellij.lang.ASTNode;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiElementVisitor;
import com.intellij.psi.util.PsiTreeUtil;
import static org.eclipse.epislon.labs.intellij.grammar.psi.eol.EolTypes.*;
import org.eclipse.epislon.labs.intellij.grammar.psi.eol.*;

public class EolMultExprImpl extends EolExprImpl implements EolMultExpr {

  public EolMultExprImpl(@NotNull ASTNode node) {
    super(node);
  }

  public void accept(@NotNull EolVisitor visitor) {
    visitor.visitMultExpr(this);
  }

  public void accept(@NotNull PsiElementVisitor visitor) {
    if (visitor instanceof EolVisitor) accept((EolVisitor)visitor);
    else super.accept(visitor);
  }

  @Override
  @NotNull
  public List<EolExpr> getExprList() {
    return PsiTreeUtil.getChildrenOfTypeAsList(this, EolExpr.class);
  }

  @Override
  @Nullable
  public PsiElement getDivOp() {
    return findChildByType(EOL_DIV_OP);
  }

  @Override
  @Nullable
  public PsiElement getTimesOp() {
    return findChildByType(EOL_TIMES_OP);
  }

}

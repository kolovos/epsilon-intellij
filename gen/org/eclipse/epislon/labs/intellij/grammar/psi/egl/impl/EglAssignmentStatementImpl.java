/*******************************************************************************
 * Copyright (c) 2008 The University of York.
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *     Horacio Hoyos Rodriguez - initial API and implementation
 ******************************************************************************/
package org.eclipse.epislon.labs.intellij.grammar.psi.egl.impl;

import java.util.List;
import org.jetbrains.annotations.*;
import com.intellij.lang.ASTNode;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiElementVisitor;
import com.intellij.psi.util.PsiTreeUtil;
import static org.eclipse.epislon.labs.intellij.grammar.psi.egl.EglTypes.*;
import com.intellij.extapi.psi.ASTWrapperPsiElement;
import org.eclipse.epislon.labs.intellij.grammar.psi.egl.*;

public class EglAssignmentStatementImpl extends ASTWrapperPsiElement implements EglAssignmentStatement {

  public EglAssignmentStatementImpl(@NotNull ASTNode node) {
    super(node);
  }

  public void accept(@NotNull EglVisitor visitor) {
    visitor.visitAssignmentStatement(this);
  }

  public void accept(@NotNull PsiElementVisitor visitor) {
    if (visitor instanceof EglVisitor) accept((EglVisitor)visitor);
    else super.accept(visitor);
  }

  @Override
  @NotNull
  public List<EglExpr> getExprList() {
    return PsiTreeUtil.getChildrenOfTypeAsList(this, EglExpr.class);
  }

  @Override
  @Nullable
  public EglPrint getPrint() {
    return findChildByClass(EglPrint.class);
  }

  @Override
  @Nullable
  public PsiElement getDivAssigOp() {
    return findChildByType(EGL_DIV_ASSIG_OP);
  }

  @Override
  @Nullable
  public PsiElement getEquivAssignOp() {
    return findChildByType(EGL_EQUIV_ASSIGN_OP);
  }

  @Override
  @Nullable
  public PsiElement getMinusAssignOp() {
    return findChildByType(EGL_MINUS_ASSIGN_OP);
  }

  @Override
  @Nullable
  public PsiElement getPlusAssignOp() {
    return findChildByType(EGL_PLUS_ASSIGN_OP);
  }

  @Override
  @Nullable
  public PsiElement getSemicolon() {
    return findChildByType(EGL_SEMICOLON);
  }

  @Override
  @Nullable
  public PsiElement getSpecialAssignOp() {
    return findChildByType(EGL_SPECIAL_ASSIGN_OP);
  }

  @Override
  @Nullable
  public PsiElement getTimesAssignOp() {
    return findChildByType(EGL_TIMES_ASSIGN_OP);
  }

}

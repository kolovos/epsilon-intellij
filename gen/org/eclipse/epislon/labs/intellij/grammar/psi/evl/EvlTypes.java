/*******************************************************************************
 * Copyright (c) 2008 The University of York.
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *     Horacio Hoyos Rodriguez - initial API and implementation
 ******************************************************************************/
package org.eclipse.epislon.labs.intellij.grammar.psi.evl;

import com.intellij.psi.tree.IElementType;
import com.intellij.psi.PsiElement;
import com.intellij.lang.ASTNode;
import org.eclipse.epislon.labs.intellij.grammar.psi.evl.impl.*;

public interface EvlTypes {

  IElementType EVL_CHECK = new EvlElementType("EVL_CHECK");
  IElementType EVL_CONSTRAINT = new EvlElementType("EVL_CONSTRAINT");
  IElementType EVL_CONTENT = new EvlElementType("EVL_CONTENT");
  IElementType EVL_CONTEXT = new EvlElementType("EVL_CONTEXT");
  IElementType EVL_CONTEXT_BLOCK = new EvlElementType("EVL_CONTEXT_BLOCK");
  IElementType EVL_CONTEXT_CONTENT = new EvlElementType("EVL_CONTEXT_CONTENT");
  IElementType EVL_CRITIQUE = new EvlElementType("EVL_CRITIQUE");
  IElementType EVL_FIX = new EvlElementType("EVL_FIX");
  IElementType EVL_FIX_BLOCK = new EvlElementType("EVL_FIX_BLOCK");
  IElementType EVL_FIX_BODY = new EvlElementType("EVL_FIX_BODY");
  IElementType EVL_GUARD = new EvlElementType("EVL_GUARD");
  IElementType EVL_INVARIANT_BLOCK = new EvlElementType("EVL_INVARIANT_BLOCK");
  IElementType EVL_MESSAGE = new EvlElementType("EVL_MESSAGE");
  IElementType EVL_MODULE = new EvlElementType("EVL_MODULE");
  IElementType EVL_POST = new EvlElementType("EVL_POST");
  IElementType EVL_PRE = new EvlElementType("EVL_PRE");
  IElementType EVL_STATEMENT = new EvlElementType("EVL_STATEMENT");
  IElementType EVL_TITLE = new EvlElementType("EVL_TITLE");

  IElementType EVL_CHECK_KEY = new EvlTokenType("check");
  IElementType EVL_CONSTRAINT_KEY = new EvlTokenType("constraint");
  IElementType EVL_CONTEXT_KEY = new EvlTokenType("context");
  IElementType EVL_CRITIQUE_KEY = new EvlTokenType("critique");
  IElementType EVL_DO_KEY = new EvlTokenType("do");
  IElementType EVL_FIX_KEY = new EvlTokenType("fix");
  IElementType EVL_MESSAGE_KEY = new EvlTokenType("message");
  IElementType EVL_TITLE_KEY = new EvlTokenType("title");

  class Factory {
    public static PsiElement createElement(ASTNode node) {
      IElementType type = node.getElementType();
      if (type == EVL_CHECK) {
        return new EvlCheckImpl(node);
      }
      else if (type == EVL_CONSTRAINT) {
        return new EvlConstraintImpl(node);
      }
      else if (type == EVL_CONTENT) {
        return new EvlContentImpl(node);
      }
      else if (type == EVL_CONTEXT) {
        return new EvlContextImpl(node);
      }
      else if (type == EVL_CONTEXT_BLOCK) {
        return new EvlContextBlockImpl(node);
      }
      else if (type == EVL_CONTEXT_CONTENT) {
        return new EvlContextContentImpl(node);
      }
      else if (type == EVL_CRITIQUE) {
        return new EvlCritiqueImpl(node);
      }
      else if (type == EVL_FIX) {
        return new EvlFixImpl(node);
      }
      else if (type == EVL_FIX_BLOCK) {
        return new EvlFixBlockImpl(node);
      }
      else if (type == EVL_FIX_BODY) {
        return new EvlFixBodyImpl(node);
      }
      else if (type == EVL_GUARD) {
        return new EvlGuardImpl(node);
      }
      else if (type == EVL_INVARIANT_BLOCK) {
        return new EvlInvariantBlockImpl(node);
      }
      else if (type == EVL_MESSAGE) {
        return new EvlMessageImpl(node);
      }
      else if (type == EVL_MODULE) {
        return new EvlModuleImpl(node);
      }
      else if (type == EVL_POST) {
        return new EvlPostImpl(node);
      }
      else if (type == EVL_PRE) {
        return new EvlPreImpl(node);
      }
      else if (type == EVL_STATEMENT) {
        return new EvlStatementImpl(node);
      }
      else if (type == EVL_TITLE) {
        return new EvlTitleImpl(node);
      }
      throw new AssertionError("Unknown element type: " + type);
    }
  }
}

/*******************************************************************************
 * Copyright (c) 2008 The University of York.
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *     Horacio Hoyos Rodriguez - initial API and implementation
 ******************************************************************************/
package org.eclipse.epislon.labs.intellij.grammar.psi.eol.impl;

import java.util.List;
import org.jetbrains.annotations.*;
import com.intellij.lang.ASTNode;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiElementVisitor;
import com.intellij.psi.util.PsiTreeUtil;
import static org.eclipse.epislon.labs.intellij.grammar.psi.eol.EolTypes.*;
import com.intellij.extapi.psi.ASTWrapperPsiElement;
import org.eclipse.epislon.labs.intellij.grammar.psi.eol.*;

public class EolOperationDeclarationImpl extends ASTWrapperPsiElement implements EolOperationDeclaration {

  public EolOperationDeclarationImpl(@NotNull ASTNode node) {
    super(node);
  }

  public void accept(@NotNull EolVisitor visitor) {
    visitor.visitOperationDeclaration(this);
  }

  public void accept(@NotNull PsiElementVisitor visitor) {
    if (visitor instanceof EolVisitor) accept((EolVisitor)visitor);
    else super.accept(visitor);
  }

  @Override
  @Nullable
  public EolFormalParameterList getFormalParameterList() {
    return findChildByClass(EolFormalParameterList.class);
  }

  @Override
  @NotNull
  public EolOperationStart getOperationStart() {
    return findNotNullChildByClass(EolOperationStart.class);
  }

  @Override
  @NotNull
  public EolStatementBlock getStatementBlock() {
    return findNotNullChildByClass(EolStatementBlock.class);
  }

  @Override
  @NotNull
  public List<EolTypeName> getTypeNameList() {
    return PsiTreeUtil.getChildrenOfTypeAsList(this, EolTypeName.class);
  }

  @Override
  @Nullable
  public PsiElement getColonOp() {
    return findChildByType(EOL_COLON_OP);
  }

  @Override
  @Nullable
  public PsiElement getId() {
    return findChildByType(EOL_ID);
  }

  @Override
  @NotNull
  public PsiElement getLeftParen() {
    return findNotNullChildByType(EOL_LEFT_PAREN);
  }

  @Override
  @NotNull
  public PsiElement getRightParen() {
    return findNotNullChildByType(EOL_RIGHT_PAREN);
  }

}

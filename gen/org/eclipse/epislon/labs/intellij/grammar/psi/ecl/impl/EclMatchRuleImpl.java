/*******************************************************************************
 * Copyright (c) 2008 The University of York.
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *     Horacio Hoyos Rodriguez - initial API and implementation
 ******************************************************************************/
package org.eclipse.epislon.labs.intellij.grammar.psi.ecl.impl;

import java.util.List;
import org.jetbrains.annotations.*;
import com.intellij.lang.ASTNode;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiElementVisitor;
import com.intellij.psi.util.PsiTreeUtil;
import static org.eclipse.epislon.labs.intellij.grammar.psi.ecl.EclTypes.*;
import com.intellij.extapi.psi.ASTWrapperPsiElement;
import org.eclipse.epislon.labs.intellij.grammar.psi.ecl.*;
import org.eclipse.epislon.labs.intellij.grammar.parser.ecl.EclParserUtil;

public class EclMatchRuleImpl extends ASTWrapperPsiElement implements EclMatchRule {

  public EclMatchRuleImpl(@NotNull ASTNode node) {
    super(node);
  }

  public void accept(@NotNull EclVisitor visitor) {
    visitor.visitMatchRule(this);
  }

  public void accept(@NotNull PsiElementVisitor visitor) {
    if (visitor instanceof EclVisitor) accept((EclVisitor)visitor);
    else super.accept(visitor);
  }

  @Override
  @Nullable
  public EclCompareBlock getCompareBlock() {
    return findChildByClass(EclCompareBlock.class);
  }

  @Override
  @Nullable
  public EclDoBlock getDoBlock() {
    return findChildByClass(EclDoBlock.class);
  }

  @Override
  @Nullable
  public EclExtends getExtends() {
    return findChildByClass(EclExtends.class);
  }

  @Override
  @NotNull
  public List<EclFormalParameter> getFormalParameterList() {
    return PsiTreeUtil.getChildrenOfTypeAsList(this, EclFormalParameter.class);
  }

  @Override
  @Nullable
  public EclGuard getGuard() {
    return findChildByClass(EclGuard.class);
  }

  @Override
  @NotNull
  public PsiElement getMatchKey() {
    return findNotNullChildByType(ECL_MATCH_KEY);
  }

  @Override
  @NotNull
  public PsiElement getRuleKey() {
    return findNotNullChildByType(ECL_RULE_KEY);
  }

  @Override
  @NotNull
  public PsiElement getWithKey() {
    return findNotNullChildByType(ECL_WITH_KEY);
  }

  @Override
  @NotNull
  public PsiElement getLeftBrace() {
    return EclParserUtil.getLeftBrace(this);
  }

}

/*******************************************************************************
 * Copyright (c) 2008 The University of York.
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *     Horacio Hoyos Rodriguez - initial API and implementation
 ******************************************************************************/
package org.eclipse.epislon.labs.intellij.grammar.psi.egl.impl;

import java.util.List;
import org.jetbrains.annotations.*;
import com.intellij.lang.ASTNode;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiElementVisitor;
import com.intellij.psi.util.PsiTreeUtil;
import static org.eclipse.epislon.labs.intellij.grammar.psi.egl.EglTypes.*;
import com.intellij.extapi.psi.ASTWrapperPsiElement;
import org.eclipse.epislon.labs.intellij.grammar.psi.egl.*;

public class EglModelDeclarationImpl extends ASTWrapperPsiElement implements EglModelDeclaration {

  public EglModelDeclarationImpl(@NotNull ASTNode node) {
    super(node);
  }

  public void accept(@NotNull EglVisitor visitor) {
    visitor.visitModelDeclaration(this);
  }

  public void accept(@NotNull PsiElementVisitor visitor) {
    if (visitor instanceof EglVisitor) accept((EglVisitor)visitor);
    else super.accept(visitor);
  }

  @Override
  @Nullable
  public EglModelAlias getModelAlias() {
    return findChildByClass(EglModelAlias.class);
  }

  @Override
  @Nullable
  public EglModelDeclarationParameters getModelDeclarationParameters() {
    return findChildByClass(EglModelDeclarationParameters.class);
  }

  @Override
  @Nullable
  public EglModelDriver getModelDriver() {
    return findChildByClass(EglModelDriver.class);
  }

  @Override
  @NotNull
  public PsiElement getId() {
    return findNotNullChildByType(EGL_ID);
  }

  @Override
  @NotNull
  public PsiElement getSemicolon() {
    return findNotNullChildByType(EGL_SEMICOLON);
  }

}

/*******************************************************************************
 * Copyright (c) 2008 The University of York.
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *     Horacio Hoyos Rodriguez - initial API and implementation
 ******************************************************************************/
package org.eclipse.epislon.labs.intellij.grammar.psi.erl.impl;

import java.util.List;
import org.jetbrains.annotations.*;
import com.intellij.lang.ASTNode;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiElementVisitor;
import com.intellij.psi.util.PsiTreeUtil;
import static org.eclipse.epislon.labs.intellij.grammar.psi.erl.ErlTypes.*;
import com.intellij.extapi.psi.ASTWrapperPsiElement;
import org.eclipse.epislon.labs.intellij.grammar.psi.erl.*;
import org.eclipse.epislon.labs.intellij.grammar.parser.erl.ErlParserUtil;

public class ErlPostImpl extends ASTWrapperPsiElement implements ErlPost {

  public ErlPostImpl(@NotNull ASTNode node) {
    super(node);
  }

  public void accept(@NotNull ErlVisitor visitor) {
    visitor.visitPost(this);
  }

  public void accept(@NotNull PsiElementVisitor visitor) {
    if (visitor instanceof ErlVisitor) accept((ErlVisitor)visitor);
    else super.accept(visitor);
  }

  @Override
  @NotNull
  public List<ErlStatement> getStatementList() {
    return PsiTreeUtil.getChildrenOfTypeAsList(this, ErlStatement.class);
  }

  @Override
  @NotNull
  public PsiElement getPostKey() {
    return findNotNullChildByType(ERL_POST_KEY);
  }

}

/*******************************************************************************
 * Copyright (c) 2008 The University of York.
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *     Horacio Hoyos Rodriguez - initial API and implementation
 ******************************************************************************/
package org.eclipse.epislon.labs.intellij.grammar.psi.etl;

import org.jetbrains.annotations.*;
import com.intellij.psi.PsiElementVisitor;
import com.intellij.psi.PsiElement;
import org.eclipse.epislon.labs.intellij.grammar.psi.EpsilonModule;

public class EtlVisitor extends PsiElementVisitor {

  public void visitContent(@NotNull EtlContent o) {
    visitPsiElement(o);
  }

  public void visitExtends(@NotNull EtlExtends o) {
    visitPsiElement(o);
  }

  public void visitFormalParameter(@NotNull EtlFormalParameter o) {
    visitPsiElement(o);
  }

  public void visitFormalParameterList(@NotNull EtlFormalParameterList o) {
    visitPsiElement(o);
  }

  public void visitGuard(@NotNull EtlGuard o) {
    visitPsiElement(o);
  }

  public void visitModule(@NotNull EtlModule o) {
    visitEpsilonModule(o);
  }

  public void visitPost(@NotNull EtlPost o) {
    visitPsiElement(o);
  }

  public void visitPre(@NotNull EtlPre o) {
    visitPsiElement(o);
  }

  public void visitStatement(@NotNull EtlStatement o) {
    visitPsiElement(o);
  }

  public void visitTransformationRule(@NotNull EtlTransformationRule o) {
    visitPsiElement(o);
  }

  public void visitEpsilonModule(@NotNull EpsilonModule o) {
    visitElement(o);
  }

  public void visitPsiElement(@NotNull PsiElement o) {
    visitElement(o);
  }

}

/*******************************************************************************
 * Copyright (c) 2008 The University of York.
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *     Horacio Hoyos Rodriguez - initial API and implementation
 ******************************************************************************/
package org.eclipse.epislon.labs.intellij.grammar.psi.eol.impl;

import java.util.List;
import org.jetbrains.annotations.*;
import com.intellij.lang.ASTNode;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiElementVisitor;
import com.intellij.psi.util.PsiTreeUtil;
import static org.eclipse.epislon.labs.intellij.grammar.psi.eol.EolTypes.*;
import com.intellij.extapi.psi.ASTWrapperPsiElement;
import org.eclipse.epislon.labs.intellij.grammar.psi.eol.*;

public class EolCaseStatementImpl extends ASTWrapperPsiElement implements EolCaseStatement {

  public EolCaseStatementImpl(@NotNull ASTNode node) {
    super(node);
  }

  public void accept(@NotNull EolVisitor visitor) {
    visitor.visitCaseStatement(this);
  }

  public void accept(@NotNull PsiElementVisitor visitor) {
    if (visitor instanceof EolVisitor) accept((EolVisitor)visitor);
    else super.accept(visitor);
  }

  @Override
  @NotNull
  public EolExpr getExpr() {
    return findNotNullChildByClass(EolExpr.class);
  }

  @Override
  @NotNull
  public List<EolStatement> getStatementList() {
    return PsiTreeUtil.getChildrenOfTypeAsList(this, EolStatement.class);
  }

  @Override
  @Nullable
  public EolStatementBlock getStatementBlock() {
    return findChildByClass(EolStatementBlock.class);
  }

  @Override
  @NotNull
  public PsiElement getCaseKey() {
    return findNotNullChildByType(EOL_CASE_KEY);
  }

  @Override
  @NotNull
  public PsiElement getColonOp() {
    return findNotNullChildByType(EOL_COLON_OP);
  }

}

/*******************************************************************************
 * Copyright (c) 2008 The University of York.
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *     Horacio Hoyos Rodriguez - initial API and implementation
 ******************************************************************************/
package org.eclipse.epislon.labs.intellij.grammar.psi.eol.impl;

import java.util.List;
import org.jetbrains.annotations.*;
import com.intellij.lang.ASTNode;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiElementVisitor;
import com.intellij.psi.util.PsiTreeUtil;
import static org.eclipse.epislon.labs.intellij.grammar.psi.eol.EolTypes.*;
import org.eclipse.epislon.labs.intellij.grammar.psi.eol.*;

public class EolVariableDeclarationExprImpl extends EolExprImpl implements EolVariableDeclarationExpr {

  public EolVariableDeclarationExprImpl(@NotNull ASTNode node) {
    super(node);
  }

  public void accept(@NotNull EolVisitor visitor) {
    visitor.visitVariableDeclarationExpr(this);
  }

  public void accept(@NotNull PsiElementVisitor visitor) {
    if (visitor instanceof EolVisitor) accept((EolVisitor)visitor);
    else super.accept(visitor);
  }

  @Override
  @Nullable
  public EolParameterList getParameterList() {
    return findChildByClass(EolParameterList.class);
  }

  @Override
  @Nullable
  public EolTypeName getTypeName() {
    return findChildByClass(EolTypeName.class);
  }

  @Override
  @Nullable
  public PsiElement getColonOp() {
    return findChildByType(EOL_COLON_OP);
  }

  @Override
  @Nullable
  public PsiElement getExtKey() {
    return findChildByType(EOL_EXT_KEY);
  }

  @Override
  @NotNull
  public PsiElement getId() {
    return findNotNullChildByType(EOL_ID);
  }

  @Override
  @Nullable
  public PsiElement getNewKey() {
    return findChildByType(EOL_NEW_KEY);
  }

  @Override
  @Nullable
  public PsiElement getVarKey() {
    return findChildByType(EOL_VAR_KEY);
  }

}

/*******************************************************************************
 * Copyright (c) 2008 The University of York.
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *     Horacio Hoyos Rodriguez - initial API and implementation
 ******************************************************************************/
package org.eclipse.epislon.labs.intellij.grammar.psi.egl;

import java.util.List;
import org.jetbrains.annotations.*;
import com.intellij.psi.PsiElement;

public interface EglStatement extends PsiElement {

  @Nullable
  EglAbortStatement getAbortStatement();

  @Nullable
  EglAssignmentStatement getAssignmentStatement();

  @Nullable
  EglBreakAllStatement getBreakAllStatement();

  @Nullable
  EglBreakStatement getBreakStatement();

  @Nullable
  EglContinueStatement getContinueStatement();

  @Nullable
  EglDeleteStatement getDeleteStatement();

  @Nullable
  EglExpressionStatement getExpressionStatement();

  @Nullable
  EglForStatement getForStatement();

  @Nullable
  EglIfStatement getIfStatement();

  @Nullable
  EglPrint getPrint();

  @Nullable
  EglReturnStatement getReturnStatement();

  @Nullable
  EglSwitchStatement getSwitchStatement();

  @Nullable
  EglThrowStatement getThrowStatement();

  @Nullable
  EglTransactionStatement getTransactionStatement();

  @Nullable
  EglWhileStatement getWhileStatement();

}

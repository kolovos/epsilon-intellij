/*******************************************************************************
 * Copyright (c) 2008 The University of York.
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *     Horacio Hoyos Rodriguez - initial API and implementation
 ******************************************************************************/
package org.eclipse.epislon.labs.intellij.grammar.psi.egl;

import java.util.List;
import org.jetbrains.annotations.*;
import com.intellij.psi.PsiElement;

public interface EglLiteralSequentialCollection extends PsiElement {

  @Nullable
  EglExpressionListOrRange getExpressionListOrRange();

  @Nullable
  PsiElement getBagKey();

  @Nullable
  PsiElement getColKey();

  @NotNull
  PsiElement getLeftBrace();

  @Nullable
  PsiElement getListKey();

  @Nullable
  PsiElement getOrdsetKey();

  @NotNull
  PsiElement getRightBrace();

  @Nullable
  PsiElement getSeqKey();

  @Nullable
  PsiElement getSetKey();

}

/*******************************************************************************
 * Copyright (c) 2008 The University of York.
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *     Horacio Hoyos Rodriguez - initial API and implementation
 ******************************************************************************/
package org.eclipse.epislon.labs.intellij.grammar.psi.eol.impl;

import java.util.List;
import org.jetbrains.annotations.*;
import com.intellij.lang.ASTNode;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiElementVisitor;
import com.intellij.psi.util.PsiTreeUtil;
import static org.eclipse.epislon.labs.intellij.grammar.psi.eol.EolTypes.*;
import com.intellij.extapi.psi.ASTWrapperPsiElement;
import org.eclipse.epislon.labs.intellij.grammar.psi.eol.*;

public class EolBuiltInImpl extends ASTWrapperPsiElement implements EolBuiltIn {

  public EolBuiltInImpl(@NotNull ASTNode node) {
    super(node);
  }

  public void accept(@NotNull EolVisitor visitor) {
    visitor.visitBuiltIn(this);
  }

  public void accept(@NotNull PsiElementVisitor visitor) {
    if (visitor instanceof EolVisitor) accept((EolVisitor)visitor);
    else super.accept(visitor);
  }

  @Override
  @Nullable
  public PsiElement getEquivalentsBin() {
    return findChildByType(EOL_EQUIVALENTS_BIN);
  }

  @Override
  @Nullable
  public PsiElement getEquivalentBin() {
    return findChildByType(EOL_EQUIVALENT_BIN);
  }

  @Override
  @Nullable
  public PsiElement getMatchesBin() {
    return findChildByType(EOL_MATCHES_BIN);
  }

  @Override
  @Nullable
  public PsiElement getSatisfiesAllBin() {
    return findChildByType(EOL_SATISFIES_ALL_BIN);
  }

  @Override
  @Nullable
  public PsiElement getSatisfiesBin() {
    return findChildByType(EOL_SATISFIES_BIN);
  }

  @Override
  @Nullable
  public PsiElement getSatisfiesOneBin() {
    return findChildByType(EOL_SATISFIES_ONE_BIN);
  }

}

/*******************************************************************************
 * Copyright (c) 2008 The University of York.
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *     Horacio Hoyos Rodriguez - initial API and implementation
 ******************************************************************************/
package org.eclipse.epislon.labs.intellij.grammar.psi.egl.impl;

import java.util.List;
import org.jetbrains.annotations.*;
import com.intellij.lang.ASTNode;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiElementVisitor;
import com.intellij.psi.util.PsiTreeUtil;
import static org.eclipse.epislon.labs.intellij.grammar.psi.egl.EglTypes.*;
import org.eclipse.epislon.labs.intellij.grammar.psi.egl.*;

public class EglVariableDeclarationExprImpl extends EglExprImpl implements EglVariableDeclarationExpr {

  public EglVariableDeclarationExprImpl(@NotNull ASTNode node) {
    super(node);
  }

  public void accept(@NotNull EglVisitor visitor) {
    visitor.visitVariableDeclarationExpr(this);
  }

  public void accept(@NotNull PsiElementVisitor visitor) {
    if (visitor instanceof EglVisitor) accept((EglVisitor)visitor);
    else super.accept(visitor);
  }

  @Override
  @Nullable
  public EglParameterList getParameterList() {
    return findChildByClass(EglParameterList.class);
  }

  @Override
  @Nullable
  public EglTypeName getTypeName() {
    return findChildByClass(EglTypeName.class);
  }

  @Override
  @Nullable
  public PsiElement getColonOp() {
    return findChildByType(EGL_COLON_OP);
  }

  @Override
  @Nullable
  public PsiElement getExtKey() {
    return findChildByType(EGL_EXT_KEY);
  }

  @Override
  @NotNull
  public PsiElement getId() {
    return findNotNullChildByType(EGL_ID);
  }

  @Override
  @Nullable
  public PsiElement getNewKey() {
    return findChildByType(EGL_NEW_KEY);
  }

  @Override
  @Nullable
  public PsiElement getVarKey() {
    return findChildByType(EGL_VAR_KEY);
  }

}

/*******************************************************************************
 * Copyright (c) 2008 The University of York.
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *     Horacio Hoyos Rodriguez - initial API and implementation
 ******************************************************************************/
package org.eclipse.epislon.labs.intellij.grammar.psi.erl;

import com.intellij.psi.tree.IElementType;
import com.intellij.psi.PsiElement;
import com.intellij.lang.ASTNode;
import org.eclipse.epislon.labs.intellij.grammar.psi.erl.impl.*;

public interface ErlTypes {

  IElementType ERL_CONTENT = new ErlElementType("ERL_CONTENT");
  IElementType ERL_EXTENDS = new ErlElementType("ERL_EXTENDS");
  IElementType ERL_GUARD = new ErlElementType("ERL_GUARD");
  IElementType ERL_MODULE = new ErlElementType("ERL_MODULE");
  IElementType ERL_POST = new ErlElementType("ERL_POST");
  IElementType ERL_PRE = new ErlElementType("ERL_PRE");
  IElementType ERL_STATEMENT = new ErlElementType("ERL_STATEMENT");

  IElementType ERL_EXTENDS_KEY = new ErlTokenType("extends");
  IElementType ERL_GUARD_KEY = new ErlTokenType("guard");
  IElementType ERL_POST_KEY = new ErlTokenType("post");
  IElementType ERL_PRE_KEY = new ErlTokenType("pre");

  class Factory {
    public static PsiElement createElement(ASTNode node) {
      IElementType type = node.getElementType();
      if (type == ERL_CONTENT) {
        return new ErlContentImpl(node);
      }
      else if (type == ERL_EXTENDS) {
        return new ErlExtendsImpl(node);
      }
      else if (type == ERL_GUARD) {
        return new ErlGuardImpl(node);
      }
      else if (type == ERL_MODULE) {
        return new ErlModuleImpl(node);
      }
      else if (type == ERL_POST) {
        return new ErlPostImpl(node);
      }
      else if (type == ERL_PRE) {
        return new ErlPreImpl(node);
      }
      else if (type == ERL_STATEMENT) {
        return new ErlStatementImpl(node);
      }
      throw new AssertionError("Unknown element type: " + type);
    }
  }
}

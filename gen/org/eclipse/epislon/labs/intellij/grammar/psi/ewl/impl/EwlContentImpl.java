/*******************************************************************************
 * Copyright (c) 2008 The University of York.
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *     Horacio Hoyos Rodriguez - initial API and implementation
 ******************************************************************************/
package org.eclipse.epislon.labs.intellij.grammar.psi.ewl.impl;

import java.util.List;
import org.jetbrains.annotations.*;
import com.intellij.lang.ASTNode;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiElementVisitor;
import com.intellij.psi.util.PsiTreeUtil;
import static org.eclipse.epislon.labs.intellij.grammar.psi.ewl.EwlTypes.*;
import com.intellij.extapi.psi.ASTWrapperPsiElement;
import org.eclipse.epislon.labs.intellij.grammar.psi.ewl.*;
import org.eclipse.epislon.labs.intellij.grammar.parser.ewl.EwlParserUtil;
import org.eclipse.epislon.labs.intellij.grammar.psi.eol.EolAnnotationBlock;
import org.eclipse.epislon.labs.intellij.grammar.psi.eol.EolOperationDeclaration;

public class EwlContentImpl extends ASTWrapperPsiElement implements EwlContent {

  public EwlContentImpl(@NotNull ASTNode node) {
    super(node);
  }

  public void accept(@NotNull EwlVisitor visitor) {
    visitor.visitContent(this);
  }

  public void accept(@NotNull PsiElementVisitor visitor) {
    if (visitor instanceof EwlVisitor) accept((EwlVisitor)visitor);
    else super.accept(visitor);
  }

  @Override
  @NotNull
  public List<EwlPost> getPostList() {
    return PsiTreeUtil.getChildrenOfTypeAsList(this, EwlPost.class);
  }

  @Override
  @NotNull
  public List<EwlPre> getPreList() {
    return PsiTreeUtil.getChildrenOfTypeAsList(this, EwlPre.class);
  }

  @Override
  @NotNull
  public List<EwlWizard> getWizardList() {
    return PsiTreeUtil.getChildrenOfTypeAsList(this, EwlWizard.class);
  }

  @Override
  @NotNull
  public List<EolAnnotationBlock> getAnnotationBlockList() {
    return EwlParserUtil.getAnnotationBlockList(this);
  }

  @Override
  @NotNull
  public List<EolOperationDeclaration> getOperationDeclarationList() {
    return EwlParserUtil.getOperationDeclarationList(this);
  }

}

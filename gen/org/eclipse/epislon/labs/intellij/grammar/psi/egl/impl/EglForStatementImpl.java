/*******************************************************************************
 * Copyright (c) 2008 The University of York.
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *     Horacio Hoyos Rodriguez - initial API and implementation
 ******************************************************************************/
package org.eclipse.epislon.labs.intellij.grammar.psi.egl.impl;

import java.util.List;
import org.jetbrains.annotations.*;
import com.intellij.lang.ASTNode;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiElementVisitor;
import com.intellij.psi.util.PsiTreeUtil;
import static org.eclipse.epislon.labs.intellij.grammar.psi.egl.EglTypes.*;
import com.intellij.extapi.psi.ASTWrapperPsiElement;
import org.eclipse.epislon.labs.intellij.grammar.psi.egl.*;

public class EglForStatementImpl extends ASTWrapperPsiElement implements EglForStatement {

  public EglForStatementImpl(@NotNull ASTNode node) {
    super(node);
  }

  public void accept(@NotNull EglVisitor visitor) {
    visitor.visitForStatement(this);
  }

  public void accept(@NotNull PsiElementVisitor visitor) {
    if (visitor instanceof EglVisitor) accept((EglVisitor)visitor);
    else super.accept(visitor);
  }

  @Override
  @NotNull
  public EglExpr getExpr() {
    return findNotNullChildByClass(EglExpr.class);
  }

  @Override
  @NotNull
  public EglFormalParameter getFormalParameter() {
    return findNotNullChildByClass(EglFormalParameter.class);
  }

  @Override
  @Nullable
  public EglStatement getStatement() {
    return findChildByClass(EglStatement.class);
  }

  @Override
  @Nullable
  public EglStatementBlock getStatementBlock() {
    return findChildByClass(EglStatementBlock.class);
  }

  @Override
  @NotNull
  public PsiElement getForKey() {
    return findNotNullChildByType(EGL_FOR_KEY);
  }

  @Override
  @NotNull
  public PsiElement getInKey() {
    return findNotNullChildByType(EGL_IN_KEY);
  }

  @Override
  @NotNull
  public PsiElement getLeftParen() {
    return findNotNullChildByType(EGL_LEFT_PAREN);
  }

  @Override
  @NotNull
  public PsiElement getRightParen() {
    return findNotNullChildByType(EGL_RIGHT_PAREN);
  }

}

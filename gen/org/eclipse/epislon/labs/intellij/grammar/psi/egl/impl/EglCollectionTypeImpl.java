/*******************************************************************************
 * Copyright (c) 2008 The University of York.
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *     Horacio Hoyos Rodriguez - initial API and implementation
 ******************************************************************************/
package org.eclipse.epislon.labs.intellij.grammar.psi.egl.impl;

import java.util.List;
import org.jetbrains.annotations.*;
import com.intellij.lang.ASTNode;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiElementVisitor;
import com.intellij.psi.util.PsiTreeUtil;
import static org.eclipse.epislon.labs.intellij.grammar.psi.egl.EglTypes.*;
import com.intellij.extapi.psi.ASTWrapperPsiElement;
import org.eclipse.epislon.labs.intellij.grammar.psi.egl.*;

public class EglCollectionTypeImpl extends ASTWrapperPsiElement implements EglCollectionType {

  public EglCollectionTypeImpl(@NotNull ASTNode node) {
    super(node);
  }

  public void accept(@NotNull EglVisitor visitor) {
    visitor.visitCollectionType(this);
  }

  public void accept(@NotNull PsiElementVisitor visitor) {
    if (visitor instanceof EglVisitor) accept((EglVisitor)visitor);
    else super.accept(visitor);
  }

  @Override
  @Nullable
  public EglTypeNameList getTypeNameList() {
    return findChildByClass(EglTypeNameList.class);
  }

  @Override
  @Nullable
  public PsiElement getBagKey() {
    return findChildByType(EGL_BAG_KEY);
  }

  @Override
  @Nullable
  public PsiElement getColKey() {
    return findChildByType(EGL_COL_KEY);
  }

  @Override
  @Nullable
  public PsiElement getGtOp() {
    return findChildByType(EGL_GT_OP);
  }

  @Override
  @Nullable
  public PsiElement getLeftParen() {
    return findChildByType(EGL_LEFT_PAREN);
  }

  @Override
  @Nullable
  public PsiElement getListKey() {
    return findChildByType(EGL_LIST_KEY);
  }

  @Override
  @Nullable
  public PsiElement getLtOp() {
    return findChildByType(EGL_LT_OP);
  }

  @Override
  @Nullable
  public PsiElement getMapKey() {
    return findChildByType(EGL_MAP_KEY);
  }

  @Override
  @Nullable
  public PsiElement getOrdsetKey() {
    return findChildByType(EGL_ORDSET_KEY);
  }

  @Override
  @Nullable
  public PsiElement getRightParen() {
    return findChildByType(EGL_RIGHT_PAREN);
  }

  @Override
  @Nullable
  public PsiElement getSeqKey() {
    return findChildByType(EGL_SEQ_KEY);
  }

  @Override
  @Nullable
  public PsiElement getSetKey() {
    return findChildByType(EGL_SET_KEY);
  }

}
